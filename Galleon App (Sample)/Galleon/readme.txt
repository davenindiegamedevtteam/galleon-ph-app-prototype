--- Corrective Dimensions in GridView ---

[TVDPI]
Two Way GridView (main - main4): 250dp x 250dp
ListView Layout Base: 250dp x 250dp





--- API for Sending from Android to Webservice Database Server ---

[10:42:00 AM] adin234: 
just stick with the rest api for now, we all have deadlines to meet. and another suggestion, put all URLs in a static class 
so that it wont be hard to update the app when the url changes(ie. somewhere.com to somewhereelse.com)

you can use a static class

class UrlContract {
 public static final String BASE_URL = "http://somewhere.com/";
 public static final String SEARCH_URL = BASE_URL + "search/keyword=%s&page=%s";
}

Original URL: "http://www.galleon.ph/search?keyword=Macross"





--- Sending/Receiving ---

[10:55:42 AM] adin234: for example you have a url for sending the data
you can put

string SEND_URL = BASE_URL + "send/";


so in your code, you can just use 

UrlContract.SEND_URL instead of using "http://galleon.ph/send/";





--- Sample JSON File (Normal) ---

{
	"product_id":"23703",
	"product_amazon":"B007R6HUBM",
	"product_url":"http:\/\/www.amazon.com\/HMDX-HX-P230RD-Bluetooth-Wireless-Strawberry\/dp\/B007R6HUBM%3FSubscriptionId%3DAKIAJ4UT35G3XRRMJZBQ%26tag%3Ddevkit-20%26linkCode%3Dxm2%26camp%3D2025%26creative%3D165953%26creativeASIN%3DB007R6HUBM",
	"product_name":"HMDX Audio HX-P230RD JAM Bluetooth Wireless Speaker (Strawberry)",
	"product_description":"The HMDX Jam Bluetooth Speaker is a wireless portable speaker with incredible sound quality in a small footprint. Listen to the Jam's great sound for up to four hours of wireless play, up to thirty feet away from your paired Bluetooth-enabled devices. Let your smart phones, tablets and any other Bluetooth-enabled device jam, with the HMDX Jam Bluetooth Speaker. Available in a variety of colors.","product_price":"40","product_srp":"2726","product_active":"2",
	"product_updated":"1376206626",
	"product_created":"1349270897",
	"product_category":"92",
	"product_image":"http:\/\/ecx.images-amazon.com\/images\/I\/41dbS%2BcsvRL.jpg"
}





--- Sample JSON File (REST) ---

{
	"ASIN":"B00029WYEY",
	"title":"K&N 99-5000 Aerosol Recharger Filter Care Service Kit",
	"manufacturer":"K&N","upc":"024844000224",
	"feature":["Includes 6.5 oz. air filter oil, 12 oz. filter cleaner","Weight of the recharger kit is 2 lb (0.9 kg.)","Air filter oil prevents dirt from entering your engine and works to dissolve the dirt build up and oil","This item is not for sale in Catalina Island","\"Special Shipping Information: This item cannot be returned and has additional shipping restrictions\""],
	"height":"243",
	"weight":"150",
	"length":"900",
	"width":"469",
	"url":"http:\/\/www.amazon.com\/99-5000-Aerosol-Recharger-Filter-Service\/dp\/B00029WYEY%3FSubscriptionId%3DAKIAI3XM53IGZRPXET5Q%26tag%3Dgalgome-20%26linkCode%3Dxm2%26camp%3D2025%26creative%3D165953%26creativeASIN%3DB00029WYEY",
	"price":"$11.56",
	"model":"99-5000",
	"fulfilled":1,
	"description":"K&N Recharger kit contains K&N air filter cleaner and air filter oil. It is a six-step maintenance system designed to recharge any K&N FilterCharger air filter. Air filter oil is used to treat foam air filter elements to increase their filtering capacity. Red filter oil helps to extend engine life and prevents dirt from entering your engine. Air filter oil boosts the performance of cars and trucks. K&N air filter cleaner is the only cleaner formulated to clean K&N FilterCharger elements. K&N air filter cleaner works to dissolve the dirt build up and old filter oil, and can be washed away with water. It is biodegradable and safe on paint, chrome, cast aluminum, plastic, rubber and vinyl when used as directed. Complete instruction is included with this kit.",
	"images":["http:\/\/ecx.images-amazon.com\/images\/I\/515n4GMRWkL.jpg"],
	"product_srp":"0",
	"product_id":"1271470",
	"product_active":"1"}