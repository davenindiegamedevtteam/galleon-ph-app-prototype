package com.openovate.galleon;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Iterator;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.HttpVersion;
import org.apache.http.message.BasicHttpResponse;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.database.Cursor;
import android.database.SQLException;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;
import au.com.bytecode.opencsv.CSVWriter;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.MenuItem;
import com.jess.ui.TwoWayGridView;
import com.openovate.galleon.Controls.MenuContentControls.MenuFragHelper;
import com.openovate.galleon.Database.Controls.RegistrationListener;
import com.openovate.galleon.Database.Controls.HttpPostPackage.HttpPostProductIdHelper;
import com.openovate.galleon.Database.Model.DatabaseDbHelper;
import com.openovate.galleon.Database.Model.DatabaseTracker;
import com.openovate.galleon.Model.Flaggers.Flag;
import com.openovate.galleon.Model.RegionLists.RegionSchema;
import com.openovate.galleon.Model.URLFinder.URLContractFinder;
import com.openovate.galleon.View.Category.Category_1_ToysKidsandBaby;
import com.openovate.galleon.View.Category.Category_2_SportsAndOutdoors;
import com.openovate.galleon.View.Category.Category_3_ClothingShoesAndJewelry;
import com.openovate.galleon.View.Category.Category_4_ElectronicsAndComputer;
import com.openovate.galleon.View.Category.Category_5_HomeGardenAndTools;
import com.openovate.galleon.View.Category.Category_Custom_Search;
import com.openovate.galleon.View.Category.Category_Select_Dialog_Pane;
import com.openovate.galleon.View.Notifications.Message_Dialog;
import com.openovate.galleon.View.Share.Share_Dialog;
import com.openovate.galleon.jsonParser.adapters.ProductListAdapter2;
import com.openovate.galleon.jsonParser.models.FeatureModel;
import com.openovate.galleon.jsonParser.models.ProductModel2;

@SuppressLint("ShowToast")
public class PurchaseActivity extends SherlockActivity
{
	//TODO _______________[ Field Data ]_______________
	// Purchase Activity Context
	public final Context context = this;
	
	// Registration Listener
	@SuppressWarnings("unused")
	private RegistrationListener rl;
	
	// Values for Billing Info
	public static EditText billName;
	public static EditText billEmail;
	public static EditText billPhone;
	public static EditText billStreet1;
	public static EditText billStreet2;
	public static EditText billCity;
	public static EditText billCountry;
	public static EditText billZipCode;
	public static Spinner billRegion;
	
	// Values for Shipping Info
	public static EditText shipName;
	public static EditText shipEmail;
	public static EditText shipPhone;
	public static EditText shipStreet1;
	public static EditText shipStreet2;
	public static EditText shipCity;
	public static EditText shipCountry;
	public static EditText shipZipCode;
	public static Spinner shipRegion;
	
	// Values for Order Review
	private TextView shippingCost;
	private TextView subtotal;
	public static TextView grandtotal;

	// Table Column Labels Form for Displaying Customer's Database on Android (For testing purposes.)
	public static TextView name;
	public static TextView email;
	public static TextView phone;
	public static TextView street1;
	public static TextView street2;
	public static TextView city;
	public static TextView country;
	public static TextView zip;
	public static TextView region;
	public static TextView nameShipping;
	public static TextView emailShipping;
	public static TextView phoneShipping;
	public static TextView street1Shipping;
	public static TextView street2Shipping;
	public static TextView cityShipping;
	public static TextView countryShipping;
	public static TextView zipShipping;
	public static TextView regionShipping;
	public static TextView items;
	public static TextView costs;
	public static TextView payment;

	// Table Column Labels for Values (For testing purposes.)
	public static TextView nameGot;
	public static TextView emailGot;
	public static TextView phoneGot;
	public static TextView street1Got;
	public static TextView street2Got;
	public static TextView cityGot;
	public static TextView countryGot;
	public static TextView zipGot;
	public static TextView regionGot;
	public static TextView nameShippingGot;
	public static TextView emailShippingGot;
	public static TextView phoneShippingGot;
	public static TextView street1ShippingGot;
	public static TextView street2ShippingGot;
	public static TextView cityShippingGot;
	public static TextView countryShippingGot;
	public static TextView zipShippingGot;
	public static TextView regionShippingGot;
	public static TextView itemGot;
	public static TextView costGot;
	public static TextView paymentGot;
	
	// Radio Group for Choices
	private RadioGroup shippingFormOptions;
	private RadioGroup paymentMethodOptions;
	
	// Query
	public static String query = "SELECT * FROM " + DatabaseTracker.TABLE_LABELS_2;
	
	// Database Helper
	public DatabaseDbHelper dh;
	
	// Table Layouts
	public static TableRow tableRow;
	public static TableLayout tableLayout;
	
	// String Value from Choice Results
	public static String bc = null; // --> Value result from choices under radio button group. (Billing Region)
	public static String sc = null; // --> Value result from choices under radio button group. (Shipping Region)
	public static String pc = "BDO (Bank Deposit)"; // --> Value result from choices under radio button group. (Payment Method)
	
	// Message String
	public static final String NULL_ERROR = "field is null";
	public static final String FIELD_ERROR = "field is empty";
	
	// Destination String
	private String target = null;
	private String pre_target = null; // --> When a user asks for the same destination to be delivered.




	
	
	//TODO _______________[ Sherlock Activity Overriden Methods ]_______________
	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		// On Create Activity
		super.onCreate(savedInstanceState);
		setContentView(R.layout.purchase_form);
	
		// JSON File Check (Shipping Cost)
		Log.v("SHIPPING COST JSON", Flag.shippingCostMap);
		
		// Billing Info
		billName = (EditText) findViewById(R.id.ET_Billing_Name);
		billEmail = (EditText) findViewById(R.id.ET_Billing_Email);
		billPhone = (EditText) findViewById(R.id.ET_Billing_Phone);
		billStreet1 = (EditText) findViewById(R.id.ET_Billing_Street_1);
		billStreet2 = (EditText) findViewById(R.id.ET_Billing_Street_2);
		billCity = (EditText) findViewById(R.id.ET_Billing_City);
		billCountry = (EditText) findViewById(R.id.ET_Billing_Country);
		billZipCode = (EditText) findViewById(R.id.ET_Billing_Zip_Code);
		billRegion = (Spinner) findViewById(R.id.SP_Billing_Region);
		
		// Shipping Info
		shipName = (EditText) findViewById(R.id.ET_Shipping_Name);
		shipEmail = (EditText) findViewById(R.id.ET_Shipping_Email);
		shipPhone = (EditText) findViewById(R.id.ET_Shipping_Phone);
		shipStreet1 = (EditText) findViewById(R.id.ET_Shipping_Street_1);
		shipStreet2 = (EditText) findViewById(R.id.ET_Shipping_Street_2);
		shipCity = (EditText) findViewById(R.id.ET_Shipping_City);
		shipCountry = (EditText) findViewById(R.id.ET_Shipping_Country);
		shipZipCode = (EditText) findViewById(R.id.ET_Shipping_Zip_Code);
		shipRegion = (Spinner) findViewById(R.id.SP_Shipping_Region);
		
		// Radio Group (Shipping and Payment Options [Reserved])
		shippingFormOptions = (RadioGroup) findViewById(R.id.RG_Shipping_Delivery_Method);
		paymentMethodOptions = (RadioGroup) findViewById(R.id.RG_Payment_Method_Choices);
		
		// Flag (Toggle Radio Group ON/OFF)
		TextView label = (TextView) findViewById(R.id.TV_Payment_Label);
		paymentMethodOptions.setVisibility(View.GONE);
		label.setVisibility(View.GONE);
		
		// Buttons
//		Button debug = (Button) findViewById(R.id.BTN_Tester); // --> For testing purpose only.
		ImageButton submit = (ImageButton) findViewById(R.id.BTN_Submit_Order);
		
		// Set click listener to record it on database.
		submit.setOnClickListener(RegistrationListener.getFinishButtonResponse(context));
		
		// Region List Adapter Dropdown
        setRegionList();
        
        // Order Review Info
        TextView cartlist = (TextView) findViewById(R.id.TV_Cart_List);
        subtotal = (TextView) findViewById(R.id.TV_Order_Review_Subtotal);
        grandtotal = (TextView) findViewById(R.id.TV_Order_Review_Grandtotal);
        shippingCost = (TextView) findViewById(R.id.TV_Order_Review_Shipping_Cost);
        
        // Order Review Display
        if(Flag.listResult.equals("DETAIL"))
		{
        	cartlist.setText(ProductDetailActivity.items);
        	subtotal.setText(ProductDetailActivity.costs);
			Log.v("LISTENER ITEM AT (PURCHASE ACTIVITY)", "Product Details Activity");
			
		} else if(Flag.listResult.equals("CART")) {
			
			cartlist.setText(CartListActivity.items);
			subtotal.setText(CartListActivity.costs);
			Log.v("LISTENER ITEM AT (PURCHASE ACTIVITY)", "Cartlist Activity");
			
		}
		
        // Choice for Shipping Information
		shippingFormOptions.setOnCheckedChangeListener(new OnCheckedChangeListener() 
		{
			@Override
			public void onCheckedChanged(RadioGroup group, int checkedId) 
			{
				switch(checkedId)
				{
				case R.id.RB_Shipping_Method_1: // --> Claim order at Galleon.ph office.
					shipName.setText("Galleon Office");
					shipEmail.setText("support@galleon.ph");
					shipPhone.setText("09054188697");
					shipStreet1.setText("Tower 1, Pioneer corner Madison Street");
					shipStreet2.setText(" ");
					shipCity.setText("Mandaluyong City");
					shipCountry.setText("Philippines");
					shipZipCode.setText("1552");
					sc = "NCR (National Capital Region)";
					shipName.setEnabled(false);
					shipEmail.setEnabled(false);
					shipPhone.setEnabled(false);
					shipStreet1.setEnabled(false);
					shipStreet2.setEnabled(false);
					shipCity.setEnabled(false);
					shipCountry.setEnabled(false);
					shipZipCode.setEnabled(false);
					shipRegion.setEnabled(false);
					target = "NCR";
					getShippingCost(Flag.shippingCostMap);
					break;
					
				case R.id.RB_Shipping_Method_2: // --> Ship to the same address.
					shipName.setText(billName.getText().toString());
					shipEmail.setText(billEmail.getText().toString());
					shipPhone.setText(billPhone.getText().toString());
					shipStreet1.setText(billStreet1.getText().toString());
					shipStreet2.setText(billStreet2.getText().toString());
					shipCity.setText(billCity.getText().toString());
					shipCountry.setText(billCountry.getText().toString());
					shipZipCode.setText(billZipCode.getText().toString());
					sc = bc;
					shipName.setEnabled(false);
					shipEmail.setEnabled(false);
					shipPhone.setEnabled(false);
					shipStreet1.setEnabled(false);
					shipStreet2.setEnabled(false);
					shipCity.setEnabled(false);
					shipCountry.setEnabled(false);
					shipZipCode.setEnabled(false);
					shipRegion.setEnabled(false);
					target = pre_target;
					getShippingCost(Flag.shippingCostMap);
					break;
					
				case R.id.RB_Shipping_Method_3: // --> Ship to the specified address.
					shipName.setText(null);
					shipEmail.setText(null);
					shipPhone.setText(null);
					shipStreet1.setText(null);
					shipStreet2.setText(null);
					shipCity.setText(null);
					shipCountry.setText(null);
					shipZipCode.setText(null);
					sc = null;
					shipName.setEnabled(true);
					shipEmail.setEnabled(true);
					shipPhone.setEnabled(true);
					shipStreet1.setEnabled(true);
					shipStreet2.setEnabled(true);
					shipCity.setEnabled(true);
					shipCountry.setEnabled(true);
					shipZipCode.setEnabled(true);
					shipRegion.setEnabled(true);
					break;
				}
			}
		});
		
		// Choice for Billing Information
		paymentMethodOptions.setOnCheckedChangeListener(new OnCheckedChangeListener() 
		{
			@Override
			public void onCheckedChanged(RadioGroup group, int checkedId) 
			{
				switch(checkedId)
				{
				case R.id.RB_Payment_1: System.out.println("BDO"); pc = "BDO (Bank Deposit)"; break;
				case R.id.RB_Payment_2: System.out.println("Smart"); pc = "AsiaPay, Smart Money, or Globe G-Cash"; break;
				case R.id.RB_Payment_3: System.out.println("VISA"); pc = "Paypal, VISA, or Mastercarrd"; break;
				case R.id.RB_Payment_4: System.out.println("CashSense"); pc = "CashSense"; break;
				}
			}
		});
		
		rl = new RegistrationListener(context);
	}

	@SuppressWarnings({ "unused", "static-access" })
	@Override
    public boolean onCreateOptionsMenu(com.actionbarsherlock.view.Menu menu) 
    {
		super.onCreateOptionsMenu(menu);
		
        // Inflate the menu; this adds items to the action bar if it is present.
        getSupportMenuInflater().inflate(R.menu.project_galleon_main, menu);
        ActionBar ab = getSupportActionBar();
        ab.setCustomView(R.layout.main_actionbar_galleon);
        
        // Set functionality of the textbox for edit listener.
        MenuFragHelper.SearchFunction.searchTextBox(ab, this);
        
        // Cart Button on Actionbar.
        ImageButton cartButton = (ImageButton) ab.getCustomView().findViewById(R.id.IB_Add_to_Cart);
        cartButton.setOnClickListener(new View.OnClickListener() 
        {
			@Override
			public void onClick(View v) 
			{
				Intent intent = new Intent();
				intent.setClass(context, CartListActivity.class);
				startActivity(intent);
			}
		});
        
        ab.setDisplayOptions(ab.DISPLAY_SHOW_CUSTOM | ab.DISPLAY_SHOW_CUSTOM);
        return true;
    }

	@Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
    	// Choice logics within the soft menu key dropdown event when one of the item ID's is touched.
    	switch(item.getItemId())
    	{
    	case R.id.OPT_1_Home:
    		home();
    		return true;
    		
    	case R.id.OPT_2_Login:
    		login();
    		return true;
    		
    	case R.id.OPT_3_Share:
    		share();
    		return true;
    		
    	case R.id.OPT_4_Categories:
    		categories();
    		return true;
    		
    	case R.id.OPT_5_Search:
    		search();
    		return true;
    		
    	case R.id.OPT_6_Cart_List:
    		cart();
    		return true;
    		
    	case R.id.OPT_7_Wish_List:
    		wish();
    		return true;
    		
    	case R.id.OPT_8_Buy:
    		buy();
    		return true;
    		
    	case R.id.OPT_9_Exit:
    		exit();
    		return true;
    		
    	default:
    		return true;
    	}
    }
    
    
    
    
    
    //TODO _______________[ On Choice Listener Event for Menu Soft Key ]_______________
    private void home()
    {
    	Intent intent = new Intent(this, ProjectGalleonMainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP); 
        startActivity(intent);
    }
    
    private void login()
    {
    	Flag.dialogResult = "login";
    	System.out.println("FLAG " + Flag.dialogResult);
    	startActivity(new Intent(this, Message_Dialog.class));
    }
    
    private void share()
    {
    	startActivity(new Intent(this, Share_Dialog.class));
    }
    
    private void categories()
    {
//    	startActivity(new Intent(this, Category_Select_Dialog_Pane.class));
    	Flag.dialogResult = "login";
    	System.out.println("FLAG " + Flag.dialogResult);
    	startActivity(new Intent(this, Message_Dialog.class));
    }
    
    private void search()
    {
    	finish();
    	startActivity(new Intent(this, Category_Custom_Search.class));
    }
    
    private void cart()
    {
    	finish();
    	startActivity(new Intent(this, CartListActivity.class));
    }
    
    private void wish()
    {
    	finish();
    	startActivity(new Intent(this, WishListActivity.class));
    }
    
    private void buy()
    {
    	finish();
    	startActivity(new Intent(this, PurchaseActivity.class));
    }
    
    private void exit()
    {
    	Flag.dialogResult = "exit";
    	System.out.println("FLAG " + Flag.dialogResult);
    	startActivity(new Intent(this, Message_Dialog.class));
    }
    
    
    
    
    
    
    //TODO _______________[ On Method Callbacks for Category Dialogs ]_______________
    public void toysKidsandBaby()
    {
    	startActivity(new Intent(this, Category_1_ToysKidsandBaby.class));
    }
    
    public void sportsAndOutdoors()
    {
    	startActivity(new Intent(this, Category_2_SportsAndOutdoors.class));
    }
    
    public void clothingShoesAndJewelry()
    {
    	startActivity(new Intent(this, Category_3_ClothingShoesAndJewelry.class));
    }
    
    public void electronicsAndComputers()
    {
    	startActivity(new Intent(this, Category_4_ElectronicsAndComputer.class));
    }
    
    public void homeGardenAndTools()
    {
    	startActivity(new Intent(this, Category_5_HomeGardenAndTools.class));
    }
    
    
    
    
    
    //TODO _______________[ Show Message Dialog Method) ]_______________
    public void showMessageDialog() 
    {
    	startActivity(new Intent(this, Message_Dialog.class));
	}
	
	
	
	
	
	//TODO __________[ Method for Showing the Database (For testing purposes only.) ]__________
	@TargetApi(Build.VERSION_CODES.FROYO) 
	public void showCustomerDatabase(View view)
	{
		showCustomerDatabase();
	}
	
	@SuppressWarnings("unused")
	@TargetApi(Build.VERSION_CODES.FROYO) 
	public void showCustomerDatabase()
	{
		setContentView(R.layout.database_viewer);
		LinearLayout sub = (LinearLayout) findViewById(R.id.Scroll_Layout);
		
		dh = new DatabaseDbHelper(context);
		dh.db = dh.db;
		
		Cursor c = dh.db.rawQuery(query, null);
	    int count= c.getCount();
	    c.moveToFirst();
	    
	    LinearLayout mainLayout = new LinearLayout(getApplicationContext());
	    mainLayout.setOrientation(LinearLayout.VERTICAL);
	    LinearLayout.LayoutParams params = new LinearLayout.LayoutParams
	    		(
	    				LinearLayout.LayoutParams.MATCH_PARENT,
	    				LinearLayout.LayoutParams.MATCH_PARENT
	    		);
	    
	    LinearLayout databaseLayout = new LinearLayout(getApplicationContext());
	    databaseLayout.setOrientation(LinearLayout.VERTICAL);
	    LinearLayout.LayoutParams dbParams = new LinearLayout.LayoutParams
	    		(
	    				LinearLayout.LayoutParams.MATCH_PARENT,
	    				0
	    		);
	    databaseLayout.setWeightSum(1);
	    
	    LinearLayout buttonLayout = new LinearLayout(getApplicationContext());
	    buttonLayout.setOrientation(LinearLayout.HORIZONTAL);
	    LinearLayout.LayoutParams buttonParams = new LinearLayout.LayoutParams
	    		(
	    				LinearLayout.LayoutParams.MATCH_PARENT,
	    				0
	    		);
	    buttonLayout.setWeightSum(1);
	    buttonLayout.setGravity(Gravity.CENTER);
	    
	    tableLayout = new TableLayout(getApplicationContext());
	    TableLayout.LayoutParams tblParams = new TableLayout.LayoutParams
	    		(
	    				LinearLayout.LayoutParams.MATCH_PARENT,
	    				10
	    		);
	    tableLayout.setWeightSum(0.5f);
	    tableLayout.setGravity(Gravity.RIGHT);
	    
	    Button exitButton = new Button(getApplicationContext());
	    exitButton.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));
	    exitButton.setWidth(150);
	    exitButton.setHeight(50);
	    exitButton.setText("Exit");
	    exitButton.setOnClickListener(RegistrationListener.exit(context));
	    buttonLayout.addView(exitButton);
	    
	    Button exportButton = (Button) findViewById(R.id.BTN_Checkout);
	    exportButton.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));
	    exportButton.setWidth(180);
	    exportButton.setHeight(50);
	    exportButton.setText("Export Database");
	    exportButton.setOnClickListener(new View.OnClickListener() 
	    {
			@Override
			public void onClick(View v) 
			{
				try
		        {
					new PurchaseActivity.ExportCSVDatabase().execute("");

		        } catch(Exception ex){
		              
		        	Log.e("Error in exporting database under AndroidStarter.",ex.toString());
		        	
		        }
			}
		});
//	    buttonLayout.addView(exportButton);
	    
	    tableLayout.addView(buttonLayout);
	    
	    ScrollView scroll = new ScrollView(getApplicationContext());
	    scroll.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, 10));
	    scroll.setVerticalScrollBarEnabled(true);
	    scroll.setHorizontalScrollBarEnabled(true);
	    scroll.setVisibility(1);
	    
	    tableRow = new TableRow(getApplicationContext());
	    
	    name = new TextView(getApplicationContext());
	    name.setText("NAME");
	    name.setTextColor(Color.argb(255, 255, 119, 0));
	  	name.setTypeface(null, Typeface.BOLD);
	  	name.setPadding(20, 20, 20, 20);
	  	tableRow.addView(name);
	  	
	    email = new TextView(getApplicationContext());
	  	email.setText("EMAIL");
	  	email.setTextColor(Color.argb(255, 255, 119, 0));
	  	email.setTypeface(null, Typeface.BOLD);
	  	email.setPadding(20, 20, 20, 20);
	  	tableRow.addView(email);
	  	
	  	phone = new TextView(getApplicationContext());
	  	phone.setText("PHONE");
	  	phone.setTextColor(Color.argb(255, 255, 119, 0));
	  	phone.setTypeface(null, Typeface.BOLD);
	  	phone.setPadding(20, 20, 20, 20);
	  	tableRow.addView(phone);
	  	
	  	street1 = new TextView(getApplicationContext());
	  	street1.setText("STREET 1");
	  	street1.setTextColor(Color.argb(255, 255, 119, 0));
	  	street1.setTypeface(null, Typeface.BOLD);
	  	street1.setPadding(20, 20, 20, 20);
	  	tableRow.addView(street1);
	  	
	  	street2 = new TextView(getApplicationContext());
	  	street2.setText("STREET 2");
	  	street2.setTextColor(Color.argb(255, 255, 119, 0));
	  	street2.setTypeface(null, Typeface.BOLD);
	  	street2.setPadding(20, 20, 20, 20);
	  	tableRow.addView(street2);
	  	
	  	city = new TextView(getApplicationContext());
	  	city.setText("CITY");
	  	city.setTextColor(Color.argb(255, 255, 119, 0));
	  	city.setTypeface(null, Typeface.BOLD);
	  	city.setPadding(20, 20, 20, 20);
	  	tableRow.addView(city);
	  	
	  	country = new TextView(getApplicationContext());
	  	country.setText("COUNTRY");
	  	country.setTextColor(Color.argb(255, 255, 119, 0));
	  	country.setTypeface(null, Typeface.BOLD);
	  	country.setPadding(20, 20, 20, 20);
	  	tableRow.addView(country);
	  	
	  	zip = new TextView(getApplicationContext());
	  	zip.setText("ZIP CODE");
	  	zip.setTextColor(Color.argb(255, 255, 119, 0));
	  	zip.setTypeface(null, Typeface.BOLD);
	  	zip.setPadding(20, 20, 20, 20);
	  	tableRow.addView(zip);
	  	
	  	region = new TextView(getApplicationContext());
	  	region.setText("REGION");
	  	region.setTextColor(Color.argb(255, 255, 119, 0));
	  	region.setTypeface(null, Typeface.BOLD);
	  	region.setPadding(20, 20, 20, 20);
	  	tableRow.addView(region);
	  	
	  	nameShipping = new TextView(getApplicationContext());
	    nameShipping.setText("NAME (SHIPPING)");
	    nameShipping.setTextColor(Color.argb(255, 255, 119, 0));
	  	nameShipping.setTypeface(null, Typeface.BOLD);
	  	nameShipping.setPadding(20, 20, 20, 20);
	  	tableRow.addView(nameShipping);
	  	
	    emailShipping = new TextView(getApplicationContext());
	  	emailShipping.setText("EMAIL (SHIPPING)");
	  	emailShipping.setTextColor(Color.argb(255, 255, 119, 0));
	  	emailShipping.setTypeface(null, Typeface.BOLD);
	  	emailShipping.setPadding(20, 20, 20, 20);
	  	tableRow.addView(emailShipping);
	  	
	  	phoneShipping = new TextView(getApplicationContext());
	  	phoneShipping.setText("PHONE (SHIPPING)");
	  	phoneShipping.setTextColor(Color.argb(255, 255, 119, 0));
	  	phoneShipping.setTypeface(null, Typeface.BOLD);
	  	phoneShipping.setPadding(20, 20, 20, 20);
	  	tableRow.addView(phoneShipping);
	  	
	  	street1Shipping = new TextView(getApplicationContext());
	  	street1Shipping.setText("STREET 1 (SHIPPING)");
	  	street1Shipping.setTextColor(Color.argb(255, 255, 119, 0));
	  	street1Shipping.setTypeface(null, Typeface.BOLD);
	  	street1Shipping.setPadding(20, 20, 20, 20);
	  	tableRow.addView(street1Shipping);
	  	
	  	street2Shipping = new TextView(getApplicationContext());
	  	street2Shipping.setText("STREET 2 (SHIPPPING)");
	  	street2Shipping.setTextColor(Color.argb(255, 255, 119, 0));
	  	street2Shipping.setTypeface(null, Typeface.BOLD);
	  	street2Shipping.setPadding(20, 20, 20, 20);
	  	tableRow.addView(street2Shipping);
	  	
	  	cityShipping = new TextView(getApplicationContext());
	  	cityShipping.setText("CITY (SHIPPING)");
	  	cityShipping.setTextColor(Color.argb(255, 255, 119, 0));
	  	cityShipping.setTypeface(null, Typeface.BOLD);
	  	cityShipping.setPadding(20, 20, 20, 20);
	  	tableRow.addView(cityShipping);
	  	
	  	countryShipping = new TextView(getApplicationContext());
	  	countryShipping.setText("COUNTRY (SHIPPING)");
	  	countryShipping.setTextColor(Color.argb(255, 255, 119, 0));
	  	countryShipping.setTypeface(null, Typeface.BOLD);
	  	countryShipping.setPadding(20, 20, 20, 20);
	  	tableRow.addView(countryShipping);
	  	
	  	zipShipping = new TextView(getApplicationContext());
	  	zipShipping.setText("ZIP CODE (SHIPPING)");
	  	zipShipping.setTextColor(Color.argb(255, 255, 119, 0));
	  	zipShipping.setTypeface(null, Typeface.BOLD);
	  	zipShipping.setPadding(20, 20, 20, 20);
	  	tableRow.addView(zipShipping);
	  	
	  	regionShipping = new TextView(getApplicationContext());
	  	regionShipping.setText("REGION (SHIPPING)");
	  	regionShipping.setTextColor(Color.argb(255, 255, 119, 0));
	  	regionShipping.setTypeface(null, Typeface.BOLD);
	  	regionShipping.setPadding(20, 20, 20, 20);
	  	tableRow.addView(regionShipping);
	  	
	  	items = new TextView(getApplicationContext());
	  	items.setText("ITEM(S)");
	  	items.setTextColor(Color.argb(255, 255, 119, 0));
	  	items.setTypeface(null, Typeface.BOLD);
	  	items.setPadding(20, 20, 20, 20);
	  	tableRow.addView(items);
	  	
	  	costs = new TextView(getApplicationContext());
	  	costs.setText("TOTAL COSTS");
	  	costs.setTextColor(Color.argb(255, 255, 119, 0));
	  	costs.setTypeface(null, Typeface.BOLD);
	  	costs.setPadding(20, 20, 20, 20);
	  	tableRow.addView(costs);
	  	
	  	payment = new TextView(getApplicationContext());
	  	payment.setText("PAYMENT METHOD");
	  	payment.setTextColor(Color.argb(255, 255, 119, 0));
	  	payment.setTypeface(null, Typeface.BOLD);
	  	payment.setPadding(20, 20, 20, 20);
	  	tableRow.addView(payment);
	  	
	  	tableRow.setBackgroundColor(Color.argb(255, 51, 51, 51));
	  	
	  	tableLayout.addView(tableRow, tblParams);
	  	
	    for (Integer j = 0; j < count; j++)
	    {
	       tableRow = new TableRow(getApplicationContext());
	       
	       // From Customer Info List
	       nameGot = new TextView(getApplicationContext()); // --> NAME
	       nameGot.setText(c.getString(c.getColumnIndex(DatabaseTracker.CustomerList.NAME)));
	       nameGot.setTextColor(Color.argb(255, 0, 0, 0));
	       emailGot = new TextView(getApplicationContext()); // --> EMAIL
	       emailGot.setText(c.getString(c.getColumnIndex(DatabaseTracker.CustomerList.EMAIL)));
	       emailGot.setTextColor(Color.argb(255, 0, 0, 0));
	       phoneGot = new TextView(getApplicationContext()); // --> PHONE NUMBER
	       phoneGot.setText(c.getString(c.getColumnIndex(DatabaseTracker.CustomerList.PHONE)));
	       phoneGot.setTextColor(Color.argb(255, 0, 0, 0));
	       street1Got = new TextView(getApplicationContext()); // --> ADDRESS 1
	       street1Got.setText(c.getString(c.getColumnIndex(DatabaseTracker.CustomerList.STREET_1)));
	       street1Got.setTextColor(Color.argb(255, 0, 0, 0));
	       street2Got = new TextView(getApplicationContext()); // --> ADDRESS 2
	       street2Got.setText(c.getString(c.getColumnIndex(DatabaseTracker.CustomerList.STREET_2)));
	       street2Got.setTextColor(Color.argb(255, 0, 0, 0));
	       cityGot = new TextView(getApplicationContext()); // --> CITY
	       cityGot.setText(c.getString(c.getColumnIndex(DatabaseTracker.CustomerList.CITY)));
	       cityGot.setTextColor(Color.argb(255, 0, 0, 0));
	       countryGot = new TextView(getApplicationContext()); // --> COUNTRY
	       countryGot.setText(c.getString(c.getColumnIndex(DatabaseTracker.CustomerList.COUNTRY)));
	       countryGot.setTextColor(Color.argb(255, 0, 0, 0));
	       zipGot = new TextView(getApplicationContext()); // --> ZIP
	       zipGot.setText(c.getString(c.getColumnIndex(DatabaseTracker.CustomerList.ZIP_CODE)));
	       zipGot.setTextColor(Color.argb(255, 0, 0, 0));
	       regionGot = new TextView(getApplicationContext()); // --> REGION
	       regionGot.setText(c.getString(c.getColumnIndex(DatabaseTracker.CustomerList.REGION)));
	       regionGot.setTextColor(Color.argb(255, 0, 0, 0));
	       
	       // From Customer Info List for Shipping
	       nameShippingGot = new TextView(getApplicationContext()); // --> NAME
	       nameShippingGot.setText(c.getString(c.getColumnIndex(DatabaseTracker.CustomerList.SHIPPING_NAME)));
	       nameShippingGot.setTextColor(Color.argb(255, 0, 0, 0));
	       emailShippingGot = new TextView(getApplicationContext()); // --> EMAIL
	       emailShippingGot.setText(c.getString(c.getColumnIndex(DatabaseTracker.CustomerList.SHIPPING_EMAIL)));
	       emailShippingGot.setTextColor(Color.argb(255, 0, 0, 0));
	       phoneShippingGot = new TextView(getApplicationContext()); // --> PHONE NUMBER
	       phoneShippingGot.setText(c.getString(c.getColumnIndex(DatabaseTracker.CustomerList.SHIPPING_PHONE)));
	       phoneShippingGot.setTextColor(Color.argb(255, 0, 0, 0));
	       street1ShippingGot = new TextView(getApplicationContext()); // --> ADDRESS 1
	       street1ShippingGot.setText(c.getString(c.getColumnIndex(DatabaseTracker.CustomerList.SHIPPING_STREET_1)));
	       street1ShippingGot.setTextColor(Color.argb(255, 0, 0, 0));
	       street2ShippingGot = new TextView(getApplicationContext()); // --> ADDRESS 2
	       street2ShippingGot.setText(c.getString(c.getColumnIndex(DatabaseTracker.CustomerList.SHIPPING_STREET_2)));
	       street2ShippingGot.setTextColor(Color.argb(255, 0, 0, 0));
	       cityShippingGot = new TextView(getApplicationContext()); // --> CITY
	       cityShippingGot.setText(c.getString(c.getColumnIndex(DatabaseTracker.CustomerList.SHIPPING_CITY)));
	       cityShippingGot.setTextColor(Color.argb(255, 0, 0, 0));
	       countryShippingGot = new TextView(getApplicationContext()); // --> COUNTRY
	       countryShippingGot.setText(c.getString(c.getColumnIndex(DatabaseTracker.CustomerList.SHIPPING_COUNTRY)));
	       countryShippingGot.setTextColor(Color.argb(255, 0, 0, 0));
	       zipShippingGot = new TextView(getApplicationContext()); // --> ZIP
	       zipShippingGot.setText(c.getString(c.getColumnIndex(DatabaseTracker.CustomerList.SHIPPING_ZIP_CODE)));
	       zipShippingGot.setTextColor(Color.argb(255, 0, 0, 0));
	       regionShippingGot = new TextView(getApplicationContext()); // --> REGION
	       regionShippingGot.setText(c.getString(c.getColumnIndex(DatabaseTracker.CustomerList.SHIPPING_REGION)));
	       regionShippingGot.setTextColor(Color.argb(255, 0, 0, 0));
	       
	       
	       // Others by Expenses (TABLE 2 - CUSTOMER)
	       itemGot = new TextView(getApplicationContext()); // --> LIST OF ITEMS
	       itemGot.setText(c.getString(c.getColumnIndex(DatabaseTracker.CustomerList.LIST_OF_ITEMS)));
	       itemGot.setTextColor(Color.argb(255, 0, 0, 0));
	       costGot = new TextView(getApplicationContext()); // --> TOTAL COST
	       costGot.setText(c.getString(c.getColumnIndex(DatabaseTracker.CustomerList.TOTAL_COST)));
	       costGot.setTextColor(Color.argb(255, 0, 0, 0));
	       paymentGot = new TextView(getApplicationContext()); // --> PAYMENT
	       paymentGot.setText(c.getString(c.getColumnIndex(DatabaseTracker.CustomerList.PAYMENT_METHOD)));
	       paymentGot.setTextColor(Color.argb(255, 0, 0, 0));
	       
	       // Value Check
	       Log.v("COL VALUE BILLING", "_______________[ BILLING ROW " + j + " ]_______________");
	       Log.v("COL VALUE - NAME", nameGot.getText().toString());
	       Log.v("COL VALUE - EMAIL", emailGot.getText().toString());
	       Log.v("COL VALUE - STREET 1", street1Got.getText().toString());
	       Log.v("COL VALUE - STREET 2", street2Got.getText().toString());
	       Log.v("COL VALUE - PHONE", phoneGot.getText().toString());
	       Log.v("COL VALUE - CITY", cityGot.getText().toString());
	       Log.v("COL VALUE - ZIP", zipGot.getText().toString());
	       Log.v("COL VALUE - REGION", regionGot.getText().toString());
	       Log.v("COL VALUE - COUNTRY", countryGot.getText().toString());
	       Log.v("COL VALUE - ITEMS", itemGot.getText().toString());
	       Log.v("COL VALUE - COST", costGot.getText().toString());
	       Log.v("COL VALUE - PAYMENT", paymentGot.getText().toString());
	       Log.v("COL VALUE SHIPPING", "_______________[ SHIPPING ROW " + j + " ]_______________");
	       Log.v("COL VALUE - S. NAME", nameShippingGot.getText().toString());
	       Log.v("COL VALUE - S. EMAIL", emailShippingGot.getText().toString());
	       Log.v("COL VALUE - S. STREET 1", street1ShippingGot.getText().toString());
	       Log.v("COL VALUE - S. STREET 2", street2ShippingGot.getText().toString());
	       Log.v("COL VALUE - S. PHONE", phoneShippingGot.getText().toString());
	       Log.v("COL VALUE - S. CITY", cityShippingGot.getText().toString());
	       Log.v("COL VALUE - S. ZIP", zipShippingGot.getText().toString());
	       Log.v("COL VALUE - S. REGION", regionShippingGot.getText().toString());
	       Log.v("COL VALUE - S. COUNTRY", countryShippingGot.getText().toString());
	       
	       // Padding Settings
	       nameGot.setPadding(20, 20, 20, 20);
	       emailGot.setPadding(20, 20, 20, 20);
	       phoneGot.setPadding(20, 20, 20, 20);
	       street1Got.setPadding(20, 20, 20, 20);
	       street2Got.setPadding(20, 20, 20, 20);
	       cityGot.setPadding(20, 20, 20, 20);
	       countryGot.setPadding(20, 20, 20, 20);
	       zipGot.setPadding(20, 20, 20, 20);
	       regionGot.setPadding(20, 20, 20, 20);
	       nameShippingGot.setPadding(20, 20, 20, 20);
	       emailShippingGot.setPadding(20, 20, 20, 20);
	       phoneShippingGot.setPadding(20, 20, 20, 20);
	       street1ShippingGot.setPadding(20, 20, 20, 20);
	       street2ShippingGot.setPadding(20, 20, 20, 20);
	       cityShippingGot.setPadding(20, 20, 20, 20);
	       countryShippingGot.setPadding(20, 20, 20, 20);
	       zipShippingGot.setPadding(20, 20, 20, 20);
	       regionShippingGot.setPadding(20, 20, 20, 20);
	       itemGot.setPadding(20, 20, 20, 20);;
	       costGot.setPadding(20, 20, 20, 20);
	       paymentGot.setPadding(20, 20, 20, 20);
	       
	       tableRow.addView(nameGot);
	       tableRow.addView(emailGot);
	       tableRow.addView(phoneGot);
	       tableRow.addView(street1Got);
	       tableRow.addView(street2Got);
	       tableRow.addView(cityGot);
	       tableRow.addView(countryGot);
	       tableRow.addView(zipGot);
	       tableRow.addView(regionGot);
	       tableRow.addView(nameShippingGot);
	       tableRow.addView(emailShippingGot);
	       tableRow.addView(phoneShippingGot);
	       tableRow.addView(street1ShippingGot);
	       tableRow.addView(street2ShippingGot);
	       tableRow.addView(cityShippingGot);
	       tableRow.addView(countryShippingGot);
	       tableRow.addView(zipShippingGot);
	       tableRow.addView(regionShippingGot);
	       tableRow.addView(itemGot);
	       tableRow.addView(costGot);
	       tableRow.addView(paymentGot);
	       tableLayout.addView(tableRow, tblParams);
	       
	       c.moveToNext() ;
	    }
	    
//	    scroll.addView(tableLayout);
//	    databaseLayout.addView(scroll, params);
	    
//	    mainLayout.addView(databaseLayout);
	    mainLayout.addView(tableLayout);
//	    mainLayout.addView(buttonLayout);
	    
//	    setContentView(mainLayout);
	    sub.addView(mainLayout);
	    
		// Orientation
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
	     
	    dh.db.close();
	}
	
	
	
	
	
	public void deleteAll(View view)
	{
		dh = new DatabaseDbHelper(context);
		dh.db = dh.db;
		dh.deleteAllCustomers();
		
		Toast.makeText(context, "Database has been reset!", Toast.LENGTH_SHORT);
	}

	
	
	
	
	//TODO __________[ AsyncTask Method for Exporting Database ]__________
	public class ExportCSVDatabase extends AsyncTask<String, Void, Boolean>
	{
		private final ProgressDialog d = new ProgressDialog(context);
		private final DatabaseDbHelper dh = new DatabaseDbHelper(context);
		
		@Override
		protected void onPreExecute()
		{
			this.d.setMessage("Exporting database. Please stand by...");
			this.d.show();
		}
		
		@Override
		protected Boolean doInBackground(final String... args) 
		{
			File dbFile = getDatabasePath("CheckoutDatabase.db");
			System.out.println("Database name: " + dbFile);
			
			File exportDir = new File(Environment.getExternalStorageDirectory(), "");
			
			if(!exportDir.exists())
			{
				exportDir.mkdirs();
				dh.db =  dh.db;
			}
			
			File file = new File(exportDir, "List of Customer's Orders (Updated).csv");
			
			try
			{
				file.createNewFile();
				CSVWriter writeCSV = new CSVWriter(new FileWriter(file));
				
				Cursor c = dh.db.rawQuery(query, null);
				
				while(c.moveToNext())
				{
					// Get values on all columns per row. 
					String[] arrString =
					{
							c.getString(0),
							c.getString(1),
							c.getString(2),
							c.getString(3),
							c.getString(4),
							c.getString(5),
							c.getString(6),
							c.getString(7),
							c.getString(8),
							c.getString(9),
							c.getString(10)
					};
					
					System.out.println("------ NEW ROW -----");
					System.out.println(c.getString(0));
					System.out.println(c.getString(1));
					System.out.println(c.getString(2));
					System.out.println(c.getString(3));
					System.out.println(c.getString(4));
					System.out.println(c.getString(5));
					System.out.println(c.getString(6));
					System.out.println(c.getString(7));
					System.out.println(c.getString(8));
					System.out.println(c.getString(9));
					System.out.println(c.getString(10));
//					System.out.println(c.getString(11));
//					System.out.println(c.getString(12));
//					System.out.println(c.getString(13));
//					System.out.println(c.getString(14));
//					System.out.println(c.getString(15));
//					System.out.println(c.getString(16));
//					System.out.println(c.getString(17));
//					System.out.println(c.getString(18));
//					System.out.println(c.getString(19));
//					System.out.println(c.getString(20));
//					System.out.println(c.getString(21));
					
					// Preview the name of each column to see if exists within this table.
//					Log.v("COL 1", "" + c.getString(0));
//					Log.v("COL 2", "" + c.getString(1));
//					Log.v("COL 3", "" + c.getString(2));
//					Log.v("COL 4", "" + c.getString(3));
//					Log.v("COL 5", "" + c.getString(4));
//					Log.v("COL 6", "" + c.getString(5));
//					Log.v("COL 7", "" + c.getString(6));
//					Log.v("COL 8", "" + c.getString(7));
//					Log.v("COL 9", "" + c.getString(8));
//					Log.v("COL 10", "" + c.getString(9));
//					Log.v("COL 11", "" + c.getString(10));
					
					writeCSV.writeNext(arrString);
				}
				
				writeCSV.close();
				c.close();
				dh.db.close();
				
				this.d.dismiss();
				
				return true;
				
			} catch(SQLException e) {
				
				Log.e("AndroidStarter - SQL Status", e.getMessage(), e);
				return false;
				
			} catch(IOException e) {
				
				Log.e("AndroidStarter - I/O Status", e.getMessage(), e);
				return false;
				
			}
		}
		
		@Override
		protected void onPostExecute(Boolean success)
		{
			if(this.d.isShowing())
			{
				this.d.dismiss();
			}
			
			if (success)
	        {
	            Toast.makeText(PurchaseActivity.this, "Exporting database successful!", Toast.LENGTH_SHORT).show();

	        } else {

	            Toast.makeText(PurchaseActivity.this, "Exporting database failed.", Toast.LENGTH_SHORT).show();

	        }
		}
	}
	
	
	
	
	
	//TODO _______________[ Spinner Adapter Methods for Region List ]_______________
	private void setRegionList()
	{
		// Region List for Bill Information
		billRegion.setAdapter(new RegionSchema(context).getListOfRegions());
        billRegion.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() 
        {
			@Override
			public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) 
			{
				// Get the index and string value of each item.
				String value = billRegion.getItemAtPosition(i).toString();
				System.out.println(value);
				
				// When picked the right match, add it as values for the region. (Billing Information)
				if(value.equals("NCR (National Capital Region)"))
				{
					bc = "NCR (National Capital Region)";
					pre_target = "NCR";
					
				} else if(value.equals("CAR (Cordillera Administrive Region)")) {
					
					bc = "CAR (Cordillera Administrive Region)";
					pre_target = "Luzon";
					
				} else if(value.equals("Region I (Ilocos Region)")) {
					
					bc = "Region I (Ilocos Region)";
					pre_target = "Luzon";
					
				} else if(value.equals("Region II (Cagayan Valey)")) {
					
					bc = "Region II (Cagayan Valey)";
					pre_target = "Luzon";
					
				} else if(value.equals("Region III (Central Luzon)")) {
					
					bc = "Region III (Central Luzon)";
					pre_target = "Luzon";
					
				} else if(value.equals("Region IV-A (CALABARZON)")) {
					
					bc = "Region IV-A (CALABARZON)";
					pre_target = "Luzon";
					
				} else if(value.equals("Region IV-B (MIMAROPA)")) {
					
					bc = "Region IV-B (MIMAROPA)";
					pre_target = "Luzon";
					
				} else if(value.equals("Region V (Bicol Region)")) {
					
					bc = "Region V (Bicol Region)";
					pre_target = "Luzon";
					
				} else if(value.equals("Region VI (Western Visayas)")) {
					
					bc = "Region VI (Western Visayas)";
					pre_target = "Visayas";
					
				} else if(value.equals("Region VII (Central Visayas)")) {
					
					bc = "Region VII (Central Visayas)";
					pre_target = "Visayas";
					
				} else if(value.equals("Region VIII (Eastern Visayas)")) {
					
					bc = "Region VIII (Eastern Visayas)";
					pre_target = "Visayas";
					
				} else if(value.equals("Region IX (Zamboanga Peninsula)")) {
					
					bc = "Region IX (Zamboanga Peninsula)";
					pre_target = "Mindanao";
					
				} else if(value.equals("Region X (Northern Mindanao)")) {
					
					bc = "Region X (Northern Mindanao)";
					pre_target = "Mindanao";
					
				} else if(value.equals("Region XI (Davao Region)")) {
					
					bc = "Region XI (Davao Region)";
					pre_target = "Mindanao";
					
				} else if(value.equals("Region XII(SOCCSKSARGEN)")) {
					
					bc = "Region XII(SOCCSKSARGEN)";
					pre_target = "Mindanao";
					
				} else if(value.equals("Region XIII (Caraga)")) {
					
					bc = "Region XIII (Caraga)";
					pre_target = "Mindanao";
					
				} else if(value.equals("ARMM (Autonomous Region in Muslim Mindanao)")) {
					
					bc = "ARMM (Autonomous Region in Muslim Mindanao)";
					pre_target = "Mindanao";
					
				} else {
					
					bc = null;
					
				}
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) 
			{
				// ? ? ?
			}
        });
        
        // Region List for Shipping Information
        shipRegion.setAdapter(new RegionSchema(context).getListOfRegions());
        shipRegion.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() 
        {
			@Override
			public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) 
			{
				// Get the index and string value of each item.
				String value = billRegion.getItemAtPosition(i).toString();
				System.out.println(value);
				
				// When picked the right match, add it as values for the region. (Shipping Information)
				if(value.equals("NCR (National Capital Region)"))
				{
					sc = "NCR (National Capital Region)";
					target = "NCR";
					getShippingCost(Flag.shippingCostMap);
					
				} else if(value.equals("CAR (Cordillera Administrive Region)")) {
					
					sc = "CAR (Cordillera Administrive Region)";
					target = "Luzon";
					getShippingCost(Flag.shippingCostMap);
					
				} else if(value.equals("Region I (Ilocos Region)")) {
					
					sc = "Region I (Ilocos Region)";
					target = "Luzon";
					getShippingCost(Flag.shippingCostMap);
					
				} else if(value.equals("Region II (Cagayan Valey)")) {
					
					sc = "Region II (Cagayan Valey)";
					target = "Luzon";
					getShippingCost(Flag.shippingCostMap);
					
				} else if(value.equals("Region III (Central Luzon)")) {
					
					sc = "Region III (Central Luzon)";
					target = "Luzon";
					getShippingCost(Flag.shippingCostMap);
					
				} else if(value.equals("Region IV-A (CALABARZON)")) {
					
					sc = "Region IV-A (CALABARZON)";
					target = "Luzon";
					getShippingCost(Flag.shippingCostMap);
					
				} else if(value.equals("Region IV-B (MIMAROPA)")) {
					
					sc = "Region IV-B (MIMAROPA)";
					target = "Luzon";
					getShippingCost(Flag.shippingCostMap);
					
				} else if(value.equals("Region V (Bicol Region)")) {
					
					sc = "Region V (Bicol Region)";
					target = "Luzon";
					getShippingCost(Flag.shippingCostMap);
					
				} else if(value.equals("Region VI (Western Visayas)")) {
					
					sc = "Region VI (Western Visayas)";
					target = "Visayas";
					getShippingCost(Flag.shippingCostMap);
					
				} else if(value.equals("Region VII (Central Visayas)")) {
					
					sc = "Region VII (Central Visayas)";
					target = "Visayas";
					getShippingCost(Flag.shippingCostMap);
					
				} else if(value.equals("Region VIII (Eastern Visayas)")) {
					
					sc = "Region VIII (Eastern Visayas)";
					target = "Visayas";
					getShippingCost(Flag.shippingCostMap);
					
				} else if(value.equals("Region IX (Zamboanga Peninsula)")) {
					
					sc = "Region IX (Zamboanga Peninsula)";
					target = "Mindanao";
					getShippingCost(Flag.shippingCostMap);
					
				} else if(value.equals("Region X (Northern Mindanao)")) {
					
					sc = "Region X (Northern Mindanao)";
					target = "Mindanao";
					getShippingCost(Flag.shippingCostMap);
					
				} else if(value.equals("Region XI (Davao Region)")) {
					
					sc = "Region XI (Davao Region)";
					target = "Mindanao";
					getShippingCost(Flag.shippingCostMap);
					
				} else if(value.equals("Region XII(SOCCSKSARGEN)")) {
					
					sc = "Region XII(SOCCSKSARGEN)";
					target = "Mindanao";
					getShippingCost(Flag.shippingCostMap);
					
				} else if(value.equals("Region XIII (Caraga)")) {
					
					sc = "Region XIII (Caraga)";
					target = "Mindanao";
					getShippingCost(Flag.shippingCostMap);
					
				} else if(value.equals("ARMM (Autonomous Region in Muslim Mindanao)")) {
					
					sc = "ARMM (Autonomous Region in Muslim Mindanao)";
					target = "Mindanao";
					getShippingCost(Flag.shippingCostMap);
					
				} else {
					
					sc = null;
					target = null;
					
				}
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) 
			{
				// ? ? ?
				
			}
        });
	}


	
	
	
	//TODO _______________[ Shipping Cost ]_______________
	private void getShippingCost(String json)
	{
		try
        {
            JSONObject jObject = new JSONObject(json); // --> Get the result as constructor for JSON file.
            
            for(int i = 0; i < 1; i++)
            {
            	// Pick the value or shipping cost according to the region.
            	if(target.equalsIgnoreCase("NCR"))
            	{
            		Log.v("DESTINATION TARGET", jObject.getString("ncr"));
            		shippingCost.setText(jObject.getString("ncr"));
            		
            	} else if(target.equalsIgnoreCase("Luzon")) {
            		
            		Log.v("DESTINATION TARGET", jObject.getString("luzon"));
            		shippingCost.setText(jObject.getString("luzon"));
            		
            	} else if(target.equalsIgnoreCase("Visayas")) {
            		
            		Log.v("DESTINATION TARGET", jObject.getString("visayas"));
            		shippingCost.setText(jObject.getString("visayas"));
            		
            	} else if(target.equalsIgnoreCase("Mindanao")) {
            		
            		Log.v("DESTINATION TARGET", jObject.getString("mindanao"));
            		shippingCost.setText(jObject.getString("mindanao"));
            		
            	}
            	
            	// Then, compute now for grandtotal
            	grandtotal.setText("₱ " + String.valueOf(Integer.parseInt(shippingCost.getText().toString()) + Integer.parseInt(subtotal.getText().toString())));
            }
             
        } catch (Exception e) {
        	
            e.printStackTrace();
            
        }
	}
}
