package com.openovate.galleon;

import android.app.Activity;
import android.os.Bundle;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.GridView;
import android.widget.Toast;

import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnRefreshListener2;
import com.handmark.pulltorefresh.library.PullToRefreshGridView;
import com.openovate.galleon.Model.URLFinder.JSONSourceURLFinder;
import com.openovate.galleon.jsonParser.utils.TestAsync2;

public class TestActivity extends Activity
{
	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		webViewTest();
	}
	
	
	
	
	
	private void webViewTest()
	{
		getWindow().requestFeature(Window.FEATURE_PROGRESS);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
		setContentView(R.layout.activity_main);
		final Activity activity = this;
		
		WebView web = (WebView) findViewById(R.id.WV_Sample);
		
		web.setWebChromeClient(new WebChromeClient()
		{
			@Override
			public void onProgressChanged(WebView view, int newProgress) 
			{
				super.onProgressChanged(view, newProgress);
				activity.setProgress(newProgress * 1000);
			}
		});
		
		web.setWebViewClient(new WebViewClient()
		{
			@Override
			public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) 
			{
				super.onReceivedError(view, errorCode, description, failingUrl);
				Toast.makeText(activity, "Oh no! " + description, Toast.LENGTH_SHORT).show();
			}
		});
		
		web.loadUrl("https://www.facebook.com/Galleon.ph");
	}
	
	
	
	
	
	private void pullToRefrshTest()
	{
		final JSONSourceURLFinder url = new JSONSourceURLFinder();
		
		final PullToRefreshGridView gv = new PullToRefreshGridView(getApplicationContext());
		gv.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
//		gv.setOrientation(PullToRefreshGridView.HORIZONTAL);
		gv.setOnRefreshListener(new OnRefreshListener2<GridView>() 
		{
			@Override
			public void onPullDownToRefresh(PullToRefreshBase<GridView> refreshView) 
			{
				Toast.makeText(getApplicationContext(), "Refresh Down: Now loading...", Toast.LENGTH_LONG).show();
			}

			@Override
			public void onPullUpToRefresh(PullToRefreshBase<GridView> refreshView) 
			{
//				new TestAsync2(url.getURL3(0, 0), gv, getApplicationContext()).execute();
				Toast.makeText(getApplicationContext(), "Refresh Up: Now loading...", Toast.LENGTH_LONG).show();
			}
		});

		new TestAsync2(url.getURL2(0, 0), gv, getApplicationContext()).execute();
		
		setContentView(gv);
	}
}
