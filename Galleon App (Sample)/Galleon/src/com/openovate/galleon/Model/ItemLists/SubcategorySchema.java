package com.openovate.galleon.Model.ItemLists;

import android.content.Context;
import android.widget.ArrayAdapter;

import com.openovate.galleon.R;

public class SubcategorySchema 
{
	//TODO _______________[ Array Adapters ]_______________
	// Categories
	private ArrayAdapter<CharSequence> choiceAdapter1; // --> TOYS, KIDS, & BABY
	private ArrayAdapter<CharSequence> choiceAdapter2; // --> SPORTS & OUTDOORS
	private ArrayAdapter<CharSequence> choiceAdapter3; // --> CLOTHING, SHOES, & JEWELRY
	private ArrayAdapter<CharSequence> choiceAdapter4; // --> ELECTRONICS AND COMPUTERS
	private ArrayAdapter<CharSequence> choiceAdapter5; // --> HOME, GARDEN, AND TOOLS
	
	// Subcategories per Group
	private ArrayAdapter<CharSequence> subAdapter1_1; // --> TOYS & GAMES (TOYS, KIDS, & BABY)
	private ArrayAdapter<CharSequence> subAdapter2_1; // --> HUNTING AND FISHING (SPORTS AND OUTDOORS)
	private ArrayAdapter<CharSequence> subAdapter3_1; // --> WATCHES (CLOTHING, SHOES, & JEWELRY)
	private ArrayAdapter<CharSequence> subAdapter3_2; // --> ACCESSORIES (CLOTHING, SHOES, & JEWELRY)
	private ArrayAdapter<CharSequence> subAdapter4_1; // --> MP3 PLAYERS & ACCESSORIES (ELECTRONICS AND COMPUTERS)
	private ArrayAdapter<CharSequence> subAdapter4_2; // --> HOME AUDIO & HOME THEATER (ELECTRONICS AND COMPUTERS)
	private ArrayAdapter<CharSequence> subAdapter5_1; // --> KITCHEN & DINING (HOME, GARDEN, AND TOOLS)
	private ArrayAdapter<CharSequence> subAdapter5_2; // --> PATIO, LAWN, & GARDEN (HOME, GARDEN, AND TOOLS)
	
	
	
	
	
	//TODO _______________[ Constructor ]_______________
	public SubcategorySchema(final Context context)
	{
		// Subcategory Within the Category
		choiceAdapter1 = ArrayAdapter.createFromResource(context, R.array.subcategory_1, R.layout.category_spinner_layout);
		choiceAdapter2 = ArrayAdapter.createFromResource(context, R.array.subcategory_2, R.layout.category_spinner_layout);
		choiceAdapter3 = ArrayAdapter.createFromResource(context, R.array.subcategory_3, R.layout.category_spinner_layout);
		choiceAdapter4 = ArrayAdapter.createFromResource(context, R.array.subcategory_4, R.layout.category_spinner_layout);
		choiceAdapter5 = ArrayAdapter.createFromResource(context, R.array.subcategory_5, R.layout.category_spinner_layout);
        
        // Subcategory Within the Subcategory
		subAdapter1_1 = ArrayAdapter.createFromResource(context, R.array.subcategory_1_1, R.layout.subcategory_spinner_layout);
		subAdapter2_1 = ArrayAdapter.createFromResource(context, R.array.subcategory_2_1, R.layout.subcategory_spinner_layout);
		subAdapter3_1 = ArrayAdapter.createFromResource(context, R.array.subcategory_3_1, R.layout.subcategory_spinner_layout);
		subAdapter3_2 = ArrayAdapter.createFromResource(context, R.array.subcategory_3_2, R.layout.subcategory_spinner_layout);
		subAdapter4_1 = ArrayAdapter.createFromResource(context, R.array.subcategory_4_1, R.layout.subcategory_spinner_layout);
		subAdapter4_2 = ArrayAdapter.createFromResource(context, R.array.subcategory_4_2, R.layout.subcategory_spinner_layout);
		subAdapter5_1 = ArrayAdapter.createFromResource(context, R.array.subcategory_5_1, R.layout.subcategory_spinner_layout);
		subAdapter5_2 = ArrayAdapter.createFromResource(context, R.array.subcategory_5_2, R.layout.subcategory_spinner_layout);
	}
	
	
	
	
	
	//TODO _______________[ Array Adapter Getters for Categories ]_______________
	public ArrayAdapter<CharSequence> getListOfCChoices1()
	{
		return choiceAdapter1;
	}
	
	public ArrayAdapter<CharSequence> getListOfCChoices2()
	{
		return choiceAdapter2;
	}
	
	public ArrayAdapter<CharSequence> getListOfCChoices3()
	{
		return choiceAdapter3;
	}
	
	public ArrayAdapter<CharSequence> getListOfCChoices4()
	{
		return choiceAdapter4;
	}
	
	public ArrayAdapter<CharSequence> getListOfCChoices5()
	{
		return choiceAdapter5;
	}
	
	
	
	
	
	//TODO _______________[ Array Adapter Getters for Subcategories per Group ]_______________
	public ArrayAdapter<CharSequence> getSublistOfCChoices1()
	{
		return subAdapter1_1;
	}
	
	public ArrayAdapter<CharSequence> getSublistOfCChoices2()
	{
		return subAdapter2_1;
	}
	
	public ArrayAdapter<CharSequence> getSublistOfCChoices3(int subGroupNumber)
	{
		if(subGroupNumber == 2)
		{
			return subAdapter3_2;
			
		} else {
			
			return subAdapter3_1;
			
		}
	}
	
	public ArrayAdapter<CharSequence> getSublistOfCChoices4(int subGroupNumber)
	{
		if(subGroupNumber == 2)
		{
			return subAdapter4_2;
			
		} else {
			
			return subAdapter4_1;
			
		}
	}
	
	public ArrayAdapter<CharSequence> getSublistOfCChoices5(int subGroupNumber)
	{
		if(subGroupNumber == 2)
		{
			return subAdapter5_2;
			
		} else {
			
			return subAdapter5_1;
			
		}
	}
}
