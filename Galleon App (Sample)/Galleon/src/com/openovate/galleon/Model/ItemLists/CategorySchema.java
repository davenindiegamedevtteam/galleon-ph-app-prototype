package com.openovate.galleon.Model.ItemLists;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.widget.ArrayAdapter;

import com.openovate.galleon.R;

public class CategorySchema 
{
	//TODO _______________[ Field Objects ]_______________
	private ArrayAdapter<CharSequence> choiceAdapter;
	private ArrayAdapter<CharSequence> choiceAdapter2;
	
	
	
	
	
	//TODO _______________[ Array Adapter Getters ]_______________
	public CategorySchema(final Context context)
	{
		choiceAdapter = ArrayAdapter.createFromResource(context, R.array.list_of_categories, R.layout.category_spinner_layout);
		choiceAdapter2 = ArrayAdapter.createFromResource(context, R.array.list_of_categories_alt, R.layout.subcategory_spinner_layout);
	}
	
	
	
	
	
	//TODO _______________[ Adapter Getter ]_______________
	public ArrayAdapter<CharSequence> getListOfCategories(int type)
	{
		if(type == 1)
		{
			return choiceAdapter;
			
		} else {
			
			return choiceAdapter2;
			
		}
	}
}
