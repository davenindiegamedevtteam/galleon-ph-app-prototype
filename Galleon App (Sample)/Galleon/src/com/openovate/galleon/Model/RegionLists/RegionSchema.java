package com.openovate.galleon.Model.RegionLists;

import android.content.Context;
import android.widget.ArrayAdapter;

import com.openovate.galleon.R;

public class RegionSchema 
{
	//TODO _______________[ Field Objects ]_______________
	private ArrayAdapter<CharSequence> region;
	
	
	
	
	
	//TODO _______________[ Array Adapter Getters ]_______________
	public RegionSchema(final Context context)
	{
		region = ArrayAdapter.createFromResource(context, R.array.region, R.layout.purchase_spinner_layout);
	}
	
	
	
	
	
	//TODO _______________[ Adapter Getter ]_______________
	public ArrayAdapter<CharSequence> getListOfRegions()
	{
		return region;
	}
}
