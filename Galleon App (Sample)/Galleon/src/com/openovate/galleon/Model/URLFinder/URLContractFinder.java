package com.openovate.galleon.Model.URLFinder;

public class URLContractFinder 
{
	/**
	 * 
	 * Returns a string value of the URL type for fetching the JSON file where <br>
	 * an Integer value is: <br><br>
	 * 
	 * 1 --> category<br>
	 * 2 --> search<br>
	 * 3 --> browse random<br>
	 * 4 --> browse by page<br>
	 * 5 --> recommended
	 * 
	 * <br>
	 * Also, second parameter is a String where it used for searching a keyword. <br>
	 * Only for index no. 2.
	 * 
	 */
	public static String GET_URL_TYPE(int index, String keyword)
	{
		switch(index)
		{
		case 1: return "&type=category"; 
		case 2: return "&type=search&keyword=" + keyword; 
		case 3: return "&type=browse_random"; 
		case 4: return "&type=by_page"; 
		case 5: return "&type=recommended"; 
		default: return null;
		}
	}
	
	/**
	 * 
	 * Returns a string value of the URL type for fetching the JSON file where <br>
	 * an Integer value is: <br><br>
	 * 
	 * 1 --> category<br>
	 * 2 --> search<br>
	 * 3 --> browse random<br>
	 * 4 --> browse by page<br>
	 * 5 --> recommended
	 * 
	 */
	public static String GET_URL_TYPE(int index)
	{
		switch(index)
		{
		case 1: return "&type=category"; 
		case 2: return "&type=search&keyword=a"; 
		case 3: return "&type=browse_random"; 
		case 4: return "&type=by_page"; 
		case 5: return "&type=recommended"; 
		default: return null;
		}
	}
	
	/**
	 * 
	 * Add this at the end of <B>GET_SEARCH_URL</B>.
	 * 
	 */
	public static String getKeyword(String keyword, int page)
	{
		return keyword + "/" + page;
	}
	
	/**
	 * 
	 * Add this at the end of <B>GET_BROWSE_BY_PAGE_URL</B>.
	 * 
	 */
	public static String getBrowseByPageNumber(int page, int max)
	{
		return page + "/" + max;
	}
	
	
	
	
	
	// REST URL Fragments
	private static final String BASE_REST_URL = "http://rest.galleon.ph/";
	private static final String API_KEY = "ea490445da70c033fbbbfbf43344b577/";
	private static final String API_SECRET = "458b97edec20da929b1141f9053fb978/";
	private static final String GET_CATEGORY = "get/category/";
	private static final String GET_POPULAR = "get/popular/";
	private static final String GET_SEARCH = "search/product/";
	private static final String GET_BROWSE = "browse/random/";
	private static final String GET_BROWSE_BY_PAGE = "browse/bypage/";
	private static final String GET_SHIPMENT = "post/shipment/";
	private static final String GET_LIMIT = "15"; // --> Maximum is 15, minimum is 1.
	
	/*
	 * 
	 * 			CATEGORY      --> http://rest.galleon.ph/get/category/ea490445da70c033fbbbfbf43344b577/458b97edec20da929b1141f9053fb978/
	 * 			BROWSE RANDOM --> http://rest.galleon.ph/browse/random/ea490445da70c033fbbbfbf43344b577/458b97edec20da929b1141f9053fb978/15
	 * 
	 */
	
	// REST URL Page
	public static final String GET_COMPLETE_URL = BASE_REST_URL + API_KEY + "&" + API_SECRET; // --> Deprecated
	public static final String GET_CATEGORY_URL = BASE_REST_URL + GET_CATEGORY + API_KEY + API_SECRET;
	public static final String GET_POPULAR_URL = BASE_REST_URL + GET_POPULAR + API_KEY + API_SECRET;
	public static final String GET_SEARCH_URL = BASE_REST_URL + GET_SEARCH + API_KEY + API_SECRET;
	public static final String GET_BROWSE_URL = BASE_REST_URL + GET_BROWSE + API_KEY + API_SECRET + GET_LIMIT;
	public static final String GET_BROWSE_BY_PAGE_URL = BASE_REST_URL + GET_BROWSE_BY_PAGE + API_KEY + API_SECRET;
	public static final String GET_SHIPMENT_POST = BASE_REST_URL + GET_SHIPMENT + API_KEY + API_SECRET;
	
	// Share URL Page
	public static final String GET_FACEBOOK_URL = "https://www.facebook.com/Galleon.ph";
	public static final String GET_TWITTER_URL = "https://twitter.com/galleonph";
}
