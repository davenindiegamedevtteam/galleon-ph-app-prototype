package com.openovate.galleon.Model.URLFinder;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Iterator;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.HttpVersion;
import org.apache.http.message.BasicHttpResponse;
import org.json.JSONObject;

import com.jess.ui.TwoWayGridView;
import com.openovate.galleon.ProjectGalleonMainActivity;
import com.openovate.galleon.jsonParser.adapters.ProductListAdapter2;
import com.openovate.galleon.jsonParser.models.FeatureModel;
import com.openovate.galleon.jsonParser.models.ProductModel2;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

public class JSONSourceURLFinder 
{
	//TODO _______________[ Field Objects ]_______________
	private int page = 1;
	private int number_of_pages = 0;
	private String keyword = "";
	
	
	
	
	
	//TODO _______________[ Constructor ]_______________
	public JSONSourceURLFinder()
	{
		// ? ? ?
	}
	
	
	
	
	
	//TODO _______________[ JSON Source URL's String Model ]_______________
	private String test[] = // --> For testing purposes only.
	{
		"1", "2", "3", "4"
	};
	
	private static String JSONSourcePopular = URLContractFinder.GET_POPULAR_URL; // --> Popular Search Results/Most Recommended
	private static String JSONSourceBrowse = URLContractFinder.GET_BROWSE_URL; // Browse Products
	
	private String   JSONSource0 = ""; // --> ALL
	private String[][] JSONSource1 = new String[7][25]; // --> TOYS, KIDS, & BABY
	private String[][] JSONSource2 = new String[5][24]; // --> SPORTS & OUTDOORS
	private String[][] JSONSource3 = new String[3][15]; // --> CLOTHING, SHOES, AND JEWELRY
	private String[][] JSONSource4 = new String[6][49]; // --> ELECTRONICS AND COMPUTERS
	private String[][] JSONSource5 = new String[5][8]; // --> HOME, GARDEN, AND TOOLS
	
	
	
	
	
	//TODO _______________[ JSON Source URL's Page Getter(s) ]_______________
	public static String getPopularSearchURL()
	{
		return JSONSourcePopular;
	}
	
	public static String getPopularSearchURL(int limit)
	{
		return JSONSourcePopular + String.valueOf(limit);
	}
	
	public static String getBrowseURL()
	{
		return JSONSourceBrowse;
	}
	
	
	
	
	
	
	//TODO _______________[ JSON Source URL's Page Update Method(s) ]_______________
	private void updateURL0(int number_of_pages, String keyword)
	{
		System.out.println("URL --> " + URLContractFinder.GET_SEARCH_URL + URLContractFinder.getKeyword(keyword, number_of_pages));
		JSONSource0 = URLContractFinder.GET_SEARCH_URL + URLContractFinder.getKeyword(keyword, number_of_pages);
	}
	
	private void updateURL1()
	{
		for(int i = 0; i < 7; i++)
		{
			for(int si = 0; si < 25; si++)
			{
				if(i == 0) // --> Subcategory: All
				{
					String shit = "B & B";
					System.out.println(shit);
					JSONSource1[i][si] = "http://www.galleon.ph/category/14/?json&page=" + Integer.toString(si + 1);
					
				} else if(i == 1) {
					
					if(si < 21) // --> Subcategory: Toys and Games
					{
						JSONSource1[i][si] = "http://www.galleon.ph/category/15/toys-games?json&page=" + Integer.toString(si + 1);
					}
					
				} else if(i == 2) {
					
					if(si < 1) // --> Subcategory: RC Toys
					{
						JSONSource1[i][si] = "http://www.galleon.ph/category/16/toy-remote-control-play-vehicles?json&page=" + Integer.toString(si + 1);
					}
					
				} else if(i == 3) {
					
					if(si < 16) // --> Subcategory: Action and Toy Figures
					{
						JSONSource1[i][si] = "http://www.galleon.ph/category/17/action-toy-figures?json&page=" + Integer.toString(si + 1);
					}
					
				} else if(i == 4) {
					
					if(si < 2) // --> Subcategory: Lego Blocks
					{
						JSONSource1[i][si] = "http://www.galleon.ph/category/77/lego?json&page=" + Integer.toString(si + 1);
					}
					
				} else if(i == 5) {
					
					if(si < 1) // --> Subcategory: Robots
					{
						JSONSource1[i][si] = "http://www.galleon.ph/category/80/robots?json&page=" + Integer.toString(si + 1);
					}
					
				} else if(i == 6) {
					
					if(si < 1) // --> Subcategory: Video Games for Kids (Playstation 3)
					{
						JSONSource1[i][si] = "http://www.galleon.ph/category/52/playstation-3?json&page=" + Integer.toString(si + 1);
					}
					
				}
			}
		}
	}
	
	private void updateURL2()
	{
		for(int i = 0; i < 5; i++)
		{
			for(int si = 0; si < 24; si++)
			{
				if(i == 0) // --> Subcategory: All
				{
					JSONSource2[i][si] = "http://www.galleon.ph/category/18/sports-outdoors?json&page=" + Integer.toString(si + 1);
					
				} else if(i == 1) { // --> Subcategory: Cycling (Bike Tools)
					
					if(si < 1)
					{
						JSONSource2[i][si] = "http://www.galleon.ph/category/20/bike-tools-maintenance?json&page=" + Integer.toString(si + 1);
					}
					
				} else if(i == 2) { // --> Subcategory: Hunting and Fishing
					
					if(si < 1)
					{
						JSONSource2[i][si] = "http://www.galleon.ph/category/45/hunting-fishing?json&page=" + Integer.toString(si + 1);
					}
					
				} else if(i == 3) { // --> Subcategory: Air Guns
					
					if(si < 1)
					{
						JSONSource2[i][si] = "http://www.galleon.ph/category/46/air-guns?json&page=" + Integer.toString(si + 1);
					}
					
				} else if(i == 4) { // --> Subcategory: Gun Accessories
					
					if(si < 1)
					{
						JSONSource2[i][si] = "http://www.galleon.ph/category/47/gun-accessories?json&page=" + Integer.toString(si + 1);
					}
					
				}
			}
		}
	}
	
	private void updateURL3()
	{
		for(int i = 0; i < 3; i++)
		{
			for(int si = 0; si < 15; si++)
			{
				if(i == 0) // --> Subcategory: All
				{
					JSONSource3[i][si] = "http://www.galleon.ph/category/21/clothing-shoes-jewelry?json&page=" + Integer.toString(si + 1);
					
				} else if(i == 1) { // --> Subcategory: Watches (Sports)
					
					if(si < 1)
					{
						JSONSource3[i][si] = "www://www.galleon.ph/category/23/sport?json&page=" + Integer.toString(si + 1);
					}
					
				} else if(i == 2) { // --> Accessories (Men)
					
					if(si < 2)
					{
						JSONSource3[i][si] = "www://www.galleon.ph/category/25/men?json&page=" + Integer.toString(si + 1);
					}
					
				}
			}
		}
	}
	
	private void updateURL4()
	{
		for(int i = 0; i < 6; i++)
		{
			for(int si = 0; si < 49; si++)
			{
				if(i == 0) // --> Subcategory: All
				{
					JSONSource4[i][si] = "http://www.galleon.ph/category/26/electronics-computers?json&page=" + Integer.toString(si + 1);
					
				} else if(i == 1) { // --> Subcategory: MP3 Players & Accessories (Accessories)
					
					if(si == 0)
					{
						JSONSource4[i][si] = "http://www.galleon.ph/category/28/accessories?json";
					}
					
				} else if(i == 2) { // --> Subcategory: Home Audio & Home Theater (All)
					
					if(si < 2) // --> Where this condition tells to stop at the last page.
					{
						JSONSource4[i][si] = "http://www.galleon.ph/category/32/home-audio-home-theater?json&page=" + Integer.toString(si + 1);
					}
					
				} else if(i == 3) { // --> Subcategory: Home Audio & Home Theater (Headphones)
					
					if(si == 0)
					{
						JSONSource4[i][si] = "http://www.galleon.ph/category/33/headphones?json&page=" + Integer.toString(si + 1);
					}
					
				} else if(i == 4) { // --> Subcategory: Home Audio & Home Theater (Subwoofer)
					
					if(si < 1)
					{
						JSONSource4[i][si] = "http://www.galleon.ph/category/76/subwoofer?json&page=" + Integer.toString(si + 1);
					}
					
				} else if(i == 5) { // --> Subcategory: Home Audio & Home Theater (Speaker System)
					
					if(si < 1)
					{
						JSONSource4[i][si] = "http://www.galleon.ph/category/76/subwoofer?json&page=" + Integer.toString(si + 1);
					}
					
				}
			}
		}
	}
	
	private void updateURL5()
	{
		for(int i = 0; i < 5; i++)
		{
			for(int si = 0; si < 8; si++)
			{
				if(i == 0) // Subcategories: All
				{
					JSONSource5[i][si] = "http://www.galleon.ph/category/29/home-garden-tools?json&page=" + Integer.toString(si + 1);
					
				} else if(i == 1) { // Subcategories: Kitchen and Dining (All)
					
					if(si < 5)
					{
						JSONSource5[i][si] = "http://www.galleon.ph/category/30/kitchen-dining?json&page=" + Integer.toString(si + 1);
					}
					
				} else if(i == 2) { // Subcategories: Kitchen and Dining (Cookware)
					
					if(si < 2)
					{
						JSONSource5[i][si] = "http://www.galleon.ph/category/31/cookware?json&page=" + Integer.toString(si + 1);
					}
					
				} else if(i == 3) { // Subcategories: Patio, Lawn, and Garden (All)
					
					if(si < 1)
					{
						JSONSource5[i][si] = "http://www.galleon.ph/category/34/patio-lawn-garden?json&page=" + Integer.toString(si + 1);
					}
					
				} else if(i == 4) { // Subcategories: Patio, Lawn, and Garden (Grills and Outdooor Cooking)
					
					if(si < 1)
					{
						JSONSource5[i][si] = "http://www.galleon.ph/category/35/grills-outdoor-cooking?json&page=" + Integer.toString(si + 1);
					}
					
				}
			}
		}
	}
	
	
	
	
	
	//TODO _______________[ JSON Source URL Category Getters ]_______________
	public String getURL0(int number_of_pages, String keyword) // --> SEARCH RESULTS
	{
		this.number_of_pages = number_of_pages;
		this.keyword = keyword;
		
		Log.v("URL 0 STATUS", "Successfully loaded. All categories displayed.");
		updateURL0(this.number_of_pages, this.keyword);
		return JSONSource0;
	}
	
	public String getURL1(int index, int subindex) // --> TOYS, KIDS, & BABY
	{
		Log.v("URL 1 STATUS", "Successfully loaded.");
		updateURL1();
		return JSONSource1[index][subindex];
	}
	
	public String getURL2(int index, int subindex) // --> SPORTS & OUTDOORS
	{
		Log.v("URL 2 STATUS", "Successfully loaded.");
		updateURL2();
		return JSONSource2[index][subindex];
	}
	
	public String getURL3(int index, int subindex) // --> SPORTS & OUTDOORS
	{
		Log.v("URL 3 STATUS", "Successfully loaded.");
		updateURL3();
		return JSONSource3[index][subindex];
	}
	
	public String getURL4(int index, int subindex) // --> ELECTRONICS AND COMPUTERS
	{
		Log.v("URL 4 STATUS", "Successfully loaded.");
		updateURL4();
		return JSONSource4[index][subindex];
	}
	
	public String getURL5(int index, int subindex) // --> HOME, GARDEN, & TOOLS
	{
		Log.v("URL 5 STATUS", "Successfully loaded.");
		updateURL5();
		return JSONSource5[index][subindex];
	}
	
	
	
	
	
	public void updatePage(int number_of_pages, String keyword)
	{
		this.number_of_pages = number_of_pages;
		this.keyword = keyword;
		updateURL0(this.number_of_pages, this.keyword);
	}
	
	
	
	
	
	//TODO _______________[ JSON Category Index Getter ]_______________
	public int length(int index, int categoryNumber)
	{
		try
		{
			if(categoryNumber == 1)
			{
				Log.v("URL 1 STATUS", "Updated.");
				updateURL1();
				return JSONSource1[index].length;
				
			} else if(categoryNumber == 2) {
				
				Log.v("URL 2 STATUS", "Updated.");
				updateURL2();
				return JSONSource2[index].length;
				
			} else if(categoryNumber == 3) {
				
				Log.v("URL 3 STATUS", "Updated.");
				updateURL3();
				return JSONSource3[index].length;
				
			} else if(categoryNumber == 4) {
				
				Log.v("URL 4 STATUS", "Updated.");
				updateURL4();
				return JSONSource4[index].length;
				
			} else if(categoryNumber == 5) {
				
				Log.v("URL 5 STATUS", "Updated.");
				updateURL5();
				return JSONSource5[index].length;
				
			} else {
				
				Log.v("URL 0 STATUS", "Updated.");
				updateURL0(number_of_pages, keyword);
//				return JSONSource0.length;
				
			}
			
		} catch(ArrayIndexOutOfBoundsException e) {
			
			Log.v("CATEGORY INDEX STATUS", "Index out of bounds, return to default point. (All categories)");
			
		}
		
		return 0;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	//TODO _______________[ JSON Source URL Finder Subclass - Category Async Task ]_______________
//	public class getParentCategory extends AsyncTask<String, String, String> 
//	{
//		//TODO _______________[ Field Objects ]_______________
//	    private String url;
//	    private JSONObject jObject;
//
//	    
//	    
//	    
//	    
//	    //TODO _______________[ Constructor ]_______________
//	    /**
//	     * <b>INFO:</b> The constructor of the parent category URL.
//	     * 
//	     * @param url = String value for the JSON file. (URL)
//	     */
//	    public getParentCategory(String url)
//	    {
//	        this.url = url;
//	    }
//
//	    
//	    
//	    
//	    
//	    //TODO _______________[ Async Task Overriden Method(s) ]_______________
//	    @Override
//	    protected String doInBackground(String... parameters)
//	    {
//	        try 
//	        {
//	        	// Fetch the URL first and buffer it.
//	            URL dataSource = new URL(url);
//	            URLConnection dSCon = dataSource.openConnection();
//	            BufferedReader in = new BufferedReader(new InputStreamReader(dSCon.getInputStream()));
//	            String line;
//	            String str = "";
//
//	            // Check if line is empty.
//	            while ((line = in.readLine()) != null) 
//	            {
//	                str += line;
//	            }
//	            
//	            // Check the server connection status.
//	            HttpResponse response = new BasicHttpResponse(HttpVersion.HTTP_1_1, 
//	            HttpStatus.SC_OK, "OK");
//	            System.out.println(response.getProtocolVersion());
//	            System.out.println(response.getStatusLine().getStatusCode());
//	            System.out.println(response.getStatusLine().getReasonPhrase());
//	            System.out.println(response.getStatusLine().toString());
//
//	            return str;
//	            
//	        } catch (Exception e) {
//	        	
//	            e.printStackTrace();
//	            return e.getMessage();
//	            
//	        }
//	    }
//
//		@SuppressWarnings("unchecked")
//		@Override
//	    protected void onPostExecute(String result) // --> String str
//	    {
//	        try
//	        {
//	            jObject = new JSONObject(result); // --> Get the result as constructor for JSON file.
//	            
//	            //for(int i=0; i<(jObject.length()); i++) // --> Do the loop until all the contents are filled in the list.
//	            if(index)
//	            {
//	            	boolean stop = false;
//	            	int flag = 0;
//	            	
//	            	for(Iterator<String> iter = jObject.keys(); iter.hasNext();)
//	                {
//	                	String key = iter.next();
//	                	{
//	                		temp = new ProductModel2(jObject.getJSONObject(key).getString("product_id"));
//	                		temp.setProductId(jObject.getJSONObject(key).getString("product_id"));
//	                        temp.setProductImageUrl(jObject.getJSONObject(key).getString("product_image"));
//	                        temp.setProductName(jObject.getJSONObject(key).getString("product_name"));
//	                        temp.setProductDescription(jObject.getJSONObject(key).getString("product_description"));
//	                        temp.setProductPrice(jObject.getJSONObject(key).getString("product_price"));
//	                        temp.setProductSrp(jObject.getJSONObject(key).getString("product_srp"));
//	                        temp.setProductActive(jObject.getJSONObject(key).getString("product_active"));
//	                        list.add(temp);
//	                        
//	                        feature = new FeatureModel(jObject.getJSONObject(key).getString("product_id"));
//	                        feature.setProductId(jObject.getJSONObject(key).getString("product_id"));
//	                        feature.setProductImageUrl(jObject.getJSONObject(key).getString("product_image"));
//	                        feature.setProductName(jObject.getJSONObject(key).getString("product_name"));
//	                        feature.setProductDescription(jObject.getJSONObject(key).getString("product_description"));
//	                        feature.setProductPrice(jObject.getJSONObject(key).getString("product_price"));
//	                        feature.setProductSrp(jObject.getJSONObject(key).getString("product_srp"));
//	                        feature.setProductActive(jObject.getJSONObject(key).getString("product_active"));
//	                        f.add(feature);
//
//	                        Log.v("DOWNLOAD ASYNC TASK 3 ID", jObject.getJSONObject(key).getString("product_id"));
//	                	}
//	                	
//	                	Log.v("DOWNLOAD ASYNC TASK 3 KEY", key);
//	                	Log.v("DOWNLOAD ASYNC TASK 3 URL", jObject.getJSONObject(key).optString("product_image"));
//	                }
//	            	
//	            	productUrlListView.setAdapter(new ProductListAdapter2(applicationContext, list)); // --> Set the adapter.
//	            	
//	            } else {
//	            	
//	            	String key = "246507";
//	            	feature = new FeatureModel(jObject.getJSONObject(key).getString("product_id"));
//	                feature.setProductImageUrl(jObject.getJSONObject(key).optString("product_image"));
//	                feature.setProductName(jObject.getJSONObject(key).getString("product_name"));
//	                feature.setProductDescription(jObject.getJSONObject(key).getString("product_description"));
//	                feature.setProductPrice(jObject.getJSONObject(key).getString("product_price"));
//	                feature.setProductSrp(jObject.getJSONObject(key).getString("product_srp"));
//	                feature.setProductActive(jObject.getJSONObject(key).getString("product_active"));
//	                f.add(feature);
//	            	
//	                Log.v("DOWNLOAD ASYNC TASK 3 STATUS", "Complete");
//	            }
//	             
//	        } catch (Exception e) {
//	        	
//	            e.printStackTrace();
//	            
//	        }
//	    }
//	}
}
