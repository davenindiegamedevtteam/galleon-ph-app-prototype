package com.openovate.galleon.listParser.models;

import android.app.Dialog;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ListView;

import com.openovate.galleon.R;
import com.openovate.galleon.Model.URLFinder.JSONSourceURLFinder;
import com.openovate.galleon.View.Category.Category_1_ToysKidsandBaby;
import com.openovate.galleon.jsonParser.utils.DownloadContentAsyncTask;

public class SubcategoryforToysAndGames implements AdapterView.OnItemClickListener
{
	private static int index;
	private ListView list;
	private Dialog dialog;
	private JSONSourceURLFinder url;
	private Object obj;
	private Category_1_ToysKidsandBaby cat;
	
	
	
	
	
	public SubcategoryforToysAndGames(int index, ListView list, Dialog dialog, JSONSourceURLFinder url,
			Category_1_ToysKidsandBaby cat)
	{
		this.index= index;
		this.list = list;
		this.dialog = dialog;
		this.url = url;
		this.cat = cat;
	}





	public static int getUpdatedIndex()
	{
		return index;
	}
	
	
	
	
	
	public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) 
	{
		String value = list.getItemAtPosition(i).toString();
		System.out.println(value);
		
		Category_1_ToysKidsandBaby.grid = (GridView) dialog.findViewById(R.id.poductUrlList);
		
		if(value.equals("All"))
		{
			Category_1_ToysKidsandBaby.index = 1;
			new DownloadContentAsyncTask(url.getURL1(1, 0), (GridView) dialog.findViewById(R.id.poductUrlList), dialog.getContext()).execute();
			dialog.hide();
			Log.v("SUBCATEGORY of TOYS AND GAMES ONLY", "All Contents");
			
		} else if(value.equals("Toy RC and Play Vehicles")) {
			
			Category_1_ToysKidsandBaby.index = 2;
			new DownloadContentAsyncTask(url.getURL1(2, 0), (GridView) dialog.findViewById(R.id.poductUrlList), dialog.getContext()).execute();
			dialog.hide();
			Log.v("SUBCATEGORY of TOYS AND GAMES ONLY", "Toy RC and Play Vehicles");
			
		} else if(value.equals("Action and Toy Figures")) {
			
			Category_1_ToysKidsandBaby.index = 3;
			new DownloadContentAsyncTask(url.getURL1(3, 0), (GridView) dialog.findViewById(R.id.poductUrlList), dialog.getContext()).execute();
			dialog.hide();
			Log.v("SUBCATEGORY of TOYS AND GAMES ONLY", "Action and Toy Figures");
			
		} else if(value.equals("Lego")) {
			
			Category_1_ToysKidsandBaby.index = 4;
			new DownloadContentAsyncTask(url.getURL1(4, 0), (GridView) dialog.findViewById(R.id.poductUrlList), dialog.getContext()).execute();
			dialog.hide();
			Log.v("SUBCATEGORY of TOYS AND GAMES ONLY", "Lego");
			
		} else if(value.equals("Robots")) {
			
			Category_1_ToysKidsandBaby.index = 5;
			new DownloadContentAsyncTask(url.getURL1(5, 0), (GridView) dialog.findViewById(R.id.poductUrlList), dialog.getContext()).execute();
			dialog.hide();
			Log.v("SUBCATEGORY of TOYS AND GAMES ONLY", "Robots");
			
		}
	}
}
