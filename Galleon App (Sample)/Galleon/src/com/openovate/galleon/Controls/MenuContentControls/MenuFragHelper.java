package com.openovate.galleon.Controls.MenuContentControls;

import android.app.Activity;
import android.content.Intent;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;

import com.actionbarsherlock.app.ActionBar;
import com.openovate.galleon.ProjectGalleonMainActivity;
import com.openovate.galleon.R;
import com.openovate.galleon.View.Category.Category_Custom_Search;

public class MenuFragHelper 
{
	/**
	 * 
	 * Enables you to bring the ActionBar layout for TEXTBOX search functionality.<br><br>
	 * 
	 * @author David Coronado Dimalanta
	 *
	 */
	public static class SearchFunction
	{
		public static ActionBar searchTextBox(ActionBar ab, final Activity act)
		{
			ab.setCustomView(R.layout.main_actionbar_galleon);
	        
	        // Set functionality of the textbox for edit listener.
	        ProjectGalleonMainActivity.search = (EditText) ab.getCustomView().findViewById(R.id.ET_Search);
	        ProjectGalleonMainActivity.search.setText(""); // --> Set as blank.
	        ProjectGalleonMainActivity.search.getText().toString(); // --> Converted it into string value. (Used it if SEARCH ITEM at menu is selected.)
	        ProjectGalleonMainActivity.search.setOnEditorActionListener(new OnEditorActionListener() 
	        {
				@Override
				public boolean onEditorAction(TextView v, int actionId, KeyEvent event) 
				{
					String current = ProjectGalleonMainActivity.search.getText().toString(); // --> Converted it into string value.
			        String replaced = current.replaceAll(" ", "+"); // --> Check for spaces and replace it with a plus sign. ("+")
					
					// Check if there is a blank textbox or with space at the start.
					if(!(replaced.equalsIgnoreCase("+")) || !(replaced.equalsIgnoreCase(null)))
					{
						ProjectGalleonMainActivity.search.setText(replaced); // --> Set the replaced String as the parameter.
						act.startActivity(new Intent(act, Category_Custom_Search.class));;
						
					} else {
						
						ProjectGalleonMainActivity.search.setText(""); // --> Set as blank again to display all random items.
						act.startActivity(new Intent(act, Category_Custom_Search.class));
						
					}
					
					return true;
				}
			});
			
			return ab;
		}
	}
	
	/**
	 * 
	 * Enables you to bring the ActionBar layout for WEBVIEW event.<br><br>
	 * 
	 * @author David Coronado Dimalanta
	 *
	 */
	public static class WebViewFunction
	{
		public static ActionBar closeButton(ActionBar ab, final Activity act)
		{
			ab.setCustomView(R.layout.close_tab_actionbar_galleon);
			
			Button close = (Button) ab.getCustomView().findViewById(R.id.BTN_Close_Web_View);
			close.setOnClickListener(new View.OnClickListener() 
			{
				@Override
				public void onClick(View v) 
				{
					act.finish(); // --> Close the Web View activity itself but not the rest of the activity.
				}
			});
			
			return ab;
		}
	}
}
