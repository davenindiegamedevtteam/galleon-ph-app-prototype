package com.openovate.galleon.Controls.CacheManagement;

import java.io.File;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

public class Cache 
{
	public static void showCacheDirectory(Context context)
	{
		Toast.makeText(context, context.getCacheDir().toString(), Toast.LENGTH_LONG).show();
	}
	
    public static void trimCache(Context context) 
    {
    	Toast.makeText(context, "Start up!", Toast.LENGTH_SHORT).show();
    	Log.v("TRIM CACHE STATUS", "Starting...");
    	
        try 
        {
           File dir = context.getCacheDir();
           
           Log.i("CACHE DIRECTORY", context.getCacheDir().toString());
           
           if (dir != null && dir.isDirectory()) 
           {
              deleteDir(dir);
              
              if(deleteDir(dir))
              {
            	  Toast.makeText(context, "Cache memory cleared.", Toast.LENGTH_SHORT).show();
            	  Log.v("TRIM CACHE STATUS", "Cache memory cleared.");
            	  
              } else {
            	  
            	  Toast.makeText(context, "Cache memory already cleared.", Toast.LENGTH_SHORT).show();
            	  Log.v("TRIM CACHE STATUS", "Cache memory already empty.");
            	  
              }
           }
           
        } catch (Exception e) {
        	
           Toast.makeText(context, "Cache not cleared on exception.", Toast.LENGTH_SHORT).show();
           Log.e("TRIM CACHE STATUS", "Cache not cleared on exception!");
           Log.e("TRIM CACHE STATUS", "ERROR!");
       	
        }
     }

     public static boolean deleteDir(File dir) 
     {
         if (dir != null && dir.isDirectory()) 
         {
            String[] children = dir.list();
           
            for (int i = 0; i < children.length; i++) 
            {
               boolean success = deleteDir(new File(dir, children[i]));
               
          	   Log.v("TRIM CACHE STATUS", "Clearing...");
              
               if (!success) 
               {
            	  Log.v("TRIM CACHE STATUS", "Cache can't find cache directory. Directory is empty.");
                  return false;
               }
            }
         }

         return dir.delete(); // --> File is deleted.
     }
}
