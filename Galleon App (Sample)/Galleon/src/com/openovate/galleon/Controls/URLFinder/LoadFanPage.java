package com.openovate.galleon.Controls.URLFinder;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

import com.openovate.galleon.R;
import com.openovate.galleon.WebViewActivity;
import com.openovate.galleon.Model.URLFinder.URLContractFinder;

public class LoadFanPage extends AsyncTask<Boolean, Void, Boolean> 
{
	private Activity act;
	private boolean status = true;
	private ProgressDialog dialog;
	
	
	
	
	
	public LoadFanPage(Activity act)
	{
		this.act = act;
		dialog = new ProgressDialog(this.act);
	}
	
	
	
	
	
	@Override
	protected Boolean doInBackground(Boolean... params) 
	{
		WebViewActivity.web.setWebChromeClient(new WebChromeClient()
		{
			@Override
			public void onProgressChanged(WebView view, int newProgress) 
			{
				super.onProgressChanged(view, newProgress);
				act.setProgress(newProgress * 1000);
			}
		});
		
		WebViewActivity.web.setWebViewClient(new WebViewClient()
		{
			@Override
			public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) 
			{
				super.onReceivedError(view, errorCode, description, failingUrl);
				Toast.makeText(act, "Oh no! " + description, Toast.LENGTH_SHORT).show();
				
				status = false;
			}
		});
		
//		WebViewActivity.web.loadUrl("http://lh5.ggpht.com/-VxpRg3Fdr8A/UL6Yf7jvaKI/AAAAAAAAFC0/M0uhxiUyoB8/s0/P02.jpg");
		WebViewActivity.web.loadUrl(URLContractFinder.GET_FACEBOOK_URL);
		
		return status;
	}

	@Override
	protected void onPostExecute(Boolean result) 
	{
		super.onPostExecute(result);
		
		if(result)
		{
			dialog.dismiss();
		}
	}

	@Override
	protected void onPreExecute() 
	{
		super.onPreExecute();
		dialog.setMessage("Now loading...");
		dialog.show();
	}
}
