package com.openovate.galleon.Database.Controls;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.Toast;

import com.openovate.galleon.CartListActivity;
import com.openovate.galleon.ProductDetailActivity;
import com.openovate.galleon.PurchaseActivity;
import com.openovate.galleon.Database.Controls.HttpPostPackage.HttpPostHelper;
import com.openovate.galleon.Database.Model.DatabaseDbHelper;
import com.openovate.galleon.Model.Flaggers.Flag;
import com.openovate.galleon.View.Notifications.Message_Dialog;

public class RegistrationListener
{
	//TODO ____________________[ Data Fields ]____________________
	// On Click Listeners
	private static OnClickListener finishListener;
	private static OnClickListener wishListListener;
	private static OnClickListener refreshListener;
	private static OnClickListener searchCustomerListener;
	
	// Database Handler Set
	public static RegistrationCheck chk;
	public static DatabaseDbHelper dh;
	
	// Customer Info Before Delivery
	public static String name = "Dork Fu Syet";
	public static String street_1 = "Nilaga St.";
	public static String street_2 = "Pootah St.";
	public static String city = "Navotas";
	public static String phone = "09397776661";
	public static String email = "dorkfu@yahoo.com";
	public static String country = "Philippines";
	public static String zip = "0666";
	public static String region = "NCR";
	public static String name_shipping = "Dork Fu Syet";
	public static String street_1_shipping = "Nilaga St.";
	public static String street_2_shipping = "Pootah St.";
	public static String city_shipping = "Navotas";
	public static String phone_shipping = "09397776661";
	public static String email_shipping = "dorkfu@yahoo.com";
	public static String country_shipping = "Philippines";
	public static String zip_shipping = "0666";
	public static String region_shipping = "NCR";
	public static String items = "";
	public static String costs = "999999999";
	public static String payment_method = "BDO";
	
	// List of Products per Customer/App/Wish
	public static String productName = "Yummy";
	public static String productID = "123456789987654321";
	public static String price = "9999";
	public static String details = "Blah blah blah...";
	public static String imageURL = "www.shit.com/hentai.jpg";
	public static String quantity = "9999";
	public static String subtotal = "XXX";
	
	// Flag (Optional)
	public static boolean isSent = true;
	
	
	
	
	
	//TODO ____________________[ Constructor ]____________________
	public RegistrationListener(final Context context)
	{
    	chk = new RegistrationCheck();
    	dh = new DatabaseDbHelper(context);
	}
	
	
	
	
	
	//TODO ____________________[ Database Update Method(s) ]____________________
	public static void updateDetailResponse(final Context context)
	{
		productName = ProductDetailActivity.productNameTV.getText().toString();
		price = ProductDetailActivity.priceTV.getText().toString();
		
		Toast.makeText(context, "Updating amount of item(s)...", Toast.LENGTH_SHORT).show();
//		chk.checkProductDetails(context, dh, productName, price, quantity, subtotal);
	}





	//TODO ____________________[ Click Listener Event Getters ]____________________
	public static void getDetailResponse(final Context context)
	{
		productName = ProductDetailActivity.productNameTV2.getText().toString();
		price = ProductDetailActivity.priceTV.getText().toString();
		productID = ProductDetailActivity.productID.getText().toString();
		
		chk.checkProductDetails(context, dh, productName, price, productID);
		
		Log.v("MAGKANO?", price);
	}
	
	public static void getWishListResponse(final Context context)
	{
		productName = ProductDetailActivity.productNameTV.getText().toString();
		price = ProductDetailActivity.priceTV.getText().toString();
		details = ProductDetailActivity.detailTV.getText().toString();
		imageURL = ProductDetailActivity.imageURL;
		
		chk.checkProductDetails(context, dh, productName, price, details, imageURL);
	}
	
	public static OnClickListener getShowDetailResponse(final Context context)
	{
	 	finishListener = new OnClickListener() 
		{
			@Override
			public void onClick(View v) 
			{
				productName = ProductDetailActivity.productNameTV.getText().toString();
				price = ProductDetailActivity.priceTV.getText().toString();
				productID = ProductDetailActivity.productID.getText().toString();
				
				Toast.makeText(context, "1 new item added.", Toast.LENGTH_SHORT).show();
				chk.checkProductDetails(context, dh, productName, price, productID);
			}
		};
	 	
		return finishListener;
	}
	
	public static OnClickListener getShowWishListResponse(final Context context)
	{
		wishListListener = new OnClickListener() 
		{
			@Override
			public void onClick(View v) 
			{
				productName = ProductDetailActivity.productNameTV.getText().toString();
				price = ProductDetailActivity.priceTV.getText().toString();
				details = ProductDetailActivity.detailTV.getText().toString();
				imageURL = ProductDetailActivity.imageURL;
				
				Toast.makeText(context, "1 new item added in the wish list.", Toast.LENGTH_SHORT).show();
				chk.checkProductDetails(context, dh, productName, price, details, imageURL);
			}
		};
	 	
		return wishListListener;
	}
	
	public static OnClickListener getFinishButtonResponse(final Context context)
	{
	 	finishListener = new OnClickListener() 
		{
			@Override
			public void onClick(View v) 
			{
				// Initialize String values from the textfields.
				name = PurchaseActivity.billName.getText().toString();
				street_1 = PurchaseActivity.billStreet1.getText().toString();
				street_2 = PurchaseActivity.billStreet2.getText().toString();
				country = PurchaseActivity.billCountry.getText().toString();
				phone = PurchaseActivity.billPhone.getText().toString();
				email = PurchaseActivity.billEmail.getText().toString();
				city = PurchaseActivity.billCity.getText().toString();
				zip = PurchaseActivity.billZipCode.getText().toString();
				region = PurchaseActivity.bc;
				name_shipping = PurchaseActivity.shipName.getText().toString();
				street_1_shipping = PurchaseActivity.shipStreet1.getText().toString();
				street_2_shipping = PurchaseActivity.shipStreet2.getText().toString();
				country_shipping = PurchaseActivity.shipCountry.getText().toString();
				phone_shipping = PurchaseActivity.shipPhone.getText().toString();
				email_shipping = PurchaseActivity.shipEmail.getText().toString();
				city_shipping = PurchaseActivity.shipCity.getText().toString();
				zip_shipping = PurchaseActivity.shipZipCode.getText().toString();
				region_shipping = PurchaseActivity.sc;
				payment_method = PurchaseActivity.pc;
				costs = PurchaseActivity.grandtotal.getText().toString();
				
				// Flag it depending either the two activities. (CartListActivity screen and ProductDetailsActivity screen.)
				if(Flag.listResult.equals("DETAIL"))
				{
					items = ProductDetailActivity.items;
					Log.v("LISTENER ITEM AT", "Product Details Activity");
					
				} else if(Flag.listResult.equals("CART")) {
					
					items = CartListActivity.items;
					Log.v("LISTENER ITEM AT", "Cartlist Activity");
					
				}
				
				System.out.println(name + " " + street_1 + " " + phone + " " + email + " " + street_2);
				
				if(!("".equalsIgnoreCase(name)) &&
						!("".equalsIgnoreCase(street_1)) &&
						!("".equalsIgnoreCase(country)) &&
						!("".equalsIgnoreCase(phone)) &&
						!("".equalsIgnoreCase(email)) &&
						!("".equalsIgnoreCase(city)) &&
						!("".equalsIgnoreCase(zip)) &&
						!("".equalsIgnoreCase(region)) &&
						!("".equalsIgnoreCase(name_shipping)) &&
						!("".equalsIgnoreCase(street_1_shipping)) &&
						!("".equalsIgnoreCase(country_shipping)) &&
						!("".equalsIgnoreCase(phone_shipping)) &&
						!("".equalsIgnoreCase(email_shipping)) &&
						!("".equalsIgnoreCase(city_shipping)) &&
						!("".equalsIgnoreCase(zip_shipping)) &&
						!("".equalsIgnoreCase(region_shipping))
						) // --> Check if one of the fields are empty.
				{
					// Check if the customer don't have any items in the Cart List.
					try
					{
						if(!("".equals(items)))
						{
							// Announced that the claim form is complete.
							Toast.makeText(context, "Your delivery is set!", Toast.LENGTH_SHORT).show();
							
							 // Save it onto the database of the following columns.
							chk.checkAll(context, dh, name, street_1, street_2, country, phone, email, city, zip, region, items, costs, payment_method, name_shipping, street_1_shipping, street_2_shipping, country_shipping, phone_shipping, email_shipping, city_shipping, zip_shipping, region_shipping);
							
							// Do the HTTP post thing.
							HttpPostHelper post = new HttpPostHelper(name, street_1, street_2, country, phone, email, city, zip, region, name_shipping, street_1_shipping, street_2_shipping, country_shipping, phone_shipping, email_shipping, city_shipping, zip_shipping, region_shipping, items, costs, payment_method, context);
							post.execute();
							
							// Sent Flag
							isSent = true;
							
						} else {
							
							// Error Message
							Flag.dialogResult = PurchaseActivity.FIELD_ERROR;
							context.startActivity(new Intent(context, Message_Dialog.class));
							
							// Toast Message
							Toast.makeText(context, "Cannot be purchased. No items in the Cart List.", Toast.LENGTH_SHORT).show();
							
							// Sent Flag
							isSent = false;
							
						}
						
					} catch(NullPointerException e) {
						
						// Toast Message
						Toast.makeText(context, "Value returns null. Error!", Toast.LENGTH_SHORT).show();
						
						// Error Message (Value Returns Null)
						Flag.dialogResult = PurchaseActivity.NULL_ERROR;
						context.startActivity(new Intent(context, Message_Dialog.class));
						
					} finally {
						
						if(isSent)
						{
							// Message after Purchase
							if(PurchaseActivity.pc.equals("BDO (Bank Deposit)"))
							{
								Flag.dialogResult = PurchaseActivity.pc;
								context.startActivity(new Intent(context, Message_Dialog.class));
							}
						}
						
					}
				 	
				} else {
					
					// Error Message
					Flag.dialogResult = PurchaseActivity.FIELD_ERROR;
					context.startActivity(new Intent(context, Message_Dialog.class));
					
					// Toast Message
					Toast.makeText(context, "Incomplete billing and shipping info..", Toast.LENGTH_SHORT).show();
					
					// Sent Flag
					isSent = false;
					
				}
			}
		};
	 	
		return finishListener;
	}
	
	public static OnClickListener refreshDatabase(final Context context)
	{
		refreshListener = new OnClickListener() 
			{
				@Override
				public void onClick(View v) 
				{
					// ? ? ?
				}
			};
		
		return refreshListener;
	}
	
	public static OnClickListener searchName(final Context context, final EditText searchedName)
	{
		searchCustomerListener = new OnClickListener() 
		{
			@Override
			public void onClick(View v) 
			{
//				dh.searchName("", context);
//				String name = searchedName.getText().toString();
//				dh.searchName(name, context);
			}
		};
    	
    	return searchCustomerListener;
	}
	
	public static OnClickListener exit(final Context context)
	{
		searchCustomerListener = new OnClickListener() 
		{
			@Override
			public void onClick(View v) 
			{
				for(int i = 0; i < 2500; i += 50)
				{
					if(i == 2400)
					{
						System.exit(0);
					}
				}
			}
		};
    	
    	return searchCustomerListener;
	}
}
