package com.openovate.galleon.Database.Controls;

import java.util.List;

import android.content.Context;
import android.widget.Toast;

import com.openovate.galleon.Database.Model.DatabaseDbHelper;

public class RegistrationCheck 
{
	//TODO ____________________[ Method for Calculating Registration ]____________________
	public void checkAll(Context context, DatabaseDbHelper dh, String name, String street1, String street2, String country, String phone, String email, String city, String zip, String region, String items, String costs, String payment, String name_shipping, String street1_shipping, String street2_shipping, String country_shipping, String phone_shipping, String email_shipping, String city_shipping, String zip_shipping, String region_shipping)
	{
		dh = new DatabaseDbHelper(context); // --> Database Handler from Class (DatabaseHandler.java)
		dh.insertAllColumnsPerRow(name, street1, street2, country, phone, email, city, zip, region, name_shipping, street1_shipping, street2_shipping, country_shipping, phone_shipping, email_shipping, city_shipping, zip_shipping, region_shipping, items, costs, payment); // --> Inserts a new text.
	}
	
//	public void checkAllForShipping(Context context, DatabaseDbHelper dh, String name, String street1, String street2, String country, String phone, String email, String city, String zip, String region)
//	{
//		dh = new DatabaseDbHelper(context); // --> Database Handler from Class (DatabaseHandler.java)
//		dh.insertAllColumnsPerRowForShipping(name, street1, street2, country, phone, email, city, zip, region); // --> Inserts a new text.
//	}
	
	public void checkProductDetails(Context context, DatabaseDbHelper dh, String productName, String price, String productID)
	{
		if(!isNameExistsInCartList(context, dh, productName))
		{
			dh = new DatabaseDbHelper(context); // --> Database Handler from Class (DatabaseHandler.java)
			dh.insertAllColumnsPerRowForCart(productName, price, productID); // --> Inserts a new text.
			Toast.makeText(context, "Added 1 new item in the cart list!", Toast.LENGTH_SHORT).show();
			
		} else {
			
			Toast.makeText(context, "This item is already in the cart list. Please add QTY if you wish.", Toast.LENGTH_SHORT).show();
			
		}
	}
	
	public void checkProductDetails(Context context, DatabaseDbHelper dh, String productName, String price, String details, String imageURL)
	{
		if(!isNameExistsInWishList(context, dh, productName))
		{
			dh = new DatabaseDbHelper(context); // --> Database Handler from Class (DatabaseHandler.java)
			dh.insertAllColumnsPerRowForWishList(productName, price, details, imageURL); // --> Inserts a new text.
			Toast.makeText(context, "Added 1 new item in the wish list!", Toast.LENGTH_SHORT).show();
			
		} else {
			
			Toast.makeText(context, "This item is already in the wish list.", Toast.LENGTH_SHORT).show();
			
		}
	}
	
	
	
	
	
	//TODO ____________________[ Anti-duplication Methods ]____________________
	private boolean isNameExistsInCartList(Context context,DatabaseDbHelper dh, String productName) 
	{
		
	    dh = new DatabaseDbHelper(context);
	    boolean result = false;

	    List<String> nameList = dh.searchNameMatchAtCartList();

	    for(String nameInDB : nameList) 
	    {
	         if (nameInDB.equals(productName))
	         {
	        	 result = true;
	         }
	    }

	    return result;
	}
	
	private boolean isNameExistsInWishList(Context context,DatabaseDbHelper dh, String productName) 
	{
		
	    dh = new DatabaseDbHelper(context);
	    boolean result = false;

	    List<String> nameList = dh.searchNameMatchAtCartList();

	    for(String nameInDB : nameList) 
	    {
	         if (nameInDB.equals(productName))
	         {
	        	 result = true;
	         }
	    }

	    return result;
	}

}
