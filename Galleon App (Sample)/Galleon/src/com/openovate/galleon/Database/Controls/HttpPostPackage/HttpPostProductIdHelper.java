package com.openovate.galleon.Database.Controls.HttpPostPackage;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.openovate.galleon.Model.Flaggers.Flag;
import com.openovate.galleon.Model.URLFinder.URLContractFinder;

public class HttpPostProductIdHelper extends AsyncTask<String, Void, String>
{
	private String[] product_ID;
	
	private Context context;
	
	public static String json = "sucker";
	
	private static final String server = URLContractFinder.GET_SHIPMENT_POST;
	
	
	
	
	
	public HttpPostProductIdHelper(String[] product_ID, final Context context)
	{
		this.product_ID = product_ID;
		this.context = context;
	}
	
	
	
	
	
	@Override
	protected void onPostExecute(String result) 
	{
		super.onPostExecute(result);
	}

	@Override
	protected void onPreExecute() 
	{
		super.onPreExecute();
	}

	@Override
	protected String doInBackground(String... params) 
	{
		//Check if String value for server is null or not;
		System.out.println("URL --> " + server);
		
		// Get the client and the server's URL.
		HttpClient client = new DefaultHttpClient();
		HttpPost post = new HttpPost(server);
		int status = 0;
		
		// List down and save all values.
		List<NameValuePair> pairs = new ArrayList<NameValuePair>();
		for(int i = 0; i < product_ID.length; i++)
		{
			pairs.add(new BasicNameValuePair("product_id[]", product_ID[i]));
		}
		
		// Check for HTTP response to see if posting is successful.
		try
		{
			// Get respone first.
			Log.v("ENTITY POST", EntityUtils.toString(new UrlEncodedFormEntity(pairs))); // --> Check the content of the entity from "pairs."
			post.setEntity(new UrlEncodedFormEntity(pairs));
			HttpResponse response = client.execute(post);
			status = response.getStatusLine().getStatusCode();
			String entity = EntityUtils.toString(response.getEntity()); // --> This will prevent IllegalStateException from consumable content. (HTTP response)
			
			// Check for status.
			Log.v("PAIRS @ PRODUCT_ID", pairs.toString()); // --> Straigtforward way of dusplaying POST URLLog.v("PAIRS @ PRODUCT_ID", pairs.toString()); // --> Straigtforward way of dusplaying POST URL..
			Log.v("STATUS HTTP CODE", String.valueOf(status));
			Log.v("STATUS MESSAGE", String.valueOf(response.getStatusLine()));
			Log.v("RESPONSE @ PRODUCT ID", entity); // --> Shows all the values that are successfully posted. (Last index value to first.)
			
			// Set JSON file.
			Flag.shippingCostMap = entity;
			
		} catch(UnsupportedEncodingException e) {
			
			e.printStackTrace();
			Log.e("HTTP POST STATUS", "Error!");
			
		} catch (ClientProtocolException e) {

			e.printStackTrace();
			Log.e("HTTP POST STATUS", "Failed to connect. Protocol invalid.");
			
		} catch (IOException e) {

			e.printStackTrace();
			Log.e("HTTP POST STATUS", "Invalid character/value/URL. URL is either low internet signal and loading is canceled or not recognized.");
			
		} finally {
			
			// Show result on Toast by getting the context from PurchaseActivity class.
			if(status == 200)
			{
				Log.i("HTTP POST STATUS", "Success!");
//				Toast.makeText(context, "Conncetion is valid.", Toast.LENGTH_LONG).show();
				
			} else if(status == 202) {
				
				Log.i("HTTP POST STATUS", "Success! Data retrieved.");
//				Toast.makeText(context, "Data retrieved.", Toast.LENGTH_LONG).show();
				
			} else {
				
				Log.e("HTTP POST STATUS", "Retrieval failed.");
//				Toast.makeText(context, "Connection retrieval failed.", Toast.LENGTH_LONG).show();
				
			}
			
		}
		
		return null;
	}
}
