package com.openovate.galleon.Database.Controls.HttpPostPackage;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

import com.openovate.galleon.PurchaseActivity;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

public class HttpPostHelper extends AsyncTask<String, Void, String>
{
	private String name;
	private String street1;
	private String street2;
	private String country;
	private String phone;
	private String email;
	private String city;
	private String zip;
	private String region;
	private String nameShipping;
	private String street1Shipping;
	private String street2Shipping;
	private String countryShipping;
	private String phoneShipping;
	private String emailShipping;
	private String cityShipping;
	private String zipShipping;
	private String regionShipping;
	private String items;
	private String costs;
	private String paymentMethod;
	
	private Context context;
	
	private static final String server = "http://rest.galleon.ph/post/order";
//	private static final String server = "http://rest.galleon.dev/post/order";
//	private static final String server = "http://rest.android.galleon.dev/post/order";
	
	
	
	
	
	public HttpPostHelper(String name, String street1, String street2, String country, String phone, String email, String city, String zip,
			String region, String nameShipping, String street1Shipping, String street2Shipping, String countryShipping, String phoneShipping,
			String emailShipping, String cityShipping, String zipShipping, String regionShipping, String items, String costs, String paymentMethod,
			final Context context)
	{
		this.name = name;
		this.street1 = street1;
		this.street2 = street2;
		this.country = country;
		this.phone = phone;
		this.email = email;
		this.city = city;
		this.zip = zip;
		this.region = region;
		this.nameShipping = nameShipping;
		this.street1Shipping = street1Shipping;
		this.street2Shipping = street2Shipping;
		this.countryShipping = countryShipping;
		this.phoneShipping = phoneShipping;
		this.emailShipping = emailShipping;
		this.cityShipping = cityShipping;
		this.zipShipping = zipShipping;
		this.regionShipping = regionShipping;
		this.items = items;
		this.costs = costs;
		this.paymentMethod = paymentMethod;
		this.context = context;
	}
	
	
	
	
	
	@Override
	protected void onPostExecute(String result) 
	{
		super.onPostExecute(result);
	}

	@Override
	protected void onPreExecute() 
	{
		super.onPreExecute();
	}

	@Override
	protected String doInBackground(String... params) 
	{
		//Check if String value for server is null or not;
		System.out.println("URL --> " + server);
		
		// Get the client and the server's URL.
		HttpClient client = new DefaultHttpClient();
		HttpPost post = new HttpPost(server);
		int status = 0;
		String i1 = null, i2 = null;
		
		// List down and save all values.
		List<NameValuePair> pairs = new ArrayList<NameValuePair>();
		pairs.add(new BasicNameValuePair("billing_name", name));
		pairs.add(new BasicNameValuePair("billing_street_one", street1));
		pairs.add(new BasicNameValuePair("billing_street_two", street2));
		pairs.add(new BasicNameValuePair("billing_country", country));
		pairs.add(new BasicNameValuePair("billing_phone", phone));
		pairs.add(new BasicNameValuePair("billing_email", email));
		pairs.add(new BasicNameValuePair("billing_city", city));
		pairs.add(new BasicNameValuePair("billing_zip_code", zip));
		pairs.add(new BasicNameValuePair("billing_region", region));
		pairs.add(new BasicNameValuePair("shipping_name", nameShipping));
		pairs.add(new BasicNameValuePair("shipping_street_one", street1Shipping));
		pairs.add(new BasicNameValuePair("shipping_street_two", street2Shipping));
		pairs.add(new BasicNameValuePair("shipping_country", countryShipping));
		pairs.add(new BasicNameValuePair("shipping_phone", phoneShipping));
		pairs.add(new BasicNameValuePair("shipping_email", emailShipping));
		pairs.add(new BasicNameValuePair("shipping_city", cityShipping));
		pairs.add(new BasicNameValuePair("shipping_zip_code", zipShipping));
		pairs.add(new BasicNameValuePair("shipping_region", regionShipping));
		pairs.add(new BasicNameValuePair("list_of_items", items));
		pairs.add(new BasicNameValuePair("subtotal", costs));
		pairs.add(new BasicNameValuePair("payment_method", paymentMethod));
		
		// Check for HTTP response to see if posting is successful.
		try
		{
			// Get respone first.
			Log.v("ENTITY POST", EntityUtils.toString(new UrlEncodedFormEntity(pairs))); // --> Check the content of the entity from "pairs."
			post.setEntity(new UrlEncodedFormEntity(pairs));
			HttpResponse response = client.execute(post);
			status = response.getStatusLine().getStatusCode();
			
			// Check for INET address.
//			InetAddress inet = InetAddress.getByName(server);
//			i1 = inet.getHostAddress();
//			i2 = inet.getHostName();
//			Log.v("INET STATUS", "Address: " + i1);
//			Log.v("INET STATUS", "Host Name: " + i2);
			
			// Check for status.
			Log.v("PAIRS @ BILLING INFO", pairs.toString()); // --> Straigtforward way of dusplaying POST URL.
			Log.v("STATUS HTTP CODE", String.valueOf(status));
			Log.v("STATUS MESSAGE", String.valueOf(response.getStatusLine()));
			Log.v("RESPONSE @ BILLING INFO", EntityUtils.toString(response.getEntity())); // --> Shows all the values that are successfully posted. (Last index value to first.)
			
			// Show result on Toast.
//			PurchaseActivity activity = new PurchaseActivity();
//			Toast.makeText(activity.context, "RESULT: " + String.valueOf(response.getStatusLine()), Toast.LENGTH_LONG).show();
			
		} catch(UnsupportedEncodingException e) {
			
			e.printStackTrace();
			Log.e("HTTP POST STATUS", "Error!");
			
		} catch (ClientProtocolException e) {

			e.printStackTrace();
			Log.e("HTTP POST STATUS", "Failed to connect. Protocol invalid.");
			
		} catch (IOException e) {

			e.printStackTrace();
//			Log.v("INET STATUS", "Address: " + i1);
//			Log.v("INET STATUS", "Host Name: " + i2);
			Log.e("HTTP POST STATUS", "Invalid character/value/URL. URL is either low internet signal and loading is canceled or not recognized.");
			
		} finally {
			
			// Show result on Toast by getting the context from PurchaseActivity class.
			if(status == 200)
			{
				Log.i("HTTP POST STATUS", "Success!");
//				Toast.makeText(context, "Conncetion is valid.", Toast.LENGTH_LONG).show();
				
			} else if(status == 202) {
				
				Log.i("HTTP POST STATUS", "Success! Data retrieved.");
//				Toast.makeText(context, "Data retrieved.", Toast.LENGTH_LONG).show();
				
			} else {
				
				Log.e("HTTP POST STATUS", "Retrieval failed.");
//				Toast.makeText(context, "Connection retrieval failed.", Toast.LENGTH_LONG).show();
				
			}
			
//			Log.v("URL", "" + server);
			
		}
		
		return null;
	}
}
