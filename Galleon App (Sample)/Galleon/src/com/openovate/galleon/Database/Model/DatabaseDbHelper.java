package com.openovate.galleon.Database.Model;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class DatabaseDbHelper extends SQLiteOpenHelper 
{
	//TODO ____________________[ Data Fields ]____________________
	// Tag and Context
	private static String TAG = "DatabaseDbHelper";
	Context context;
	
	// SQL
	public SQLiteDatabase db = this.getReadableDatabase();
	public SQLiteDatabase writeData = this.getWritableDatabase();
	
	
	
	
	
	//TODO ____________________[ Constructor ]____________________
	public DatabaseDbHelper(Context context) 
	{
		super(context, DatabaseTracker.DATABASE_NAME, null, DatabaseTracker.DATABASE_VERSION);
		this.context = context;
	}
	
	
	
	

	//TODO ____________________[ SQLite Open Helper Overriden Methods ]____________________
	@Override
	public void onCreate(SQLiteDatabase db) 
	{
		db.execSQL(DatabaseTracker.CartList.CREATE_CART_LIST_TABLE);
		db.execSQL(DatabaseTracker.CustomerList.CREATE_CATEGORIES_TABLE);
		db.execSQL(DatabaseTracker.CustomerShippingList.CREATE_SHIPPING_TABLE);
		db.execSQL(DatabaseTracker.WishList.CREATE_WISH_LIST_TABLE);
		Log.v(DatabaseDbHelper.class.getName(), "Database is created.");
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) 
	{
		db.execSQL("DROP TABLE IF EXISTS " + DatabaseTracker.TABLE_LABELS_1);
		db.execSQL("DROP TABLE IF EXISTS " + DatabaseTracker.TABLE_LABELS_2);
		db.execSQL("DROP TABLE IF EXISTS " + DatabaseTracker.TABLE_LABELS_3);
		db.execSQL("DROP TABLE IF EXISTS " + DatabaseTracker.TABLE_LABELS_4);
        onCreate(db);
	}
	
	
	
	
	
	//TODO ____________________[ Database Changes ]____________________
	public void insertAllColumnsPerRowForCart(String productName, String price, String productID)
	{
		// Insert values to its respective column.
		SQLiteDatabase db = this.getWritableDatabase();
		ContentValues values = new ContentValues();
		values.put(DatabaseTracker.CartList.PRODUCT_NAME, productName);
		values.put(DatabaseTracker.CartList.PRICE, price);
		values.put(DatabaseTracker.CartList.PRODUCT_ID, productID);
		
		// Inserting rows.
		db.insert(DatabaseTracker.TABLE_LABELS_1, null, values);
		db.close(); // --> Always close the database to prevent running dynamically.
	}
	
	public void insertAllColumnsPerRow(String name, String street1, String street2, String country, String phone, String email, String city, String zip, String region, String name_shipping, String street1_shipping, String street2_shipping, String country_shipping, String phone_shipping, String email_shipping, String city_shipping, String zip_shipping, String region_shipping, String items, String costs, String payment)
	{
		// Insert values to its respective column.
		SQLiteDatabase db = this.getWritableDatabase();
		ContentValues values = new ContentValues();
		values.put(DatabaseTracker.CustomerList.NAME, name);
		values.put(DatabaseTracker.CustomerList.STREET_1, street1);
		values.put(DatabaseTracker.CustomerList.STREET_2, street2);
		values.put(DatabaseTracker.CustomerList.COUNTRY, country);
		values.put(DatabaseTracker.CustomerList.PHONE, phone);
		values.put(DatabaseTracker.CustomerList.EMAIL, email);
		values.put(DatabaseTracker.CustomerList.CITY, city);
		values.put(DatabaseTracker.CustomerList.ZIP_CODE, zip);
		values.put(DatabaseTracker.CustomerList.REGION, region);
		values.put(DatabaseTracker.CustomerList.LIST_OF_ITEMS, items);
		values.put(DatabaseTracker.CustomerList.TOTAL_COST, costs);
		values.put(DatabaseTracker.CustomerList.PAYMENT_METHOD, payment);
		values.put(DatabaseTracker.CustomerList.SHIPPING_NAME, name_shipping);
		values.put(DatabaseTracker.CustomerList.SHIPPING_STREET_1, street1_shipping);
		values.put(DatabaseTracker.CustomerList.SHIPPING_STREET_2, street2_shipping);
		values.put(DatabaseTracker.CustomerList.SHIPPING_COUNTRY, country_shipping);
		values.put(DatabaseTracker.CustomerList.SHIPPING_PHONE, phone_shipping);
		values.put(DatabaseTracker.CustomerList.SHIPPING_EMAIL, email_shipping);
		values.put(DatabaseTracker.CustomerList.SHIPPING_CITY, city_shipping);
		values.put(DatabaseTracker.CustomerList.SHIPPING_ZIP_CODE, zip_shipping);
		values.put(DatabaseTracker.CustomerList.SHIPPING_REGION, region_shipping);
		
		// Inserting rows.
		db.insert(DatabaseTracker.TABLE_LABELS_2, null, values);
		db.close(); // --> Always close the database to prevent running dynamically.
	}
	
	public void insertAllColumnsPerRowForWishList(String productName, String price, String details, String imageURL)
	{
		// Insert values to its respective column.
		SQLiteDatabase db = this.getWritableDatabase();
		ContentValues values = new ContentValues();
		values.put(DatabaseTracker.WishList.PRODUCT_NAME, productName);
		values.put(DatabaseTracker.WishList.PRICE, price);
		values.put(DatabaseTracker.WishList.DETAILS, details);
		values.put(DatabaseTracker.WishList.IMAGE_URL, imageURL);
		
		// Inserting rows.
		db.insert(DatabaseTracker.TABLE_LABELS_4, null, values);
		db.close(); // --> Always close the database to prevent running dynamically.
	}
	
	public List<String> searchNameMatchAtCartList()
	{
		List<String> productNames = new ArrayList<String>(); //This must be return.
		
		//Select all Query it have.
		String selectQuery = "SELECT * FROM " + DatabaseTracker.TABLE_LABELS_1;
		
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);
		
		//Do the loop by rows to add new in the database
		if(cursor.moveToFirst())
		{
			do
			{
				productNames.add(cursor.getString(cursor.getColumnIndex(DatabaseTracker.CartList.PRODUCT_NAME)));
			}
			
			while(cursor.moveToNext());
		}
		
		//Always close the connections.
		cursor.close();
		db.close();
		
		//Return the labels for feedback and response. the "return" keyword can be like a value set for process.
		return productNames;
	}
	
	public List<String> searchNameMatchAtWishList()
	{
		List<String> productNames = new ArrayList<String>(); //This must be return.
		
		//Select all Query it have.
		String selectQuery = "SELECT * FROM " + DatabaseTracker.TABLE_LABELS_4;
		
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);
		
		//Do the loop by rows to add new in the database
		if(cursor.moveToFirst())
		{
			do
			{
				productNames.add(cursor.getString(cursor.getColumnIndex(DatabaseTracker.WishList.PRODUCT_NAME)));
			}
			
			while(cursor.moveToNext());
		}
		
		//Always close the connections.
		cursor.close();
		db.close();
		
		//Return the labels for feedback and response. the "return" keyword can be like a value set for process.
		return productNames;
	}

	
	public void deleteAllCustomers()
	{
		
		SQLiteDatabase db = this.getWritableDatabase();
		db.delete(DatabaseTracker.TABLE_LABELS_1, null, null);
		db.delete(DatabaseTracker.TABLE_LABELS_2, null, null);
		db.delete(DatabaseTracker.TABLE_LABELS_3, null, null);
		db.close();
	}
	
	public void deleteCartList()
	{
		
		SQLiteDatabase db = this.getWritableDatabase();
		db.delete(DatabaseTracker.TABLE_LABELS_1, null, null);
		db.close();
	}
	
	public void deleteCartItemRow(int ID)
	{
		
		SQLiteDatabase db = this.getWritableDatabase();
		db.delete(DatabaseTracker.TABLE_LABELS_1,  DatabaseTracker.KEY_ID + "='" + String.valueOf(ID) + "'", null);
		db.close();
	}
	
	public void deleteWishItemRow(Cursor c, String[] productName)
	{
		
		SQLiteDatabase db = this.getWritableDatabase();
		db.delete(DatabaseTracker.TABLE_LABELS_4, c.getColumnNames() + "=?", productName);
		db.close();
	}
	
	public void deleteWishItemRow(int ID)
	{
		
		SQLiteDatabase db = this.getWritableDatabase();
		db.delete(DatabaseTracker.TABLE_LABELS_4,  DatabaseTracker.KEY_ID + "='" + String.valueOf(ID) + "'", null);
		db.close();
	}
	
	public void deleteWishItemRow(String productName)
	{
		
		SQLiteDatabase db = this.getWritableDatabase();
		db.delete(DatabaseTracker.TABLE_LABELS_4,  DatabaseTracker.WishList.PRODUCT_NAME + "='" + productName + "'", null);
		db.close();
	}
}
