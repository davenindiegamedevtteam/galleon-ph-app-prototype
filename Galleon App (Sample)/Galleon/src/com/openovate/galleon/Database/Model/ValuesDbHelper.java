package com.openovate.galleon.Database.Model;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class ValuesDbHelper extends SQLiteOpenHelper 
{
	
	public ValuesDbHelper(Context context) 
	{
		super(context, DatabaseTracker.DATABASE_NAME, null, DatabaseTracker.DATABASE_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase db) 
	{
		db.execSQL(DatabaseTracker.CartList.CREATE_CART_LIST_TABLE);
		db.execSQL(DatabaseTracker.CustomerList.CREATE_CATEGORIES_TABLE);
		db.execSQL(DatabaseTracker.CustomerShippingList.CREATE_SHIPPING_TABLE);
		db.execSQL(DatabaseTracker.WishList.CREATE_WISH_LIST_TABLE);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) 
	{
		db.execSQL("DROP TABLE IF EXISTS " + DatabaseTracker.TABLE_LABELS_1);
		db.execSQL("DROP TABLE IF EXISTS " + DatabaseTracker.TABLE_LABELS_2);
		db.execSQL("DROP TABLE IF EXISTS " + DatabaseTracker.TABLE_LABELS_3);
		db.execSQL("DROP TABLE IF EXISTS " + DatabaseTracker.TABLE_LABELS_4);
        onCreate(db);
	}

}
