package com.openovate.galleon.Database.Model;

public class DatabaseTracker 
{
	public static final int DATABASE_VERSION = 9; // --> Version of the Database (Current Version Update: From 8 to 9)
	public static final String DATABASE_NAME = "MainDatabase"; // --> Name of the Database
	public static final String TABLE_LABELS_1 = "cart"; // --> Table Name
	public static final String TABLE_LABELS_2 = "customers";
	public static final String TABLE_LABELS_3 = "shipping";
	public static final String TABLE_LABELS_4 = "wishlist";
	public static final String KEY_ID = "id"; // --> Key ID

	
	
	
	
	public static abstract class CartList
	{
		public static final String PRODUCT_NAME = "productname";
		public static final String PRICE = "price";
		public static final String PRODUCT_ID = "productid";
		
		public static final String CREATE_CART_LIST_TABLE = "CREATE TABLE " + TABLE_LABELS_1 + "(" + 
				KEY_ID + " INTEGER PRIMARY KEY," + 
				PRODUCT_NAME + " TEXT, " + 
				PRICE + " TEXT, " +
				PRODUCT_ID + " TEXT)";
	}
	
	
	
	
	
	public static abstract class CustomerList
	{
		public static final String NAME = "name";
		public static final String STREET_1 = "streetone";
		public static final String STREET_2 = "streettwo";
		public static final String COUNTRY = "country";
		public static final String PHONE = "phone";
		public static final String EMAIL = "email";
		public static final String CITY = "city";
		public static final String ZIP_CODE = "zip";
		public static final String REGION = "region";
		public static final String LIST_OF_ITEMS = "items";
		public static final String TOTAL_COST = "costs";
		public static final String PAYMENT_METHOD = "paymentmethod";
		public static final String SHIPPING_NAME = "nameshipping";
		public static final String SHIPPING_STREET_1 = "streetoneshipping";
		public static final String SHIPPING_STREET_2 = "streettwoshipping";
		public static final String SHIPPING_COUNTRY = "countryshipping";
		public static final String SHIPPING_PHONE = "phoneshipping";
		public static final String SHIPPING_EMAIL = "emailshipping";
		public static final String SHIPPING_CITY = "cityshipping";
		public static final String SHIPPING_ZIP_CODE = "zipshipping";
		public static final String SHIPPING_REGION = "regionshipping";
		
		public static final String CREATE_CATEGORIES_TABLE = "CREATE TABLE " + TABLE_LABELS_2 + "(" + 
				KEY_ID + " INTEGER PRIMARY KEY, " + 
				NAME + " TEXT, " + 
				STREET_1 + " TEXT, " + 
				STREET_2 + " TEXT, " + 
				COUNTRY + " TEXT, " + 
				PHONE + " TEXT, " + 
				EMAIL + " TEXT, " + 
				CITY + " TEXT, " + 
				ZIP_CODE + " TEXT, " + 
				REGION + " TEXT, " + 
				LIST_OF_ITEMS + " TEXT, " + 
				TOTAL_COST + " TEXT, " + 
				PAYMENT_METHOD + " TEXT, " +  
				SHIPPING_NAME + " TEXT, " + 
				SHIPPING_STREET_1 + " TEXT, " + 
				SHIPPING_STREET_2 + " TEXT, " + 
				SHIPPING_COUNTRY + " TEXT, " + 
				SHIPPING_PHONE + " TEXT, " + 
				SHIPPING_EMAIL + " TEXT, " + 
				SHIPPING_CITY + " TEXT, " + 
				SHIPPING_ZIP_CODE + " TEXT, " + 
				SHIPPING_REGION + " TEXT)";
	}
	
	
	
	
	
	public static abstract class CustomerShippingList
	{
		public static final String NAME = "name";
		public static final String STREET_1 = "streetone";
		public static final String STREET_2 = "streettwo";
		public static final String COUNTRY = "country";
		public static final String PHONE = "phone";
		public static final String EMAIL = "email";
		public static final String CITY = "city";
		public static final String ZIP_CODE = "zip";
		public static final String REGION = "region";
		public static final String LIST_OF_ITEMS = "items";
		public static final String TOTAL_COST = "costs";
		public static final String SHIPPING_REGION = "shipping";
		
		public static final String CREATE_SHIPPING_TABLE = "CREATE TABLE " + TABLE_LABELS_3 + "(" + KEY_ID +
				" INTEGER PRIMARY KEY," + NAME + " TEXT, " + STREET_1 + " TEXT, " + STREET_2 + " TEXT, " 
				+ COUNTRY + " TEXT, " + PHONE + " TEXT, " + EMAIL + " TEXT, "
				 + CITY + " TEXT, " + ZIP_CODE + " TEXT, " + REGION + " TEXT, " + LIST_OF_ITEMS + " TEXT, " + TOTAL_COST + " TEXT)";
	}
	
	
	
	
	
	public static abstract class WishList
	{
		public static final String PRODUCT_NAME = "productname";
		public static final String PRICE = "price";
		public static final String DETAILS = "details";
		public static final String IMAGE_URL = "imageurl";
		
		public static final String CREATE_WISH_LIST_TABLE = "CREATE TABLE " + TABLE_LABELS_4 + "(" + KEY_ID +
				" INTEGER PRIMARY KEY," + PRODUCT_NAME + " TEXT, " + PRICE + " TEXT, " + DETAILS + " TEXT, " + IMAGE_URL + " TEXT)";
	}
}
