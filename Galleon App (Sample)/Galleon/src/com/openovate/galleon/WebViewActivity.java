package com.openovate.galleon;

import android.app.Activity;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.Menu;
import com.openovate.galleon.Controls.MenuContentControls.MenuFragHelper;
import com.openovate.galleon.Controls.URLFinder.LoadFanPage;
import com.openovate.galleon.Model.Flaggers.Flag;
import com.openovate.galleon.Model.URLFinder.URLContractFinder;

public class WebViewActivity extends SherlockActivity
{
	public static WebView web;
	
	
	
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		// Override this method.
        super.onCreate(savedInstanceState);
        
        // Screen Setup
        getWindow().requestFeature(Window.FEATURE_PROGRESS);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.web_view_layout);
        
        // Initialize the Activity class for the Async Task class. (LoadFanPage)
		final Activity activity = this;
		
		// Find the view ID of the web view.
		web = (WebView) findViewById(R.id.WV_Fan_Page);
		
		// ? ? ?
		web.setWebChromeClient(new WebChromeClient()
		{
			@Override
			public void onProgressChanged(WebView view, int newProgress) 
			{
				super.onProgressChanged(view, newProgress);
				setProgress(newProgress * 1000);
			}
		});
		
		web.setWebViewClient(new WebViewClient()
		{
			@Override
			public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) 
			{
				super.onReceivedError(view, errorCode, description, failingUrl);
				Toast.makeText(getApplicationContext(), "Oh no! " + description, Toast.LENGTH_SHORT).show();
				
//				status = false;
			}
		});
		
		// Depending on the user chooses what social media website will be used for sharing.
		if("Twitter".equals(Flag.dialogResult))
		{
			web.loadUrl(URLContractFinder.GET_TWITTER_URL);
			
		} else if("Facebook".equals(Flag.dialogResult)) {
			
			web.loadUrl(URLContractFinder.GET_FACEBOOK_URL);
			
		}
		
		// Run and execute!
//		new LoadFanPage(activity).execute();
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) 
	{
		super.onCreateOptionsMenu(menu);
		
		// Call the ActionBar.
		ActionBar ab = getSupportActionBar();
		
		// Set content view of the ActionBar Sherlock.
		MenuFragHelper.WebViewFunction.closeButton(ab, this);
		
		//
		ab.setDisplayOptions(ab.DISPLAY_SHOW_CUSTOM | ab.DISPLAY_SHOW_CUSTOM);
		
		return true;
	}
}
