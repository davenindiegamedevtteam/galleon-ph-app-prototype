package com.openovate.galleon;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.database.Cursor;
import android.database.SQLException;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;
import au.com.bytecode.opencsv.CSVWriter;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.openovate.galleon.Controls.MenuContentControls.MenuFragHelper;
import com.openovate.galleon.Database.Controls.RegistrationListener;
import com.openovate.galleon.Database.Controls.HttpPostPackage.HttpPostProductIdHelper;
import com.openovate.galleon.Database.Model.DatabaseDbHelper;
import com.openovate.galleon.Database.Model.DatabaseTracker;
import com.openovate.galleon.Model.Flaggers.Flag;
import com.openovate.galleon.View.Category.Category_1_ToysKidsandBaby;
import com.openovate.galleon.View.Category.Category_2_SportsAndOutdoors;
import com.openovate.galleon.View.Category.Category_3_ClothingShoesAndJewelry;
import com.openovate.galleon.View.Category.Category_4_ElectronicsAndComputer;
import com.openovate.galleon.View.Category.Category_5_HomeGardenAndTools;
import com.openovate.galleon.View.Category.Category_Custom_Search;
import com.openovate.galleon.View.Category.Category_Select_Dialog_Pane;
import com.openovate.galleon.View.Notifications.Message_Dialog;
import com.openovate.galleon.View.Share.Share_Dialog;
import com.openovate.galleon.WordLengthTrimmer.WordCrop;
import com.openovate.galleon.jsonParser.models.FeatureModel;
import com.openovate.galleon.jsonParser.models.ProductModel;
import com.openovate.galleon.jsonParser.models.ProductModel2;
import com.openovate.galleon.jsonParser.utils.ImageDownloadAsynctask;

/**
 * <i>
 * Created by andybooboo on 8/3/13. <br>
 * Revised by David Coronado Dimalanta since 9/24/2013. <br><br><br>
 * </i>
 * 
 * <b>INFO: </b> Previews a selected item from the list.
 */

public class ProductDetailActivity extends SherlockActivity 
{
    //TODO _______________[ Field Objects ]_______________
	// Index Values
	private int X = 0; // --> To be used for picking row number.
	private Integer rowIndex; // --> To be used for setting total number of rows.
	
	// String Values
	public static String items = "";
	public static String costs = "";
	public static String imageURL = "";
	public static String query = "SELECT * FROM cart";
	public static String query2 = "SELECT * FROM " + DatabaseTracker.TABLE_LABELS_4;
	public static String productID_Post[];
	
	// Context
	public final Context context = this;
	
	// Product Models
    private ProductModel product;
    private ProductModel2 product2;
    private FeatureModel feature;
    
    // Textview Values
    public static TextView priceTV, productNameTV, productNameTV2, detailTV, productID;
    
    // Table Label Column Names
    public static TextView productName;
	public static TextView price;
	public static TextView details;
	public static TextView imageUrl;
	public static TextView quantity;
	public static TextView subtotal;
	
	// Get values
	public static TextView getProductName;
	public static TextView getDetails;
	public static TextView getImageURL;
	public static TextView getPrice[];
	public static EditText getQuantity[];
	public static TextView getSubtotal[];
	
	// Table Layouts
	public static TableRow tableRow;
	public static TableLayout tableLayout;
	
	// Database Control
	public DatabaseDbHelper dh;
	
	// Button Listener for Database Event
	@SuppressWarnings("unused")
	private RegistrationListener rl;
    
	// Image URL
    ImageView productIV;

    
    
    
    
    //TODO _______________[ Activity Overriden Method(s) ]_______________
    @SuppressLint("NewApi")
	@Override
    public void onCreate(Bundle savedInstanceState) 
    {
    	// Set this activity.
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_product_detail);

        // Textview Constructors
        priceTV = (TextView) findViewById(R.id.productPrice);
        productNameTV = (TextView) findViewById(R.id.productName);
        productNameTV2 = (TextView) findViewById(R.id.TV_Fullname_at_Description_Layout);
        productID = (TextView) findViewById(R.id.TV_ID_Description);
        detailTV = (TextView) findViewById(R.id.productDescription);
        productIV = (ImageView) findViewById(R.id.productImage);
        
        // Button Constructor (To be used for hiding buttons due to verifying price.)
        ImageButton buy = (ImageButton) findViewById(R.id.BTN_Buy);
        ImageButton cart = (ImageButton) findViewById(R.id.BTN_Add_to_Cart);
        
        // Flags
        int flag = ProjectGalleonMainActivity.flag; // --> Change the flag tom prevent getting from IllegalStateException.
        Flag.dialogResult = ""; // --> For message dialog pane event.
        Flag.listResult = "DETAIL";

        // Flag checking between in HOME page and in DIALOG pane.
        if(flag == 0)
        {
        	/*
        	 * 
        	 * 			If product chose from ...
        	 * 
        	 */
        	
        	// Download contents first.
        	product = (ProductModel) this.getIntent().getSerializableExtra("product");
        	productID.setText(product.getSubProductId());
            priceTV.setText(product.getProductSrp());
            productNameTV2.setText(product.getProductName());
            detailTV.setText(product.getProductDescription());
            imageURL = product.getProductImageUrl();
            
            // Check for fitting the length of characters depending on the orientation.
            if(getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) 
            {
                // code to do for Portrait Mode
            	productNameTV.setText(WordCrop.truncate(product.getProductName(), 40));
            	
            } else {
            	
                // Landscape Mode   
            	productNameTV.setText(WordCrop.truncate(product.getProductName(), 50));
            	
            }
            
            // Execute!
        	ImageDownloadAsynctask idat = new ImageDownloadAsynctask(productIV, getApplicationContext(), product.getProductImageUrl());
            idat.execute();
            
            // Check if the description is available
            if(product.getProductDescription().isEmpty() || product.getProductDescription().equalsIgnoreCase("") || product.getProductDescription().equalsIgnoreCase(null))
            {
            	detailTV.setText("--- DESCRIPTION NOT AVAILABLE ---");
            }
            
            // And, check if price is verified or not. Else, BUY button is disabled or hidden.
            if(product.getProductSrp().equals("0"))
            {
            	buy.setVisibility(View.GONE);
            	cart.setVisibility(View.GONE);
            	
            } else {
            	
            	buy.setVisibility(View.VISIBLE);
            	cart.setVisibility(View.VISIBLE);
            	
            }
            
        } else if(flag == 1) {
        	
        	// Download contents first.
        	product2 = (ProductModel2) this.getIntent().getSerializableExtra("product");
        	productID.setText(product2.getSubProductId());
            priceTV.setText(product2.getProductSrp());
            productNameTV2.setText(product2.getProductName());
            detailTV.setText(product2.getProductDescription());
            imageURL = product2.getProductImageUrl();
            
            // Check for fitting the length of characters depending on the orientation.
            if(getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) 
            {
                // code to do for Portrait Mode
            	productNameTV.setText(WordCrop.truncate(product2.getProductName(), 40));
            	
            } else {
            	
                // Landscape Mode   
            	productNameTV.setText(WordCrop.truncate(product2.getProductName(), 50));
            	
            }
            
            // Execute!
        	ImageDownloadAsynctask idat = new ImageDownloadAsynctask(productIV, getApplicationContext(), product2.getProductImageUrl());
            idat.execute();
            
            // Check if the description is available
            if(product2.getProductDescription().isEmpty() || product2.getProductDescription().equalsIgnoreCase("") || product2.getProductDescription().equalsIgnoreCase(null))
            {
            	detailTV.setText("--- DESCRIPTION NOT AVAILABLE ---");
            }
            	
            // And, check if price is verified or not. Else, BUY button is disabled or hidden.
            if(product2.getProductSrp().equals("0"))
            {
            	buy.setVisibility(View.GONE);
            	cart.setVisibility(View.GONE);
            	
            } else {
            	
            	buy.setVisibility(View.VISIBLE);
            	cart.setVisibility(View.VISIBLE);
            	
            }
        	
        } else if(flag == 2) {
        	
        	// Download contents first.
//        	productID.setText(feature.getProductId());
        	feature = (FeatureModel) this.getIntent().getSerializableExtra("product");
            priceTV.setText(feature.getProductSrp());
            productNameTV2.setText(feature.getProductName());
            detailTV.setText(feature.getProductDescription());
            imageURL = feature.getProductImageUrl();
            
            // Check for fitting the length of characters depending on the orientation.
            if(getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) 
            {
                // code to do for Portrait Mode
            	productNameTV.setText(WordCrop.truncate(feature.getProductName(), 40));
            	
            } else {
            	
                // Landscape Mode   
            	productNameTV.setText(WordCrop.truncate(feature.getProductName(), 50));
            	
            }
            
            // Execute!
        	ImageDownloadAsynctask idat = new ImageDownloadAsynctask(productIV, getApplicationContext(), feature.getProductImageUrl());
            idat.execute();
            
            // Check if the description is available
            if(feature.getProductDescription().isEmpty() || feature.getProductDescription().equalsIgnoreCase("") || feature.getProductDescription().equalsIgnoreCase(null))
            {
            	detailTV.setText("--- DESCRIPTION NOT AVAILABLE ---");
            }
            	
            // And, check if price is verified or not. Else, BUY button is disabled or hidden.
            if(feature.getProductSrp().equals("0"))
            {
            	buy.setVisibility(View.GONE);
            	cart.setVisibility(View.GONE);
            	
            } else {
            	
            	buy.setVisibility(View.VISIBLE);
            	cart.setVisibility(View.VISIBLE);
            	
            }
        	
        }
        
        System.out.println("QUERY NAME: " + query2);
        System.out.println("PRODUCT NAME: " + productNameTV.getText().toString());
        System.out.println("PRICE: " + priceTV.getText().toString());
        System.out.println("DETAILS: " + detailTV.getText().toString());
        System.out.println("URL: " + imageURL);
        
        rl = new RegistrationListener(context);
    }
    
    //

	@SuppressWarnings({ "unused", "static-access" })
	@Override
    public boolean onCreateOptionsMenu(Menu menu) 
    {
		super.onCreateOptionsMenu(menu);
		
        // Inflate the menu; this adds items to the action bar if it is present.
        getSupportMenuInflater().inflate(R.menu.project_galleon_main, menu);
        ActionBar ab = getSupportActionBar();
        ab.setCustomView(R.layout.main_actionbar_galleon);
        
        // Set functionality of the textbox for edit listener.
        MenuFragHelper.SearchFunction.searchTextBox(ab, this);
        
        // Cart Button on Actionbar.
        ImageButton cartButton = (ImageButton) ab.getCustomView().findViewById(R.id.IB_Add_to_Cart);
        cartButton.setOnClickListener(new View.OnClickListener() 
        {
			@Override
			public void onClick(View v) 
			{
				Intent intent = new Intent();
				intent.setClass(context, CartListActivity.class);
				startActivity(intent);
			}
		});
        
        ab.setDisplayOptions(ab.DISPLAY_SHOW_CUSTOM | ab.DISPLAY_SHOW_CUSTOM);
        return true;
    }

	@Override
	protected void onDestroy() 
	{
		super.onDestroy();
		//
	}

	@Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
    	// Choice logics within the soft menu key dropdown event when one of the item ID's is touched.
    	switch(item.getItemId())
    	{
    	case R.id.OPT_1_Home:
    		home();
    		return true;
    		
    	case R.id.OPT_2_Login:
    		login();
    		return true;
    		
    	case R.id.OPT_3_Share:
    		share();
    		return true;
    		
    	case R.id.OPT_4_Categories:
    		categories();
    		return true;
    		
    	case R.id.OPT_5_Search:
    		search();
    		return true;
    		
    	case R.id.OPT_6_Cart_List:
    		cart();
    		return true;
    		
    	case R.id.OPT_7_Wish_List:
    		wish();
    		return true;
    		
    	case R.id.OPT_8_Buy:
    		buy();
    		return true;
    		
    	case R.id.OPT_9_Exit:
    		exit();
    		return true;
    		
    	default:
    		return true;
    	}
    }
    
    
    
    
    
    //TODO _______________[ On Choice Listener Event for Menu Soft Key ]_______________
    private void home()
    {
    	Intent intent = new Intent(this, ProjectGalleonMainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP); 
        startActivity(intent);
    }
    
    private void login()
    {
    	Flag.dialogResult = "login";
    	System.out.println("FLAG " + Flag.dialogResult);
    	startActivity(new Intent(this, Message_Dialog.class));
    }
    
    private void share()
    {
    	startActivity(new Intent(this, Share_Dialog.class));
    }
    
    private void categories()
    {
//    	startActivity(new Intent(this, Category_Select_Dialog_Pane.class));
    	Flag.dialogResult = "login";
    	System.out.println("FLAG " + Flag.dialogResult);
    	startActivity(new Intent(this, Message_Dialog.class));
    }
    
    private void search()
    {
    	finish();
    	startActivity(new Intent(this, Category_Custom_Search.class));
    }
    
    private void cart()
    {
    	finish();
    	startActivity(new Intent(this, CartListActivity.class));
    }
    
    private void wish()
    {
    	finish();
    	startActivity(new Intent(this, WishListActivity.class));
    }
    
    private void buy()
    {
    	finish();
    	startActivity(new Intent(this, PurchaseActivity.class));
    }
    
    private void exit()
    {
    	Flag.dialogResult = "exit";
    	System.out.println("FLAG " + Flag.dialogResult);
    	startActivity(new Intent(this, Message_Dialog.class));
    }
    
    
    
    
    
    
    //TODO _______________[ On Method Callbacks for Category Dialogs ]_______________
    public void toysKidsandBaby()
    {
    	startActivity(new Intent(this, Category_1_ToysKidsandBaby.class));
    }
    
    public void sportsAndOutdoors()
    {
    	startActivity(new Intent(this, Category_2_SportsAndOutdoors.class));
    }
    
    public void clothingShoesAndJewelry()
    {
    	startActivity(new Intent(this, Category_3_ClothingShoesAndJewelry.class));
    }
    
    public void electronicsAndComputers()
    {
    	startActivity(new Intent(this, Category_4_ElectronicsAndComputer.class));
    }
    
    public void homeGardenAndTools()
    {
    	startActivity(new Intent(this, Category_5_HomeGardenAndTools.class));
    }
    
    
    
    
    
    //TODO _______________[ Show Message Dialog Method) ]_______________
    public void showMessageDialog() 
    {
    	startActivity(new Intent(this, Message_Dialog.class));
	}
    
    
    
    
    
	//TODO __________[ AsyncTask Method for Exporting Database ]__________
	public class ExportCSVDatabaseCartList extends AsyncTask<String, Void, Boolean>
	{
		private final ProgressDialog d = new ProgressDialog(context);
		private final DatabaseDbHelper dh = new DatabaseDbHelper(context);
		
		@Override
		protected void onPreExecute()
		{
			this.d.setMessage("Redirecting to Checkout screen...");
			this.d.show();
		}
		
		@Override
		protected Boolean doInBackground(final String... args) 
		{
			File dbFile = getDatabasePath("AddToCartDatabase.db");
			System.out.println("Database name: " + dbFile);
			
			File exportDir = new File(Environment.getExternalStorageDirectory(), "");
			
			if(!exportDir.exists())
			{
				exportDir.mkdirs();
				dh.db =  dh.db;
			}
			
			File file = new File(exportDir, "CARTLIST - Galleon.csv");
			
			StringBuilder buildItemList = new StringBuilder();
			
			try
			{
				file.createNewFile();
				CSVWriter writeCSV = new CSVWriter(new FileWriter(file));
				
				Cursor c = dh.db.rawQuery(query, null);
				
				int current = 0;
				int next = 0;
				X = 0;
				
				while(c.moveToNext())
				{
					String[] arrString =
						{
								c.getString(0), // --> Item Index
								c.getString(1), // --> Item Name
								c.getString(2), // --> Price
								c.getString(3) // --> Product ID no.
						};
					
					System.out.println(c.getString(0) +
							" " + c.getString(1) + 
							" " + c.getString(2));
					
					buildItemList.append("Product ID no. " + c.getString(3) + ", ")
					       .append(WordCrop.truncate(c.getString(1), 15))
						   .append(" (Php ")
						   .append(c.getString(2))
						   .append(") ")
						   .append("- QTY: " + getQuantity[X].getText().toString())
						   .append("\n");
					
					next = Integer.parseInt(getSubtotal[X].getText().toString());
					current = current + next;
					X++;
					
					writeCSV.writeNext(arrString);
				}
				
				productID_Post = new String[X];
				c.moveToFirst();
				X = 0;
				
				while(c.moveToNext())
				{
					productID_Post[X] = c.getString(3);
					Log.v("PRODUCT ID", productID_Post[X]);
					X++;
				}
				
				new HttpPostProductIdHelper(productID_Post, context).execute();
				
				items = buildItemList.toString();
				costs = String.valueOf(current);
				
				writeCSV.close();
				c.close();
				dh.db.close();
				
				return true;
				
			} catch(SQLException e) {
				
				Log.e("AndroidStarter - SQL Status", e.getMessage(), e);
				return false;
				
			} catch(IOException e) {
				
				Log.e("AndroidStarter - I/O Status", e.getMessage(), e);
				return false;
				
			}
		}
		
		@Override
		protected void onPostExecute(Boolean success)
		{
			if(this.d.isShowing())
			{
				this.d.dismiss();
			}
			
			if (success)
	        {
	            Toast.makeText(ProductDetailActivity.this, "Successfully updated new item(s) to cart!", Toast.LENGTH_SHORT).show();
	            finish();
	            startActivity(new Intent(context, PurchaseActivity.class));

	        } else {

	            Toast.makeText(ProductDetailActivity.this, "Update failed. Please try again.", Toast.LENGTH_SHORT).show();

	        }
		}
	}
	
	public class ExportCSVDatabaseWishList extends AsyncTask<String, Void, Boolean>
	{
		private final ProgressDialog d = new ProgressDialog(context);
		private final DatabaseDbHelper dh = new DatabaseDbHelper(context);
		
		@Override
		protected void onPreExecute()
		{
			this.d.setMessage("Saving wish list. Please wait...");
			this.d.show();
		}
		
		@Override
		protected Boolean doInBackground(final String... args) 
		{
			File dbFile = getDatabasePath("WishListDatabase.db");
			System.out.println("Database name: " + dbFile);
			
			File exportDir = new File(Environment.getExternalStorageDirectory(), "");
			
			if(!exportDir.exists())
			{
				exportDir.mkdirs();
				dh.db =  dh.db;
			}
			
			File file = new File(exportDir, "Galleon - List of Wishes.csv");
			
			StringBuilder buildItemList = new StringBuilder();
			
			try
			{
				file.createNewFile();
				CSVWriter writeCSV = new CSVWriter(new FileWriter(file));
				
				Cursor c = dh.db.rawQuery(query2, null);
				
				int current = 0;
				int next = 0;
				X = 0;
				
				while(c.moveToNext())
				{
					String[] arrString =
						{
								c.getString(0), // --> Item Index
								c.getString(1), // --> Item Name
								c.getString(2), // --> Price
								c.getString(3), // --> Details
								c.getString(4) // --> Image URL
						};
					
					System.out.println(c.getString(0) +
							" " + c.getString(1) +
							" " + c.getString(2) +
							" " + c.getString(3) +
							" " + c.getString(4));
					
					buildItemList.append(WordCrop.truncate(c.getString(1), 20));
					
					next = Integer.parseInt(getSubtotal[X].getText().toString());
					current = current + next;
					X++;
					
					writeCSV.writeNext(arrString);
				}
				
				items = buildItemList.toString();
				
				writeCSV.close();
				c.close();
				dh.db.close();
				
				this.d.dismiss();
				
				return true;
				
			} catch(SQLException e) {
				
				Log.e("AndroidStarter - SQL Status", e.getMessage(), e);
				return false;
				
			} catch(IOException e) {
				
				Log.e("AndroidStarter - I/O Status", e.getMessage(), e);
				return false;
				
			}
		}
		
		@Override
		protected void onPostExecute(Boolean success)
		{
			if(this.d.isShowing())
			{
				this.d.dismiss();
			}
			
			if (success)
	        {
	            Toast.makeText(ProductDetailActivity.this, "Successfully updated new item(s) to wish list!", Toast.LENGTH_SHORT).show();

	        } else {

	            Toast.makeText(ProductDetailActivity.this, "Update failed. Please try again.", Toast.LENGTH_SHORT).show();

	        }
		}
	}
	
	
	
	
	
	//TODO _______________[ Text Change Listener ]_______________
	public void rowIndexGetter(final int maxIndex)
	{
		for(X = 0; X < maxIndex; X++) // --> Set each text listener. (TextWatcher)
		{
			Log.v("TEST 2", "" + X);
			
			getQuantity[X].addTextChangedListener(new TextWatcher() 
		    {
		    	private int subtotal;
		    	   
		    	@Override
		    	public void onTextChanged(CharSequence s, int start, int before, int count) 
		    	{
		    		for(X = 0; X < maxIndex; X++) // --> Reset the loop for the text change listener code for updating subtotal cost for each row.
		    		{
		    			try
			    		{
			    			subtotal = Integer.parseInt(getPrice[X].getText().toString()) * Integer.parseInt(getQuantity[X].getText().toString());
			    			   
			    			if(subtotal >= 1500)
				    		{
				    			Log.v("HIDDEN MESSAGE", "Shut up, no. " + subtotal);
				    		}
			    			   
			    		} catch(NumberFormatException e) {
			    			   
			    			subtotal = 0;
			    			Log.v("INVALID FORMAT", "Revert back to 0");
			    			   
			    		} catch(ArrayIndexOutOfBoundsException e) {
			    			   
			    			Log.v("ARRAY OUT", "" + X);
			    			   
			    		}
			    		   
			    		    getSubtotal[X].setText(String.valueOf(subtotal));
		    		    }
		    	   }
				
		    	   @Override
		    	   public void beforeTextChanged(CharSequence s, int start, int count, int after) 
		    	   {
		    		   // ? ? ?
		    	   }
				
		    	   @Override
		    	   public void afterTextChanged(Editable s) 
		    	   {
		    		   // ? ? ?
		    	   }
		     });
		}
	}
	
	
	
	
	
	//TODO _______________[ Database View ]_______________
	@SuppressWarnings("unused")
	@TargetApi(Build.VERSION_CODES.FROYO) 
	public void showListDatabase()
	{
		setContentView(R.layout.database_viewer);
		LinearLayout parent = (LinearLayout) findViewById(R.id.Scroll_Layout);
		
		dh = new DatabaseDbHelper(context);
		dh.db = dh.db;
		
		Cursor c = dh.db.rawQuery(query, null);
	    int count= c.getCount();
	    c.moveToFirst();
	    
	    LinearLayout mainLayout = new LinearLayout(getApplicationContext());
//	    mainLayout.setOrientation(LinearLayout.VERTICAL);
	    LinearLayout.LayoutParams params = new LinearLayout.LayoutParams
	    		(
	    				LinearLayout.LayoutParams.MATCH_PARENT,
	    				LinearLayout.LayoutParams.MATCH_PARENT
	    		);
	    
	    LinearLayout databaseLayout = new LinearLayout(getApplicationContext());
	    databaseLayout.setOrientation(LinearLayout.VERTICAL);
	    LinearLayout.LayoutParams dbParams = new LinearLayout.LayoutParams
	    		(
	    				LinearLayout.LayoutParams.MATCH_PARENT,
	    				0
	    		);
	    databaseLayout.setWeightSum(1);
	    
	    tableLayout = new TableLayout(getApplicationContext());
	    TableLayout.LayoutParams tblParams = new TableLayout.LayoutParams
	    		(
	    				LinearLayout.LayoutParams.MATCH_PARENT,
	    				10
	    		);
	    tableLayout.setWeightSum(0.5f);
	    tableLayout.setGravity(Gravity.RIGHT);
	    
	    Button checkout = (Button) findViewById(R.id.BTN_Checkout);
	    checkout.setOnClickListener(new View.OnClickListener() 
	    {
			@Override
			public void onClick(View v) 
			{
				try
		        {
					new ProductDetailActivity.ExportCSVDatabaseCartList().execute("");

		        } catch(Exception ex){
		              
		        	Log.e("Error in exporting database under AndroidStarter.",ex.toString());
		        	
		        }
			}
		});
	    
	    ScrollView scroll = new ScrollView(getApplicationContext());
	    scroll.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, 10));
	    scroll.setVerticalScrollBarEnabled(true);
	    scroll.setHorizontalScrollBarEnabled(true);
	    scroll.setVisibility(1);
	    
	    tableRow = new TableRow(getApplicationContext());
	  	
	  	productName = new TextView(getApplicationContext());
	  	productName.setText("ITEM");
	  	productName.setTextColor(Color.argb(255, 255, 119, 0));
	  	productName.setTypeface(null, Typeface.BOLD);
	  	productName.setPadding(20, 20, 20, 20);
	  	tableRow.addView(productName);
	  	
	  	price = new TextView(getApplicationContext());
	  	price.setText("PRICE");
	  	price.setTextColor(Color.argb(255, 255, 119, 0));
	  	price.setTypeface(null, Typeface.BOLD);
	  	price.setPadding(20, 20, 20, 20);
	  	tableRow.addView(price);
	  	
	  	quantity = new TextView(getApplicationContext());
	  	quantity.setText("QUANTITY");
	  	quantity.setTextColor(Color.argb(255, 255, 119, 0));
	  	quantity.setTypeface(null, Typeface.BOLD);
	  	quantity.setPadding(20, 20, 20, 20);
	  	tableRow.addView(quantity);
	  	
	  	subtotal = new TextView(getApplicationContext());
	  	subtotal.setText("SUBTOTAL");
	  	subtotal.setTextColor(Color.argb(255, 255, 119, 0));
	  	subtotal.setTypeface(null, Typeface.BOLD);
	  	subtotal.setPadding(20, 20, 20, 20);
	  	tableRow.addView(subtotal);
	  	
	  	tableRow.setBackgroundColor(Color.argb(255, 51, 51, 51));
	  	
	  	tableLayout.addView(tableRow, tblParams);
	  	
	    for (rowIndex = 0; rowIndex < count; rowIndex++)
	    {
	    	Log.v("ROW INDEX CHECK A", "" + rowIndex);
	    	
	    	if(rowIndex == 0)
	    	{
	    		getPrice = new TextView[count];
	    		getQuantity = new EditText[count];
	    		getSubtotal = new TextView[count];
	    	}
	    	
	       tableRow = new TableRow(getApplicationContext());
	       tableRow.setTag(rowIndex);
	       tableRow.setBackgroundColor(Color.argb(0, 255, 255, 255));
	       
	       getProductName = new TextView(getApplicationContext());
	       getProductName.setText(c.getString(c.getColumnIndex("productname")));
	       getProductName.setTextColor(Color.argb(255, 0, 0, 0));
	       
	       getPrice[rowIndex] = new TextView(getApplicationContext());
	       getPrice[rowIndex].setText(c.getString(c.getColumnIndex("price")));
	       getPrice[rowIndex].setTextColor(Color.argb(255, 0, 0, 0));
	       
	       getQuantity[rowIndex] = new EditText(getApplicationContext());
	       getQuantity[rowIndex].setText("1");
	       getQuantity[rowIndex].setBackgroundColor(Color.BLACK);
	       getQuantity[rowIndex].setTextColor(Color.WHITE);
	       
	       getSubtotal[rowIndex] = new TextView(getApplicationContext());
	       getSubtotal[rowIndex].setText(getPrice[rowIndex].getText().toString());
	       getSubtotal[rowIndex].setTextColor(Color.argb(255, 0, 0, 0));
	       
	       getProductName.setPadding(20, 20, 20, 20);
	       getPrice[rowIndex].setPadding(20, 20, 20, 20);
	       getQuantity[rowIndex].setPadding(20, 20, 20, 20);
	       getSubtotal[rowIndex].setPadding(20, 20, 20, 20);
	       
	       tableRow.addView(getProductName);
	       tableRow.addView(getPrice[rowIndex]);
	       tableRow.addView(getQuantity[rowIndex]);
	       tableRow.addView(getSubtotal[rowIndex]);
	       tableLayout.addView(tableRow, tblParams);
	       
	       c.moveToNext() ;
	    }
	    
	    rowIndexGetter(rowIndex);
	    
	    scroll.addView(tableLayout);
	    databaseLayout.addView(scroll, params);
	    
	    mainLayout.addView(databaseLayout);
	    
	    parent.addView(mainLayout);
	    
		// Orientation
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
	     
	    dh.db.close();
	}
	
	@SuppressWarnings("unused")
	@TargetApi(Build.VERSION_CODES.FROYO) 
	public void showWishListDatabase() //TODO Wish List
	{
		setContentView(R.layout.database_viewer);
//		ViewGroup parent = (ViewGroup) findViewById(R.id.LL_Database_Viewer_1);
		LinearLayout parent = (LinearLayout) findViewById(R.id.Scroll_Layout);
		
		dh = new DatabaseDbHelper(context);
		dh.db = dh.db;
		
		Cursor c = dh.db.rawQuery(query2, null);
	    int count= c.getCount();
	    c.moveToFirst();
	    
	    LinearLayout mainLayout = new LinearLayout(getApplicationContext());
	    mainLayout.setOrientation(LinearLayout.VERTICAL);
	    LinearLayout.LayoutParams params = new LinearLayout.LayoutParams
	    		(
	    				LinearLayout.LayoutParams.MATCH_PARENT,
	    				LinearLayout.LayoutParams.MATCH_PARENT
	    		);
	    
	    LinearLayout databaseLayout = new LinearLayout(getApplicationContext());
	    databaseLayout.setOrientation(LinearLayout.VERTICAL);
	    LinearLayout.LayoutParams dbParams = new LinearLayout.LayoutParams
	    		(
	    				LinearLayout.LayoutParams.MATCH_PARENT,
	    				0
	    		);
	    databaseLayout.setWeightSum(1);
	    
	    LinearLayout buttonLayout = new LinearLayout(getApplicationContext());
	    buttonLayout.setOrientation(LinearLayout.HORIZONTAL);
	    LinearLayout.LayoutParams buttonParams = new LinearLayout.LayoutParams
	    		(
	    				LinearLayout.LayoutParams.MATCH_PARENT,
	    				0
	    		);
	    buttonLayout.setWeightSum(1);
	    buttonLayout.setGravity(Gravity.CENTER);
	    
	    tableLayout = new TableLayout(getApplicationContext());
	    TableLayout.LayoutParams tblParams = new TableLayout.LayoutParams
	    		(
	    				LinearLayout.LayoutParams.MATCH_PARENT,
	    				10
	    		);
	    tableLayout.setWeightSum(0.5f);
	    tableLayout.setGravity(Gravity.RIGHT);
	    
	    Button exitButton = new Button(getApplicationContext());
	    exitButton.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));
	    exitButton.setWidth(150);
	    exitButton.setHeight(50);
	    exitButton.setText("Exit");
	    exitButton.setOnClickListener(new View.OnClickListener() 
	    {
			@Override
			public void onClick(View v) 
			{
				startActivity(new Intent(context, PurchaseActivity.class));
			}
		});
	    buttonLayout.addView(exitButton);
	    
	    Button exportButton = new Button(getApplicationContext());
	    exportButton.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));
	    exportButton.setWidth(180);
	    exportButton.setHeight(50);
	    exportButton.setText("Export Database");
	    exportButton.setOnClickListener(new View.OnClickListener() 
	    {
			@Override
			public void onClick(View v) 
			{
				try
		        {
					new ProductDetailActivity.ExportCSVDatabaseWishList().execute("");

		        } catch(Exception ex){
		              
		        	Log.e("Error in exporting database under AndroidStarter.",ex.toString());
		        	
		        }
			}
		});
	    buttonLayout.addView(exportButton);
	    
	    tableLayout.addView(buttonLayout);
	    
	    ScrollView scroll = new ScrollView(getApplicationContext());
	    scroll.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, 10));
	    scroll.setVerticalScrollBarEnabled(true);
	    scroll.setHorizontalScrollBarEnabled(true);
	    scroll.setVisibility(1);
	    
	    tableRow = new TableRow(getApplicationContext());
	  	
	  	productName = new TextView(getApplicationContext());
	  	productName.setText("ITEM");
	  	productName.setTextColor(Color.argb(255, 255, 119, 0));
	  	productName.setTypeface(null, Typeface.BOLD);
	  	productName.setPadding(20, 20, 20, 20);
	  	tableRow.addView(productName);
	  	
	  	price = new TextView(getApplicationContext());
	  	price.setText("PRICE");
	  	price.setTextColor(Color.argb(255, 255, 119, 0));
	  	price.setTypeface(null, Typeface.BOLD);
	  	price.setPadding(20, 20, 20, 20);
	  	tableRow.addView(price);
	  	
	  	details = new TextView(getApplicationContext());
	  	details.setText("DETAILS");
	  	details.setTextColor(Color.argb(255, 255, 119, 0));
	  	details.setTypeface(null, Typeface.BOLD);
	  	details.setPadding(20, 20, 20, 20);
	  	tableRow.addView(details);
	  	
	  	imageUrl = new TextView(getApplicationContext());
	  	imageUrl.setText("IMAGE URL");
	  	imageUrl.setTextColor(Color.argb(255, 255, 119, 0));
	  	imageUrl.setTypeface(null, Typeface.BOLD);
	  	imageUrl.setPadding(20, 20, 20, 20);
	  	tableRow.addView(imageUrl);
	  	
	  	tableRow.setBackgroundColor(Color.argb(255, 51, 51, 51));
	  	
	  	tableLayout.addView(tableRow, tblParams);
	  	
	    for (rowIndex = 0; rowIndex < count; rowIndex++)
	    {
	    	Log.v("ROW INDEX CHECK A", "" + rowIndex);
	    	
	    	if(rowIndex == 0)
	    	{
	    		getPrice = new TextView[count];
	    		getQuantity = new EditText[count];
	    		getSubtotal = new TextView[count];
	    	}
	    	
	       tableRow = new TableRow(getApplicationContext());
	       tableRow.setTag(rowIndex);
	       tableRow.setBackgroundColor(Color.argb(0, 255, 255, 255));
	       
	       getProductName = new TextView(getApplicationContext());
	       getProductName.setText(c.getString(c.getColumnIndex(DatabaseTracker.WishList.PRODUCT_NAME)));
	       getProductName.setTextColor(Color.argb(255, 0, 0, 0));
	       
	       getPrice[rowIndex] = new TextView(getApplicationContext());
	       getPrice[rowIndex].setText(c.getString(c.getColumnIndex(DatabaseTracker.WishList.PRICE)));
	       getPrice[rowIndex].setTextColor(Color.argb(255, 0, 0, 0));
	       
	       getDetails = new TextView(getApplicationContext());
	       getDetails.setText(c.getString(c.getColumnIndex(DatabaseTracker.WishList.DETAILS)));
	       getDetails.setTextColor(Color.argb(255, 0, 0, 0));
	       
	       getImageURL = new TextView(getApplicationContext());
	       getImageURL.setText(c.getString(c.getColumnIndex(DatabaseTracker.WishList.IMAGE_URL)));
	       getImageURL.setTextColor(Color.argb(255, 0, 0, 0));
	       
	       getProductName.setPadding(20, 20, 20, 20);
	       getPrice[rowIndex].setPadding(20, 20, 20, 20);
	       getDetails.setPadding(20, 20, 20, 20);
	       getImageURL.setPadding(20, 20, 20, 20);
	       
	       tableRow.addView(getProductName);
	       tableRow.addView(getPrice[rowIndex]);
	       tableRow.addView(getDetails);
	       tableRow.addView(getImageURL);
	       tableLayout.addView(tableRow, tblParams);
	       
	       c.moveToNext() ;
	    }
	    
//	    rowIndexGetter(rowIndex);
	    
	    scroll.addView(tableLayout);
	    databaseLayout.addView(scroll, params);
	    
	    mainLayout.addView(databaseLayout);
	    
	    parent.addView(mainLayout);
	    
		// Orientation
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
	     
	    dh.db.close();
	}
	
	
	
	
	
	//TODO _______________[ On Click Listener for Product Detail Menu Buttons ]_______________
	public void addToCart(View v) 
	{
		RegistrationListener.getDetailResponse(context);
		finish();
		startActivity(new Intent(this, CartListActivity.class));
	}
	
	public void wishList(View v) 
	{
		RegistrationListener.getWishListResponse(context);
		finish();
		startActivity(new Intent(this, WishListActivity.class));
	}
	
	public void buy(View v)
	{
		// If database is empty, add it automatically and check if there is any duplication.
		RegistrationListener.getDetailResponse(context);
		
		// Update and execute list of products to be added in the cart list.
		try
        {
//			// Get the database and look for the max index.
//			dh = new DatabaseDbHelper(context);
//			dh.db = dh.db;
//			Cursor c = dh.db.rawQuery(query2, null);
//		    rowIndex = c.getCount();
		    
		    // Execute and update!
			showListDatabase();
			new ProductDetailActivity.ExportCSVDatabaseCartList().execute("");

        } catch(Exception ex){
            
        	// Check for any signs of exception.
        	Log.e("Error in exporting database under AndroidStarter.", ex.toString());
        	
        }
	}
	
	public void back(View v)
	{
		startActivity(new Intent(context, ProductDetailActivity.class));
	}
}