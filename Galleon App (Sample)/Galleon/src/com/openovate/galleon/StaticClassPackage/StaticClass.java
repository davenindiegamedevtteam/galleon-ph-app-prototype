package com.openovate.galleon.StaticClassPackage;

import android.graphics.Bitmap;

import java.util.LinkedHashMap;

/**
 * Created by andybooboo on 8/3/13.
 */
public class StaticClass {
    public static LinkedHashMap<String, Bitmap> bitmapCache;
}
