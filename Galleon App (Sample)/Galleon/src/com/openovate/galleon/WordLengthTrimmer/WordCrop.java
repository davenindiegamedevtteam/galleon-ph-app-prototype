package com.openovate.galleon.WordLengthTrimmer;

/**
 *
 * <i> 
 * Derby - Class org.apache.derby.iapi.util.PropertyUtil <br>
 * 
 * Licensed to the Apache Software Foundation (ASF) under one or more<br>
 * contributor license agreements.  See the NOTICE file distributed with<br>
 * this work for additional information regarding copyright ownership.<br>
 * The ASF licenses this file to you under the Apache License, Version 2.0<br>
 * (the "License"); you may not use this file except in compliance with<br>
 * the License.  You may obtain a copy of the License at<br><br>
 * 
 * http://www.apache.org/licenses/LICENSE-2.0<br><br>
 * 
 * Unless required by applicable law or agreed to in writing, software<br>
 * distributed under the License is distributed on an "AS IS" BASIS,<br>
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.<br>
 * See the License for the specific language governing permissions and<br>
 * limitations under the License.<br><br>
 * 
 * See tutorial at:
 * <b>http://www.java2s.com/Tutorial/Java/0040__Data-Type/TruncateaStringtothegivenlengthwithnowarningsorerrorraisedifitisbigger.htm</b>
 * 
 * <br><br><br><br>
 * Revised by David Coronado Dimalanta.
 * </i>
 * 
 */

public class WordCrop // --> Original class name: Main
{
	 /**
	  * Truncate a String to the given length with no warnings
	  * or error raised if it is bigger.
	  * 
	  *  @param  value String to be truncated
	  *  @param  length  Maximum length of string
	  * 
	  * @return Returns value if value is null or value.length() is less or equal to than length, otherwise a String representing
	  * value truncated to length.
	  */
	
	public static String truncate(String value, int length)
	{
	 	if (value != null && value.length() > length)
	    {
	 		value = value.substring(0, length) + "...";
	    }
	 	
	 	return value;
	}
}
