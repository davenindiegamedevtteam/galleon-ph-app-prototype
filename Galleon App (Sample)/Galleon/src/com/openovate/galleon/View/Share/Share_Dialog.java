package com.openovate.galleon.View.Share;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.actionbarsherlock.view.Window;
import com.openovate.galleon.ProjectGalleonMainActivity;
import com.openovate.galleon.R;
import com.openovate.galleon.WebViewActivity;
import com.openovate.galleon.Model.Flaggers.Flag;

public class Share_Dialog extends Activity
{
	//TODO _______________[ Activity Overriden Methods ]_______________
	@SuppressLint("InlinedApi")
	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		requestWindowFeature((int) Window.FEATURE_NO_TITLE);
        setContentView(R.layout.share_dialog_base);
	}
	
	
	
	
	
	//TODO _______________[ Button On Click Events for the Dialog ]_______________
	public void twitter(View v)
	{
		System.out.println("TWITTER");
		Flag.dialogResult = "Twitter";
		Intent intent = new Intent(Share_Dialog.this, WebViewActivity.class);
//		finish();	
		startActivity(intent);
	}
	
	public void facebook(View v)
	{
		System.out.println("FACEBOOK");
		Intent intent = new Intent(Share_Dialog.this, WebViewActivity.class);
		Flag.dialogResult = "Facebook";
//		finish();
		startActivity(intent);
	}
}
