package com.openovate.galleon.View.Category;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.GridView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;
import android.widget.Toast;

import com.actionbarsherlock.view.Window;
import com.openovate.galleon.R;
import com.openovate.galleon.Model.ItemLists.SubcategorySchema;
import com.openovate.galleon.Model.URLFinder.JSONSourceURLFinder;
import com.openovate.galleon.jsonParser.utils.DownloadContentAsyncTask;

public class Category_1_ToysKidsandBaby extends Activity 
{
	//TODO _______________[ JSON Source URL ]_______________
	private JSONSourceURLFinder url;
	public static int index = 0;
	private int subindex = 0;
	
	
	
	
	
	//TODO _______________[ Field Objects ]_______________
	private Spinner subcategory;
	static public GridView grid;
	
	
	
	
	
	//TODO _______________[ Constructor ]_______________
	public Category_1_ToysKidsandBaby()
	{
		// ? ? ?
	}
	
	
	
	
	
	//TODO _______________[ Activity Overriden Method(s) ]_______________
	@Override
    protected void onCreate(Bundle savedInstanceState) 
    {
        super.onCreate(savedInstanceState);
        
        requestWindowFeature((int) Window.FEATURE_NO_TITLE);
        switch (getResources().getDisplayMetrics().densityDpi) 
    	{
    	case DisplayMetrics.DENSITY_LOW:
//    		LinearLayout messageLayout = (LinearLayout) findViewById(R.id.Category_Message);
//    		LinearLayout searchLayout = (LinearLayout) findViewById(R.id.Category_Search_Layout);
//    		LinearLayout browseLayout = (LinearLayout) findViewById(R.id.Category_Browse_Layout);
//    		messageLayout.setVisibility(View.GONE);
//    		searchLayout.setBackgroundColor(getResources().getColor(R.color.orange));
//    		browseLayout.setBackgroundColor(getResources().getColor(R.color.orange));
    	    break;
    	}
        setContentView(R.layout.category_dialog_base);
//        StaticClass.bitmapCache = new LinkedHashMap<String, Bitmap>();
        
        url = new JSONSourceURLFinder();
        new DownloadContentAsyncTask(url.getURL1(0, subindex), (GridView) findViewById(R.id.poductUrlList), getApplicationContext()).execute();
        
        final Dialog dialog = new Dialog(this);
        
        final EditText search = (EditText) findViewById(R.id.ET_Search);
        search.setOnEditorActionListener(new OnEditorActionListener() 
        {
			@Override
			public boolean onEditorAction(TextView v, int actionId, KeyEvent event) 
			{
				index = 0;
				Toast.makeText(getBaseContext(), "Searching...", Toast.LENGTH_LONG).show();
				new DownloadContentAsyncTask(url.getURL0(1, search.getText().toString()), (GridView) findViewById(R.id.poductUrlList), getBaseContext()).execute();
				return true;
			}
		});
        
        subcategory = (Spinner) findViewById(R.id.SP_Subcategory);
        subcategory.setAdapter(new SubcategorySchema(getApplicationContext()).getListOfCChoices1());
        subcategory.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() 
        {
			@Override
			public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) 
			{
				String value = subcategory.getItemAtPosition(i).toString();
				System.out.println(value);
				
				if(value.equals("All"))
				{
					index = 0;
					new DownloadContentAsyncTask(url.getURL1(0, 0), (GridView) findViewById(R.id.poductUrlList), getApplicationContext()).execute();
					
				} else if(value.equals("Toys and Games Only")) {
					
					dialog.requestWindowFeature((int) Window.FEATURE_NO_TITLE);
					dialog.setContentView(R.layout.subcategory_dialog_base);
					dialog.show();
//					new ListContentAsyncTask((ListView) findViewById(R.id.LV_Subcategory_List), getApplicationContext());
					final GridView list = (GridView) dialog.findViewById(R.id.GV_Subcategory_List);
					list.setAdapter(new SubcategorySchema(getApplicationContext()).getSublistOfCChoices1());
//					list.setOnItemClickListener(new SubcategoryforToysAndGames(index, list, dialog, url, new CategoryToysKidsandBaby()));
					list.setOnItemClickListener(new AdapterView.OnItemClickListener() 
					{
						@Override
						public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) 
						{
							String value = list.getItemAtPosition(i).toString();
							System.out.println(value);
							
							if(value.equals("All"))
							{
								index = 1;
								new DownloadContentAsyncTask(url.getURL1(1, 0), (GridView) findViewById(R.id.poductUrlList), getApplicationContext()).execute();
								dialog.hide();
								Log.v("SUBCATEGORY of TOYS AND GAMES ONLY", "All Contents");
								
							} else if(value.equals("Toy RC and Play Vehicles")) {
								
								index = 2;
								new DownloadContentAsyncTask(url.getURL1(2, 0), (GridView) findViewById(R.id.poductUrlList), getApplicationContext()).execute();
								dialog.hide();
								Log.v("SUBCATEGORY of TOYS AND GAMES ONLY", "Toy RC and Play Vehicles");
								
							} else if(value.equals("Action and Toy Figures")) {
								
								index = 3;
								new DownloadContentAsyncTask(url.getURL1(3, 0), (GridView) findViewById(R.id.poductUrlList), getApplicationContext()).execute();
								dialog.hide();
								Log.v("SUBCATEGORY of TOYS AND GAMES ONLY", "Action and Toy Figures");
								
							} else if(value.equals("Lego")) {
								
								index = 4;
								new DownloadContentAsyncTask(url.getURL1(4, 0), (GridView) findViewById(R.id.poductUrlList), getApplicationContext()).execute();
								dialog.hide();
								Log.v("SUBCATEGORY of TOYS AND GAMES ONLY", "Lego");
								
							} else if(value.equals("Robots")) {
								
								index = 5;
								new DownloadContentAsyncTask(url.getURL1(5, 0), (GridView) findViewById(R.id.poductUrlList), getApplicationContext()).execute();
								dialog.hide();
								Log.v("SUBCATEGORY of TOYS AND GAMES ONLY", "Robots");
								
							}
						}
					});
					
				} else if(value.equals("Videogames (PlayStation 3)")) {
					
					index = 6;
					new DownloadContentAsyncTask(url.getURL1(6, 0), (GridView) findViewById(R.id.poductUrlList), getApplicationContext()).execute();
					
				}
				
				// Check the total number of subindexes per index.
				Log.v("NUMBER OF SUBINDEXES", "" + url.length(index, 1));
				Log.v("CURRENT SUBCATEGORY", "Number " + index);
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) 
			{
				// ? ? ?
			}
		});
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) 
    {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.project_galleon_main, menu);
        return true;
    }
    
    
    
    
    
    //TODO _______________[ Button Listeners ]_______________
    public void next(View v)
    {
    	subindex++;
    	
    	// Check the right total number of subindexes according to the current index.
    	try
		{
			new DownloadContentAsyncTask(url.getURL1(index, subindex), (GridView) findViewById(R.id.poductUrlList), getApplicationContext()).execute();
    		
		} catch(ArrayIndexOutOfBoundsException e) {
			
			subindex = (url.length(index, 1) - 1);
			Log.v("ARRAY INDEX OUT OF BOUNDS", "Revert back to the last current index.");
			
		} finally {
			
			new DownloadContentAsyncTask(url.getURL1(index, subindex), (GridView) findViewById(R.id.poductUrlList), getApplicationContext()).execute();
			
		}
    	
    	Log.v("NEXT INDEX", "" + subindex);
    }
    
    public void prev(View v)
    {
    	subindex--;
    	
    	if(subindex > -1)
    	{
    		new DownloadContentAsyncTask(url.getURL1(index, subindex), (GridView) findViewById(R.id.poductUrlList), getApplicationContext()).execute();
    		
    	} else {
    		
    		subindex = 0;
    		
    	}
    	
    	Log.v("PREV INDEX", "" + subindex);
    }
}
