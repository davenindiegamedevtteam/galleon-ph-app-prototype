package com.openovate.galleon.View.Category;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.GridView;
import android.widget.Spinner;

import com.actionbarsherlock.view.Window;
import com.openovate.galleon.R;
import com.openovate.galleon.Model.ItemLists.SubcategorySchema;
import com.openovate.galleon.Model.URLFinder.JSONSourceURLFinder;
import com.openovate.galleon.jsonParser.utils.DownloadContentAsyncTask;

public class Category_4_ElectronicsAndComputer extends Activity 
{
	//TODO _______________[ JSON Source URL ]_______________
	private JSONSourceURLFinder url;
	public static int index = 0;
	private int subindex = 0;
	
	
	
	
	
	//TODO _______________[ Field Objects ]_______________
	private Spinner subcategory;
	static public GridView grid;
	
	
	
	
	
	//TODO _______________[ Constructor ]_______________
	public Category_4_ElectronicsAndComputer()
	{
		// ? ? ?
	}
	
	
	
	
	
	//TODO _______________[ Activity Overriden Method(s) ]_______________
	@Override
    protected void onCreate(Bundle savedInstanceState) 
    {
        super.onCreate(savedInstanceState);
        
        requestWindowFeature((int) Window.FEATURE_NO_TITLE);
        setContentView(R.layout.category_dialog_base);
//        StaticClass.bitmapCache = new LinkedHashMap<String, Bitmap>();
        
        url = new JSONSourceURLFinder();
        new DownloadContentAsyncTask(url.getURL4(0, subindex), (GridView) findViewById(R.id.poductUrlList), getApplicationContext()).execute();
        
        final Dialog dialog = new Dialog(this);
        
        subcategory = (Spinner) findViewById(R.id.SP_Subcategory);
        subcategory.setAdapter(new SubcategorySchema(getApplicationContext()).getListOfCChoices4());
        subcategory.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() 
        {
			@Override
			public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) 
			{
				String value = subcategory.getItemAtPosition(i).toString();
				System.out.println(value);
				
				if(value.equals("All"))
				{
					index = 0;
					new DownloadContentAsyncTask(url.getURL4(0, 0), (GridView) findViewById(R.id.poductUrlList), getApplicationContext()).execute();
					
				} else if(value.equals("MP3 Players and Accessories")) {
					
					dialog.requestWindowFeature((int) Window.FEATURE_NO_TITLE);
					dialog.setContentView(R.layout.subcategory_dialog_base);
					dialog.show();
					final GridView list = (GridView) dialog.findViewById(R.id.GV_Subcategory_List);
					list.setAdapter(new SubcategorySchema(getApplicationContext()).getSublistOfCChoices4(1));
					list.setOnItemClickListener(new AdapterView.OnItemClickListener() 
					{
						@Override
						public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) 
						{
							String value = list.getItemAtPosition(i).toString();
							System.out.println(value);
							
							if(value.equals("Accessories"))
							{
								index = 1;
								new DownloadContentAsyncTask(url.getURL4(1, 0), (GridView) findViewById(R.id.poductUrlList), getApplicationContext()).execute();
								dialog.hide();
								Log.v("SUBCATEGORY of MP3 PLAYERS AND ACCESSORIES", "Accessories");
							}
						}
					});
					
				} else if(value.equals("Home Audio and Home Theater")) {
					
					dialog.requestWindowFeature((int) Window.FEATURE_NO_TITLE);
					dialog.setContentView(R.layout.subcategory_dialog_base);
					dialog.show();
					final GridView list = (GridView) dialog.findViewById(R.id.GV_Subcategory_List);
					list.setAdapter(new SubcategorySchema(getApplicationContext()).getSublistOfCChoices4(2));
					list.setOnItemClickListener(new AdapterView.OnItemClickListener() 
					{
						@Override
						public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) 
						{
							String value = list.getItemAtPosition(i).toString();
							System.out.println(value);
							
							if(value.equals("All"))
							{
								index = 2;
								new DownloadContentAsyncTask(url.getURL4(2, 0), (GridView) findViewById(R.id.poductUrlList), getApplicationContext()).execute();
								dialog.hide();
								Log.v("SUBCATEGORY of HOME AUDIO AND THEATER", "All Contents");
								
							} else if(value.equals("Headphones")) {
								
								index = 3;
								new DownloadContentAsyncTask(url.getURL4(3, 0), (GridView) findViewById(R.id.poductUrlList), getApplicationContext()).execute();
								dialog.hide();
								Log.v("SUBCATEGORY of HOME AUDIO AND THEATER", "Headphones");
								
							} else if(value.equals("Subwoofer")) {
								
								index = 4;
								new DownloadContentAsyncTask(url.getURL4(4, 0), (GridView) findViewById(R.id.poductUrlList), getApplicationContext()).execute();
								dialog.hide();
								Log.v("SUBCATEGORY of HOME AUDIO AND THEATER", "Subwoofer");
								
							} else if(value.equals("Speaker System")) {
								
								index = 5;
								new DownloadContentAsyncTask(url.getURL4(5, 0), (GridView) findViewById(R.id.poductUrlList), getApplicationContext()).execute();
								dialog.hide();
								Log.v("SUBCATEGORY of HOME AUDIO AND THEATER", "Speaker System");
								
							}
						}
					});
					
				}
				
				// Check the total number of subindexes per index.
				Log.v("NUMBER OF SUBINDEXES", "" + url.length(index, 2));
				Log.v("CURRENT SUBCATEGORY", "Number " + index);
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) 
			{
				// ? ? ?
			}
		});
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) 
    {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.project_galleon_main, menu);
        return true;
    }
    
    
    
    
    
    //TODO _______________[ Button Listeners ]_______________
    public void next(View v)
    {
    	subindex++;
    	
    	// Check the right total number of subindexes according to the current index.
    	try
		{
			new DownloadContentAsyncTask(url.getURL4(index, subindex), (GridView) findViewById(R.id.poductUrlList), getApplicationContext()).execute();
    		
		} catch(ArrayIndexOutOfBoundsException e) {
			
			subindex = (url.length(index, 4) - 1);
			Log.v("ARRAY INDEX OUT OF BOUNDS", "Revert back to the last current index.");
			
		} finally {
			
			new DownloadContentAsyncTask(url.getURL4(index, subindex), (GridView) findViewById(R.id.poductUrlList), getApplicationContext()).execute();
			
		}
    	
    	Log.v("NEXT INDEX", "" + subindex);
    }
    
    public void prev(View v)
    {
    	subindex--;
    	
    	if(subindex > -1)
    	{
    		new DownloadContentAsyncTask(url.getURL4(index, subindex), (GridView) findViewById(R.id.poductUrlList), getApplicationContext()).execute();
    		
    	} else {
    		
    		subindex = 0;
    		
    	}
    	
    	Log.v("PREV INDEX", "" + subindex);
    }
}
