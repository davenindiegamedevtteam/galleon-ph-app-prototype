package com.openovate.galleon.View.Category;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.View;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;

import com.actionbarsherlock.view.Window;
import com.openovate.galleon.ProjectGalleonMainActivity;
import com.openovate.galleon.R;
import com.openovate.galleon.Controls.CacheManagement.Cache;
import com.openovate.galleon.Model.URLFinder.JSONSourceURLFinder;
import com.openovate.galleon.jsonParser.utils.SearchDownloadContentAsyncTask;

public class Category_Custom_Search extends Activity 
{
	//TODO _______________[ JSON Source URL ]_______________
	private JSONSourceURLFinder url;
	private static int no_of_pages = 1;
	public static int index = 0;
	
	
	
	
	
	//TODO _______________[ Field Objects ]_______________
	static private EditText search;
	static public String search_counter; // --> Counters the errors in NEXT or PREV page of the current word and as a flag.
	static public final String SEARCH_NOT_FOUND = "keyword unknown";
	
	
	
	
	
	//TODO _______________[ Constructor ]_______________
	public Category_Custom_Search()
	{
		// ? ? ?
	}
	
	
	
	
	
	//TODO _______________[ Activity Overriden Method(s) ]_______________
	@Override
    protected void onCreate(Bundle savedInstanceState) 
    {
        super.onCreate(savedInstanceState);
        
        requestWindowFeature((int) Window.FEATURE_NO_TITLE);
        setContentView(R.layout.search_dialog_base);
        
        Cache.showCacheDirectory(getApplicationContext());
        
        no_of_pages = 1; // --> Reset page back to 1 everytime the dialog re-opens.
        
        url = new JSONSourceURLFinder();
        new SearchDownloadContentAsyncTask(url.getURL0(no_of_pages, ProjectGalleonMainActivity.search.getText().toString()), (GridView) findViewById(R.id.poductUrlList), getApplicationContext()).execute();
        
        search_counter = ProjectGalleonMainActivity.search.getText().toString();
        
        final ProjectGalleonMainActivity dialog = new ProjectGalleonMainActivity();
        
        search = (EditText) findViewById(R.id.ET_Search);
        search.setOnEditorActionListener(new OnEditorActionListener() 
        {
			@Override
			public boolean onEditorAction(TextView v, int actionId, KeyEvent event) 
			{
				// Check if there is a blank textbox or with space at the start.
				if(!(search.getText().toString().equalsIgnoreCase("x")) || !(search.getText().toString().equalsIgnoreCase(null)))
				{
					no_of_pages = 1; // --> Set the page back to 1
					search_counter = search.getText().toString(); // --> Set current keyword.
					parseJson(false); // --> For debugging purposes and testing only.
					new SearchDownloadContentAsyncTask(url.getURL0(no_of_pages, search_counter), (GridView) findViewById(R.id.poductUrlList), getBaseContext()).execute();
					
				} else {
					
					dialog.showMessageDialog();
					
				}
				
				return true;
			}
		});
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) 
    {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.project_galleon_main, menu);
        return true;
    }
    
    
    
    
    
    
    //TODO _______________[ Test JSON Parsing Method ]_______________
    private void parseJson(boolean debug)
	{
	    String strJson="{\n\"000000000000000\":[ \n    {\n        \"employee_boycode\": \"00\",\n        \"id\": \"000\",\n        \"address\": \"abcdef\",\n        \"name\": \"Lyn Minmay\",\n        \"bankcode\": \"abc\",\n        \"branch_name\": \"abcd\",\n        \"account_no\": \"789\"\n    }\n]\n}\n";

	    if(debug)
	    {
	    	try 
	    	{
	    	    JSONObject jsnJsonObject = new JSONObject(strJson);

	    	    JSONArray contacts = jsnJsonObject.getJSONArray("000000000000000");

	    	    for (int i = 0; i < contacts.length(); i++)
	    	    {       
	    	        JSONObject c = contacts.getJSONObject(i);
	    	        String id = c.getString("id");
	    	        String boy_code = c.getString("employee_boycode");
	    	        String name = c.getString("name");
	    	        String address = c.getString("address");
	    	        String branch_name = c.getString("branch_name");
	    	        String bankcode = c.getString("bankcode");
	    	        String account_no = c.getString("account_no");

	    	        Log.v("PARSED DATA STATUS","\nID: "+id+"\nEmployee Boy Code: "+boy_code+"\nName: "+name+"\nAddress: "+address+"\nBranch Name: "+branch_name+"\nBank Code: "+bankcode+"\nAccount Number: "+account_no);
	    	    }

	    	  } catch (JSONException e) {

	    	    e.printStackTrace();
	    	        
	    	  }
	    }
	}
    
    
    
    
    
    //TODO _______________[ Button Listeners for Browsing Pages ]_______________
    public void next(View v) throws JSONException
    {
    	no_of_pages++;
    	
    	// Check the right total number of index.
    	try
		{
			new SearchDownloadContentAsyncTask(url.getURL0(no_of_pages, search_counter), (GridView) findViewById(R.id.poductUrlList), getApplicationContext()).execute();
    		
		} finally {
			
			new SearchDownloadContentAsyncTask(url.getURL0(no_of_pages, search_counter), (GridView) findViewById(R.id.poductUrlList), getApplicationContext()).execute();
			
		}
    	
    	Log.v("NEXT PAGE", "" + no_of_pages);
    	Log.v("CURRENT WORD", search_counter);
    }
    
    public void prev(View v)
    {
    	no_of_pages--;
    	
    	if(no_of_pages > 0)
    	{
    		new SearchDownloadContentAsyncTask(url.getURL0(no_of_pages, search_counter), (GridView) findViewById(R.id.poductUrlList), getApplicationContext()).execute();
    		
    	} else {
    		
    		no_of_pages = 1;
    		
    	}
    	
    	Log.v("PREV PAGE", "" + no_of_pages);
    	Log.v("CURRENT WORD", search_counter);
    }
}
