package com.openovate.galleon.View.Category;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;

import com.actionbarsherlock.view.Window;
import com.openovate.galleon.R;
import com.openovate.galleon.Model.ItemLists.CategorySchema;

public class Category_Select_Dialog_Pane extends Activity 
{
	//TODO _______________[ Field Objects ]_______________
	private GridView category;
	
	
	
	
	
	//TODO _______________[ Constructor ]_______________
	public Category_Select_Dialog_Pane()
	{
		// ? ? ?
	}
	
	
	
	
	
	//TODO _______________[ Activity Overriden Method(s) ]_______________
	@Override
    protected void onCreate(Bundle savedInstanceState) 
    {
        super.onCreate(savedInstanceState);
        
        requestWindowFeature((int) Window.FEATURE_NO_TITLE);
        setContentView(R.layout.select_category_dialog_base);
        
        category = (GridView) findViewById(R.id.GV_Category_List);
        category.setAdapter(new CategorySchema(getApplicationContext()).getListOfCategories(2));
        category.setOnItemClickListener(new AdapterView.OnItemClickListener() 
        {
			@Override
			public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) 
			{
				String value = category.getItemAtPosition(i).toString();
				System.out.println(value);
				
				if(value.equals("Toys, Kids, and Baby"))
				{
					toysKidsandBaby();
					
				} else if(value.equals("Sports and Outdoors")) {
					
					sportsAndOutdoors();
					
				} else if(value.equals("Clothing, Shoes, and Jewelry")) {
					
					clothingShoesAndJewelry();
					
				} else if(value.equals("Electronics and Computers")) {
					
					electronicsAndComputers();
					
				} else if(value.equals("Home, Garden, and Tools")) {
					
					homeGardenAndTools();
					
				} else {
					
					System.exit(0);
					
				}
			}
		});
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) 
    {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.project_galleon_main, menu);
        return true;
    }
    
    
    
    
    
    //TODO _______________[ On Method Callbacks for Category Dialogs ]_______________
    public void toysKidsandBaby()
    {
    	startActivity(new Intent(this, Category_1_ToysKidsandBaby.class));
    }
    
    public void sportsAndOutdoors()
    {
    	startActivity(new Intent(this, Category_2_SportsAndOutdoors.class));
    }
    
    public void clothingShoesAndJewelry()
    {
    	startActivity(new Intent(this, Category_3_ClothingShoesAndJewelry.class));
    }
    
    public void electronicsAndComputers()
    {
    	startActivity(new Intent(this, Category_4_ElectronicsAndComputer.class));
    }
    
    public void homeGardenAndTools()
    {
    	startActivity(new Intent(this, Category_5_HomeGardenAndTools.class));
    }
}
