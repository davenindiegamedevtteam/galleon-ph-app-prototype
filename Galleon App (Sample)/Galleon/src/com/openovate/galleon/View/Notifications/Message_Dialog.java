package com.openovate.galleon.View.Notifications;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.actionbarsherlock.view.Window;
import com.openovate.galleon.ProjectGalleonMainActivity;
import com.openovate.galleon.PurchaseActivity;
import com.openovate.galleon.R;
import com.openovate.galleon.Controls.CacheManagement.Cache;
import com.openovate.galleon.Model.Flaggers.Flag;
import com.openovate.galleon.View.Category.Category_Custom_Search;

public class Message_Dialog extends Activity
{
	//TODO _______________[ Activity Overriden Methods ]_______________
	@SuppressLint("InlinedApi")
	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		
		requestWindowFeature((int) Window.FEATURE_NO_TITLE);
        setContentView(R.layout.message_dialog_base);
        
        TextView msg = (TextView) findViewById(R.id.TV_Message);
        ImageView status = (ImageView) findViewById(R.id.IV_Message_Status);
        System.out.println("FLAG " + Flag.dialogResult);
        
        try
        {
        	if(Flag.dialogResult.equals("BDO (Bank Deposit)"))
            {
        		setContentView(R.layout.message_dialog_base_alt);
        		msg = (TextView) findViewById(R.id.TV_Message);
        		
            	int size = 0;
            	switch (getResources().getDisplayMetrics().densityDpi) 
            	{
            	case DisplayMetrics.DENSITY_LOW:
            	    size = 7;
            		Log.v("DENSITY STATUS", "LDPI " + size);
            	    break;
            	    
            	case DisplayMetrics.DENSITY_MEDIUM:
            	    size = 7;
            		Log.v("DENSITY STATUS", "HDPI " + size);
            	    break;
            	    
            	case DisplayMetrics.DENSITY_HIGH:
            	    size = 12;
            	    Log.v("DENSITY STATUS", "HDPI " + size);
            	    break;
            	    
            	case DisplayMetrics.DENSITY_TV:
            	    size = 20;
            	    Log.v("DENSITY STATUS", "TVDPI 	" + size);
            	    break;
            	    
            	case DisplayMetrics.DENSITY_XHIGH:
            	    size = 24;
            		Log.v("DENSITY STATUS", "XHDPI " + size);
            	    break;
            	    
            	case DisplayMetrics.DENSITY_XXHIGH:
            	    size = 28;
            		Log.v("DENSITY STATUS", "XXHDPI " + size);
            	    break;
            	}
            	
            	msg.setText("In order to facilitate the purchase," +
            			    "\nkindly deposit your payment to the" +
            			    "\nfollowing bank account:");
            	msg.setTextSize(size);
            	
//            	LinearLayout base = (LinearLayout) findViewById(R.id.Message_Dialog_Base);
            	LinearLayout message = (LinearLayout) findViewById(R.id.Message);
            	
//            	try
//            	{
//            		base.setLayoutParams(new LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, R.dimen.message_dialog_height_expanded));
//            		
//            	} catch(ClassCastException e) {
//            		
//            		Log.e("CASTING STATUS", "canceled");
//            		
//            	}
            	
            	View bankInfo = LayoutInflater.from(getBaseContext()).inflate(R.layout.bank_info, null);
            	message.addView(bankInfo);
            	
//            	base.setLayoutParams(new LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, R.dimen.message_dialog_height_expanded));
            	
            } else if(Flag.dialogResult.equalsIgnoreCase("login")) {
            	
            	msg.setText("Coming soon! (Next version update.)");
            	
            } else if(Flag.dialogResult.equalsIgnoreCase("exit")) {
            	
            	msg.setText("Are you sure you want to close?");
            	
            	status.setImageResource(R.drawable.warning_icon);
            	
            	Button yes = (Button) findViewById(R.id.BTN_Yes);
            	Button no = (Button) findViewById(R.id.BTN_No);
            	Button ok = (Button) findViewById(R.id.BTN_OK);
            	
            	yes.setVisibility(View.VISIBLE);
            	no.setVisibility(View.VISIBLE);
            	ok.setVisibility(View.GONE);
            	
            } else if(Flag.dialogResult.equalsIgnoreCase(PurchaseActivity.FIELD_ERROR)) {
            	
            	msg.setText("Please fill-up the form completely first.");
            	
            	status.setImageResource(R.drawable.warning_icon);
            	
            } else if(Flag.dialogResult.equalsIgnoreCase(PurchaseActivity.NULL_ERROR)) {
            	
            	msg.setText("Value is nullified! Please make sure\n" +
							"that the fields are not empty.");
            	
            	status.setImageResource(R.drawable.warning_icon);
            	
            } else if(Flag.dialogResult.equalsIgnoreCase(Category_Custom_Search.SEARCH_NOT_FOUND)) {
            	
            	msg.setText(Category_Custom_Search.search_counter + " not found.");
            	
            	status.setImageResource(R.drawable.warning_icon);
            	
            } else {
            	
            	msg.setTextSize(getResources().getDimension(R.dimen.message_text_size_2));
            	msg.setText("Ver. 1:\n" +
            				"  -  Locally saved Wish List\n" +
            				"  -  Includes basic features in online shopping.\n\n" +
            				"Ver. 1.0.1 (Coming Soon):\n" +
            				"  -  Sync Wish List when login.\n" +
            				"  -  Categories section is available.");
            	
            }
        	
        } catch(NullPointerException e) {
        	
        	Log.e("CONDITION LOGIC STATUS", "Value comparison returns null. Arguement adjourned.");
        	
        }
	}
	
	
	
	
	
	//TODO _______________[ Button On Click Events for the Dialog ]_______________
	public void OK(View v) // --> For NO button or OK button when user decides to close the dialog instead.
	{
		finish();
	}
	
	public void yes(View v)
	{
		try
		{
			Cache.trimCache(this);
			
		} catch(Exception e) {
			
			
			
		} finally {
			
			Intent intent = new Intent(Intent.ACTION_MAIN); // instead of XitActivity use your first activity
			intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			intent.addCategory(Intent.CATEGORY_HOME);
			intent.putExtra("EXIT", true);
			finish();
			startActivity(intent);
			
		}
	}
}
