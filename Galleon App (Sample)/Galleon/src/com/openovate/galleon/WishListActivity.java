package com.openovate.galleon;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.HttpVersion;
import org.apache.http.client.HttpResponseException;
import org.apache.http.message.BasicHttpResponse;

import android.annotation.TargetApi;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.database.Cursor;
import android.database.SQLException;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;
import au.com.bytecode.opencsv.CSVWriter;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.actionbarsherlock.view.Window;
import com.openovate.galleon.Controls.MenuContentControls.MenuFragHelper;
import com.openovate.galleon.Database.Controls.RegistrationListener;
import com.openovate.galleon.Database.Model.DatabaseDbHelper;
import com.openovate.galleon.Database.Model.DatabaseTracker;
import com.openovate.galleon.Model.Flaggers.Flag;
import com.openovate.galleon.View.Category.Category_1_ToysKidsandBaby;
import com.openovate.galleon.View.Category.Category_2_SportsAndOutdoors;
import com.openovate.galleon.View.Category.Category_3_ClothingShoesAndJewelry;
import com.openovate.galleon.View.Category.Category_4_ElectronicsAndComputer;
import com.openovate.galleon.View.Category.Category_5_HomeGardenAndTools;
import com.openovate.galleon.View.Category.Category_Custom_Search;
import com.openovate.galleon.View.Category.Category_Select_Dialog_Pane;
import com.openovate.galleon.View.Notifications.Message_Dialog;
import com.openovate.galleon.View.Share.Share_Dialog;
import com.openovate.galleon.WordLengthTrimmer.WordCrop;
import com.openovate.galleon.jsonParser.utils.ImageDownloadAsynctask;

/**
 * <i>
 * Created by andybooboo on 8/3/13. <br>
 * Revised by David Coronado Dimalanta since 9/24/2013. <br><br><br>
 * </i>
 * 
 * <b>INFO: </b> Previews selected item(s) from the list. (Wish List only.)
 */

public class WishListActivity extends SherlockActivity 
{
    //TODO _______________[ Field Objects ]_______________
	// Integer Values
	private int X, Y = 0; // --> To be used for picking row number. // --> To be used for setting total number of rows.
	private Integer rowIndex;
	
	// String Values
	public static String items = "";
	public static String costs = "";
	public static String imageURL = "";
	public static String query2 = "SELECT * FROM " + DatabaseTracker.TABLE_LABELS_4;
	public static String getProductNameString[];
	
	// Context
	public final Context context = this;
	
	// Textbox
	public static EditText search; // --> Used to find items.
    
	// Peoduct details retrieved from content after syncing.
    public static TextView priceTV, productNameTV, detailTV;
    
    // Textviews for Column Names
    public static TextView checkbox;
    public static TextView productName;
	public static TextView price;
	public static TextView details;
	
	// Textviews to be used for loading and displaying info from database.
	public static TextView getProductName[];
	public static TextView getDetails[];
	public static TextView getImageURL[];
	public static TextView getPrice[];
	
	// Buttons used in Dialog Pane
	private Button close;
	private Button update;
	
	// Checkbox to mark for the items to be added on the Cart List.
	private CheckBox check[];
	
	// Table Layouts
	public static TableRow tableRow;
	public static TableLayout tableLayout;
	
	// Database Controller
	public DatabaseDbHelper dh;
	
	// Database Info Insertion Class (Regiatration Listener)
	@SuppressWarnings("unused")
	private RegistrationListener rl;
    
	// Images displayed from Image Downloader Async Task class.
    ImageView productIV;
    
    // Dialog
    private Dialog dialog;

    
    
    
    
    //TODO _______________[ Activity Overriden Method(s) ]_______________
    @Override
    public void onCreate(Bundle savedInstanceState) 
    {
    	// On Create Setup for Wish List
        super.onCreate(savedInstanceState);
        rl = new RegistrationListener(context);
        dialog = new Dialog(context);
        dialog.requestWindowFeature((int) Window.FEATURE_NO_TITLE);
        showWishListDatabase();
        
        // Button Settings
        Button back = (Button) findViewById(R.id.BTN_Checkout);
        back.setText("Return to Menu");
    }

	@SuppressWarnings({ "unused", "static-access" })
	@Override
    public boolean onCreateOptionsMenu(Menu menu) 
    {
		super.onCreateOptionsMenu(menu);
		
        // Inflate the menu; this adds items to the action bar if it is present.
        getSupportMenuInflater().inflate(R.menu.project_galleon_main, menu);
        ActionBar ab = getSupportActionBar();
        ab.setCustomView(R.layout.main_actionbar_galleon);
        
        // Set functionality of the textbox for edit listener.
        MenuFragHelper.SearchFunction.searchTextBox(ab, this);
        
        // Cart Button on Actionbar.
        ImageButton cartButton = (ImageButton) ab.getCustomView().findViewById(R.id.IB_Add_to_Cart);
        cartButton.setOnClickListener(new View.OnClickListener() 
        {
			@Override
			public void onClick(View v) 
			{
				Intent intent = new Intent();
				intent.setClass(context, CartListActivity.class);
				startActivity(intent);
			}
		});
        
        ab.setDisplayOptions(ab.DISPLAY_SHOW_CUSTOM | ab.DISPLAY_SHOW_CUSTOM);
        return true;
    }

	@Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
    	// Choice logics within the soft menu key dropdown event when one of the item ID's is touched.
    	switch(item.getItemId())
    	{
    	case R.id.OPT_1_Home:
    		home();
    		return true;
    		
    	case R.id.OPT_2_Login:
    		login();
    		return true;
    		
    	case R.id.OPT_3_Share:
    		share();
    		return true;
    		
    	case R.id.OPT_4_Categories:
    		categories();
    		return true;
    		
    	case R.id.OPT_5_Search:
    		search();
    		return true;
    		
    	case R.id.OPT_6_Cart_List:
    		cart();
    		return true;
    		
    	case R.id.OPT_7_Wish_List:
    		wish();
    		return true;
    		
    	case R.id.OPT_8_Buy:
    		buy();
    		return true;
    		
    	case R.id.OPT_9_Exit:
    		exit();
    		return true;
    		
    	default:
    		return true;
    	}
    }
    
    
    
    
    
    //TODO _______________[ On Choice Listener Event for Menu Soft Key ]_______________
    private void home()
    {
    	Intent intent = new Intent(this, ProjectGalleonMainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP); 
        startActivity(intent);
    }
    
    private void login()
    {
    	Flag.dialogResult = "login";
    	System.out.println("FLAG " + Flag.dialogResult);
    	startActivity(new Intent(this, Message_Dialog.class));
    }
    
    private void share()
    {
    	startActivity(new Intent(this, Share_Dialog.class));
    }
    
    private void categories()
    {
//    	startActivity(new Intent(this, Category_Select_Dialog_Pane.class));
    	Flag.dialogResult = "login";
    	System.out.println("FLAG " + Flag.dialogResult);
    	startActivity(new Intent(this, Message_Dialog.class));
    }
    
    private void search()
    {
    	finish();
    	startActivity(new Intent(this, Category_Custom_Search.class));
    }
    
    private void cart()
    {
    	finish();
    	startActivity(new Intent(this, CartListActivity.class));
    }
    
    private void wish()
    {
    	finish();
    	startActivity(new Intent(this, WishListActivity.class));
    }
    
    private void buy()
    {
    	finish();
    	startActivity(new Intent(this, PurchaseActivity.class));
    }
    
    private void exit()
    {
    	Flag.dialogResult = "exit";
    	System.out.println("FLAG " + Flag.dialogResult);
    	startActivity(new Intent(this, Message_Dialog.class));
    }
    
    
    
    
    
    
    //TODO _______________[ On Method Callbacks for Category Dialogs ]_______________
    public void toysKidsandBaby()
    {
    	startActivity(new Intent(this, Category_1_ToysKidsandBaby.class));
    }
    
    public void sportsAndOutdoors()
    {
    	startActivity(new Intent(this, Category_2_SportsAndOutdoors.class));
    }
    
    public void clothingShoesAndJewelry()
    {
    	startActivity(new Intent(this, Category_3_ClothingShoesAndJewelry.class));
    }
    
    public void electronicsAndComputers()
    {
    	startActivity(new Intent(this, Category_4_ElectronicsAndComputer.class));
    }
    
    public void homeGardenAndTools()
    {
    	startActivity(new Intent(this, Category_5_HomeGardenAndTools.class));
    }
    
    
    
    
    
    //TODO _______________[ Show Message Dialog Method) ]_______________
    public void showMessageDialog() 
    {
    	startActivity(new Intent(this, Message_Dialog.class));
	}
    
    
    
    
    
	//TODO __________[ AsyncTask Method for Exporting Database ]__________
	public class ExportCSVDatabaseWishList extends AsyncTask<String, Void, Boolean>
	{
		private final ProgressDialog d = new ProgressDialog(context);
		private final DatabaseDbHelper dh = new DatabaseDbHelper(context);
		
		@Override
		protected void onPreExecute()
		{
			this.d.setMessage("Saving wish list. Please wait...");
			this.d.show();
		}
		
		@Override
		protected Boolean doInBackground(final String... args) 
		{
			File dbFile = getDatabasePath("WishListDatabase.db");
			System.out.println("Database name: " + dbFile);
			
			File exportDir = new File(Environment.getExternalStorageDirectory(), "");
			
			if(!exportDir.exists())
			{
				exportDir.mkdirs();
				dh.db =  dh.db;
			}
			
			File file = new File(exportDir, "WISHLIST - List of Wishes.csv");
			
			StringBuilder buildItemList = new StringBuilder();
			
			try
			{
				file.createNewFile();
				CSVWriter writeCSV = new CSVWriter(new FileWriter(file));
				
				Cursor c = dh.db.rawQuery(query2, null);
				
				int current = 0;
				int next = 0;
				X = 0;
				
				while(c.moveToNext())
				{
					String[] arrString =
						{
								c.getString(0), // --> Item Index
								c.getString(1), // --> Item Name
								c.getString(2), // --> Price
								c.getString(3), // --> Details
								c.getString(4) // --> Image URL
						};
					
					System.out.println(c.getString(0) +
							" " + c.getString(1) +
							" " + c.getString(2) +
							" " + c.getString(3) +
							" " + c.getString(4));
					
					buildItemList.append(WordCrop.truncate(c.getString(1), 20));
					
//					next = Integer.parseInt(getSubtotal[X].getText().toString());
					current = current + next;
					X++;
					
					if(check[X - 1].isChecked())
					{
						dh.deleteWishItemRow(c, getProductNameString);
					}
					
					writeCSV.writeNext(arrString);
				}
				
				items = buildItemList.toString();
				
				writeCSV.close();
				c.close();
				dh.db.close();
				
				this.d.dismiss();
				
				return true;
				
			} catch(SQLException e) {
				
				Log.e("AndroidStarter - SQL Status", e.getMessage(), e);
				return false;
				
			} catch(IOException e) {
				
				Log.e("AndroidStarter - I/O Status", e.getMessage(), e);
				return false;
				
			}
		}
		
		@Override
		protected void onPostExecute(Boolean success)
		{
			if(this.d.isShowing())
			{
				this.d.dismiss();
			}
			
			if (success)
	        {
	            Toast.makeText(WishListActivity.this, "Successfully updated new item(s) to wish list!", Toast.LENGTH_SHORT).show();

	        } else {

	            Toast.makeText(WishListActivity.this, "Update failed. Please try again.", Toast.LENGTH_SHORT).show();

	        }
		}
	}
	
	
	
	
	
	//TODO _______________[ Database View ]_______________
	@SuppressWarnings("unused")
	@TargetApi(Build.VERSION_CODES.FROYO) 
	public void showWishListDatabase() //TODO Wish List
	{
		setContentView(R.layout.database_viewer);
		LinearLayout parent = (LinearLayout) findViewById(R.id.Scroll_Layout);
		
		dh = new DatabaseDbHelper(context);
		dh.db = dh.db;
		
		Cursor c = dh.db.rawQuery(query2, null);
	    int count= c.getCount();
	    c.moveToFirst();
	    
	    LinearLayout mainLayout = new LinearLayout(getApplicationContext());
	    mainLayout.setOrientation(LinearLayout.VERTICAL);
	    LinearLayout.LayoutParams params = new LinearLayout.LayoutParams
	    		(
	    				LinearLayout.LayoutParams.MATCH_PARENT,
	    				LinearLayout.LayoutParams.MATCH_PARENT
	    		);
	    
	    LinearLayout databaseLayout = new LinearLayout(getApplicationContext());
	    databaseLayout.setOrientation(LinearLayout.VERTICAL);
	    LinearLayout.LayoutParams dbParams = new LinearLayout.LayoutParams
	    		(
	    				LinearLayout.LayoutParams.MATCH_PARENT,
	    				0
	    		);
	    databaseLayout.setWeightSum(1);
	    
	    tableLayout = new TableLayout(getApplicationContext());
	    TableLayout.LayoutParams tblParams = new TableLayout.LayoutParams
	    		(
	    				LinearLayout.LayoutParams.MATCH_PARENT,
	    				10
	    		);
	    tableLayout.setWeightSum(0.5f);
	    tableLayout.setGravity(Gravity.RIGHT);
	    
	    Button exitButton = new Button(getApplicationContext());
	    exitButton.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));
	    exitButton.setWidth(150);
	    exitButton.setHeight(50);
	    exitButton.setText("Exit");
	    exitButton.setOnClickListener(new View.OnClickListener() 
	    {
			@Override
			public void onClick(View v) 
			{
				startActivity(new Intent(context, PurchaseActivity.class));
			}
		});
	    
	    ScrollView scroll = new ScrollView(getApplicationContext());
	    scroll.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, 10));
	    scroll.setVerticalScrollBarEnabled(true);
	    scroll.setHorizontalScrollBarEnabled(true);
	    scroll.setVisibility(1);
	    
	    tableRow = new TableRow(getApplicationContext());
	    
	    checkbox = new TextView(getApplicationContext());
	  	checkbox.setText("PRODUCT INDEX NO.");
	  	checkbox.setTextColor(Color.argb(255, 255, 119, 0));
	  	checkbox.setTypeface(null, Typeface.BOLD);
	  	checkbox.setPadding(20, 20, 20, 20);
	  	tableRow.addView(checkbox);
	  	
	  	productName = new TextView(getApplicationContext());
	  	productName.setText("ITEM");
	  	productName.setTextColor(Color.argb(255, 255, 119, 0));
	  	productName.setTypeface(null, Typeface.BOLD);
	  	productName.setPadding(20, 20, 20, 20);
	  	tableRow.addView(productName);
	  	
	  	price = new TextView(getApplicationContext());
	  	price.setText("PRICE");
	  	price.setTextColor(Color.argb(255, 255, 119, 0));
	  	price.setTypeface(null, Typeface.BOLD);
	  	price.setPadding(20, 20, 20, 20);
	  	tableRow.addView(price);
	  	
	  	details = new TextView(getApplicationContext());
	  	details.setText("DETAILS");
	  	details.setTextColor(Color.argb(255, 255, 119, 0));
	  	details.setTypeface(null, Typeface.BOLD);
	  	details.setPadding(20, 20, 20, 20);
	  	tableRow.addView(details);
	  	
	  	tableRow.setBackgroundColor(Color.argb(255, 51, 51, 51));
	  	
	  	tableLayout.addView(tableRow, tblParams);
	  	
	    for (rowIndex = 0; rowIndex < count; rowIndex++)
	    {
	    	Log.v("ROW INDEX CHECK A", "" + rowIndex);
	    	
	    	if(rowIndex == 0)
	    	{
	    		getProductNameString = new String[count];
	    		check = new CheckBox[count];
	    		getProductName = new TextView[count];
	    		getPrice = new TextView[count];
	    		getDetails = new TextView[count];
	    		getImageURL = new TextView[count];
	    	}
	    	
	    	final int x = rowIndex;
	    	
	    	tableRow = new TableRow(getApplicationContext());
	    	tableRow.setTag(rowIndex);
	    	tableRow.setBackgroundColor(Color.argb(0, 255, 255, 255));
	       
	    	check[rowIndex] = new CheckBox(getApplicationContext());
	    	check[rowIndex].setText(c.getString(c.getColumnIndex(DatabaseTracker.KEY_ID)));
	    	check[rowIndex].setTextColor(Color.argb(255, 255, 255, 255));
	    	check[rowIndex].setBackgroundColor(Color.BLUE);
	    	check[rowIndex].setOnCheckedChangeListener(new OnCheckedChangeListener() 
	    	{
	    		@Override
	    		public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) 
	    		{
	    			if(isChecked)
	    			{
	    				Toast.makeText(context, "Checkbox Index " + String.valueOf(x), Toast.LENGTH_SHORT).show();
	    			}
	    		}
	    	});
	       
	    	getProductName[rowIndex] = new TextView(getApplicationContext());
	    	getProductName[rowIndex].setText(WordCrop.truncate(c.getString(c.getColumnIndex(DatabaseTracker.WishList.PRODUCT_NAME)), 44));
	    	getProductName[rowIndex].setTextColor(Color.argb(255, 0, 0, 0));
	    	getProductNameString[rowIndex] = c.getString(c.getColumnIndex(DatabaseTracker.WishList.PRODUCT_NAME));
	       
	    	getPrice[rowIndex] = new TextView(getApplicationContext());
	    	getPrice[rowIndex].setText(c.getString(c.getColumnIndex(DatabaseTracker.WishList.PRICE)));
	    	getPrice[rowIndex].setTextColor(Color.argb(255, 0, 0, 0));
	       
	    	getDetails[rowIndex] = new TextView(getApplicationContext());
	    	getDetails[rowIndex].setText(c.getString(c.getColumnIndex(DatabaseTracker.WishList.DETAILS)));
	    	getDetails[rowIndex].setTextColor(Color.argb(255, 0, 0, 0));
	       
	    	getImageURL[rowIndex] = new TextView(getApplicationContext());
	       	getImageURL[rowIndex].setText(c.getString(c.getColumnIndex(DatabaseTracker.WishList.IMAGE_URL)));
	       	getImageURL[rowIndex].setTextColor(Color.argb(255, 0, 0, 0));
	       
	       	View previewButtonLayout = LayoutInflater.from(getBaseContext()).inflate(R.layout.preview_button_layout, null);
	       	final Button preview = (Button) previewButtonLayout.findViewById(R.id.BTN_Preview);
	       	preview.setOnClickListener(new View.OnClickListener() 
		    {
				@Override
				public void onClick(View v)
				{
					System.out.println("ITEM " + String.valueOf(x));
					Toast.makeText(context, "No. " + String.valueOf(x), Toast.LENGTH_SHORT).show();
					
					if(x == 3) // --> Index no. 3 (Row 4)
					{
						System.exit(0);
					}
					
					try 
					{
						showDescriptionDialog(x);
						
					} catch(HttpResponseException e) {
						
						e.printStackTrace();
			        	Log.e("LOADING IMAGE STATUS", "Response error!");
						
					} catch(NullPointerException e) {
						
			        	Log.e("LOADING IMAGE STATUS", "Bitmap drawable returned null!");
						
					}
				}
			});
	       
	       	check[rowIndex].setPadding(20, 20, 20, 20);
	       	getProductName[rowIndex].setPadding(20, 20, 20, 20);
	       	getPrice[rowIndex].setPadding(20, 20, 20, 20);
	       	getDetails[rowIndex].setPadding(20, 20, 20, 20);
	       	getImageURL[rowIndex].setPadding(20, 20, 20, 20);
	       
	       	tableRow.addView(check[rowIndex]);
	       	tableRow.addView(getProductName[rowIndex]);
	       	tableRow.addView(getPrice[rowIndex]);
	       	tableRow.addView(previewButtonLayout);
	       	tableLayout.addView(tableRow, tblParams);
	       
	       	c.moveToNext() ;
	    }
	    
	    scroll.addView(tableLayout);
	    databaseLayout.addView(scroll, params);
	    
	    mainLayout.addView(databaseLayout);
	    
	    parent.addView(mainLayout);
	    
		// Orientation
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
	     
	    dh.db.close();
	}
	
	
	
	
	
	//TODO _______________[ Database Delete Method for Wish List ]_______________
	private void deleteRow()
	{
		// Set and call database.
		dh = new DatabaseDbHelper(context);
		dh.db =  dh.db;
		Cursor c = dh.db.rawQuery(query2, null);
		
		X = 0; // --> Index Max Loop
		
		// Begin the loop!
		while(c.moveToNext())
		{
			System.out.println(c.getString(0) +
					" " + c.getString(1) +
					" " + c.getString(2) +
					" " + c.getString(3) +
					" " + c.getString(4));
			
			X++;
			
			if(check[X - 1].isChecked())
			{
				dh.deleteWishItemRow(Integer.parseInt(c.getString(0)));
//				dh.deleteWishItemRow(c.getString(1));
			}
		}
		
		Toast.makeText(context, String.valueOf(X) + "item(s) is/are deleted.", Toast.LENGTH_SHORT).show();
		
		c.close();
		dh.db.close();
	}
	
	
	
	
	
	//TODO _______________[ Button Listeneer Events ]_______________
	public void showDescriptionDialog(int index) throws HttpResponseException, NullPointerException
	{
		// Set the dialog.
		dialog.setContentView(R.layout.dialog_product_detail);
		dialog.show();
		
		// Set the description details.
		TextView des = (TextView) dialog.findViewById(R.id.productDescription);
		TextView price = (TextView) dialog.findViewById(R.id.productPrice);
		TextView name = (TextView) dialog.findViewById(R.id.productName);
		des.setText(getDetails[index].getText().toString());
		price.setText(getPrice[index].getText().toString());
		name.setText(WordCrop.truncate(getProductNameString[index], 23));
		
		// In case of updates that will verify the official price of this product, use this code.
		String current = getProductNameString[index];
		String replaced = current.replaceAll(" ", "+");
		ProjectGalleonMainActivity.search.setText(replaced);
		
		// Set button constructors...
		close = (Button) dialog.findViewById(R.id.BTN_Close_Dialog);
		update = (Button) dialog.findViewById(R.id.BTN_Update_Product_Price);
		
		// And listeners.
		SetDialogButtonListeners(dialog);
		
		// Set ImageDownloadAsynctask object.
        ImageDownloadAsynctask idat = new ImageDownloadAsynctask((ImageView) dialog.findViewById(R.id.productImage), context, getImageURL[index].getText().toString());

        // Set the bitmsp.
//        Bitmap bitmap = idat.fetchBitmapFromCache("http:\\\\www.amazon.com\\Macross-Frontier-VF-25S-Messiah-Valkyrie\\dp\\B001GXSMPA%3FSubscriptionId%3DAKIAI3XM53IGZRPXET5Q%26tag%3Dgalgome-20%26linkCode%3Dxm2%26camp%3D2025%26creative%3D165953%26creativeASIN%3DB001GXSMPA");
        Bitmap bitmap = idat.fetchBitmapFromCache(getImageURL[index].getText().toString());
        
		// Execute the ImageDownloadAsyncTask!
        try
        {
        	// Check if bitmap has no value.
        	if(bitmap == null)
//        	if (!((bitmap == null) || (bitmap.toString().trim().equals("")))) // --> Check if the image content is not available.
            {
                idat.execute();
                
            } else {
            	
            	((ImageView) dialog.findViewById(R.id.productPic)).setImageBitmap(bitmap);
            	
            }
        	
        } catch(Exception e) {
        	
        	// Error Message
        	Log.e("LOADING IMAGE STATUS", "Loading takes too long process!");
        	
        } finally {
        	
        	// Check the server connection status.
         	HttpResponse response = new BasicHttpResponse(HttpVersion.HTTP_1_1, HttpStatus.SC_OK, "OK");
         	Log.v("DETAIL DIALOG STATUS", "Protocol Version: " + response.getProtocolVersion());
         	Log.v("DETAIL DIALOG STATUS", "Status Code: " + response.getStatusLine().getStatusCode());
         	Log.v("DETAIL DIALOG STATUS", "Reason: " + response.getStatusLine().getReasonPhrase());
         	Log.v("DETAIL DIALOG STATUS", "Line: " + response.getStatusLine().toString());
        	
        }
        
        // Check if value has contain no null value.
        Log.v("DETAIL DIALOG PRODUCT INFO", "Name: " + getProductNameString[index]);
        Log.v("DETAIL DIALOG PRODUCT INFO", "Description: " + getDetails[index].getText().toString());
        Log.v("DETAIL DIALOG PRODUCT INFO", "Image URL:" + getImageURL[index].getText().toString());
        Log.v("DETAIL DIALOG PRODUCT INFO", "Price: " + getPrice[index].getText().toString());
	}
	
	
	
	
	
	//TODO _______________[ Button Listeneer Events ]_______________
	public void checkout(View v)
	{
		finish();
	}
	
	public void discard(View v)
	{
		try
		{
			try
			{
				Toast.makeText(context, "Attempting to delete a row...", Toast.LENGTH_SHORT).show();
				
			} finally {
				
				Toast.makeText(context, "Now deleting a row...", Toast.LENGTH_SHORT).show();
				deleteRow();
				
			}
			
		} catch(IllegalStateException e) {
			
			e.printStackTrace();
			Toast.makeText(context, "Delete failed.", Toast.LENGTH_SHORT).show();
			
		} finally {
			
			finish();
			startActivity(new Intent(this, WishListActivity.class));
			
		}
	}
	
	private void SetDialogButtonListeners(final Dialog dialog)
	{
		close.setOnClickListener(new View.OnClickListener() 
		{
			@Override
			public void onClick(View v) 
			{
				finish();
			}
		});
		
		update.setOnClickListener(new View.OnClickListener() 
		{
			@Override
			public void onClick(View v) 
			{
				startActivity(new Intent(context, Category_Custom_Search.class));
				dialog.hide();
			}
		});
	}
	
	public void back(View v)
	{
		startActivity(new Intent(context, WishListActivity.class));
	}
}