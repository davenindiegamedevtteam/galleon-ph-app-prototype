package com.openovate.galleon;

import java.util.LinkedHashMap;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.jess.ui.TwoWayGridView;
import com.openovate.galleon.Controls.CacheManagement.Cache;
import com.openovate.galleon.Controls.MenuContentControls.MenuFragHelper;
import com.openovate.galleon.Database.Model.DatabaseDbHelper;
import com.openovate.galleon.Model.Flaggers.Flag;
import com.openovate.galleon.Model.URLFinder.JSONSourceURLFinder;
import com.openovate.galleon.View.Category.Category_1_ToysKidsandBaby;
import com.openovate.galleon.View.Category.Category_2_SportsAndOutdoors;
import com.openovate.galleon.View.Category.Category_3_ClothingShoesAndJewelry;
import com.openovate.galleon.View.Category.Category_4_ElectronicsAndComputer;
import com.openovate.galleon.View.Category.Category_5_HomeGardenAndTools;
import com.openovate.galleon.View.Category.Category_Custom_Search;
import com.openovate.galleon.View.Category.Category_Select_Dialog_Pane;
import com.openovate.galleon.View.Notifications.Message_Dialog;
import com.openovate.galleon.View.Share.Share_Dialog;
import com.openovate.galleon.jsonParser.models.FeatureModel;
import com.openovate.galleon.jsonParser.utils.DownloadContentAsyncTask2;
import com.openovate.galleon.jsonParser.utils.DownloadContentAsyncTask3;
import com.openovate.galleon.jsonParser.utils.DownloadContentAsyncTask4;
import com.openovate.galleon.jsonParser.utils.ImageDownloadAsynctask;
import com.openovate.galleon.jsonParser.utils.TestAsync;

public class ProjectGalleonMainActivity extends SherlockActivity 
{
	//TODO _______________[ Data Fields ]_______________
	public final Context context = this;
	
	private JSONSourceURLFinder url;
	
	public static TwoWayGridView dgrid0, dgrid1, dgrid2, dgrid3, dgrid4;
	
	public static EditText search;
	
	public static int flag;
	public static int index = 0;
	
	public static String featureURL = "Sorry! No feature available yet. :-P";
	
//	private final float scale = this.getResources().getDisplayMetrics().density;
	
	
	
	
	
	//TODO _______________[ Sherlock Activity Overriden Methods ]_______________
	@SuppressLint("NewApi")
	@Override
    protected void onCreate(Bundle savedInstanceState) 
    {
		// Override this method.
        super.onCreate(savedInstanceState);
        
        // Screen Setup
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.home_page);
        com.openovate.galleon.StaticClassPackage.StaticClass.bitmapCache = new LinkedHashMap<String, Bitmap>();
        
        // Set product layout download syncing.
        setRandomCategoryLayout();
        showMessageDialog();
        
        // Set intent flag.
        if (getIntent().getBooleanExtra("EXIT", false)) 
        {
        	try 
        	{
                Cache.trimCache(this);
                Toast.makeText(context, "Clearing cache...", Toast.LENGTH_SHORT).show();
                finish();
                
            } catch (Exception e) {
                
                e.printStackTrace();
                Toast.makeText(context, "ERROR!", Toast.LENGTH_SHORT).show();
                
            } finally {
            	
            	Toast.makeText(context, "Cache memory clear.", Toast.LENGTH_SHORT).show();
            	finish();
            	
            }
        }
        
        // Test
//        finish();
//        startActivity(new Intent(this, Share_Dialog.class));
//      finish();
//      startActivity(new Intent(this, TestActivity.class));
        
        // Reset Cart List every time the user re-opens the app.
        DatabaseDbHelper dh = new DatabaseDbHelper(context);
        dh.deleteCartList();
    }
	
    @Override
    protected void onStop()
    {
       super.onStop();
    }
    
    @Override
    protected void onDestroy() 
    {
    	super.onDestroy();
//    	unbindDrawables(findViewById(R.id.Home_Page_Root_View));
//    	System.gc();
    	
    	try 
    	{
            Cache.trimCache(this);
            Toast.makeText(context, "Clearing cache...", Toast.LENGTH_SHORT).show();
            
        } catch (Exception e) {
            
            e.printStackTrace();
            Toast.makeText(context, "ERROR!", Toast.LENGTH_SHORT).show();
            
        } finally {
        	
        	Toast.makeText(context, "Cache memory clear.", Toast.LENGTH_SHORT).show();
        	
        }
    }

	@SuppressWarnings({ "unused", "static-access" })
	@Override
    public boolean onCreateOptionsMenu(Menu menu) 
    {
		super.onCreateOptionsMenu(menu);
		
        // Inflate the menu; this adds items to the action bar if it is present.
        getSupportMenuInflater().inflate(R.menu.project_galleon_main, menu);
        ActionBar ab = getSupportActionBar();
        
        
        // Set functionality of the textbox for edit listener.
        MenuFragHelper.SearchFunction.searchTextBox(ab, this);
        
        // Cart Button on Actionbar. (Call MenuFragHelper first.)
        ImageButton cartButton = (ImageButton) ab.getCustomView().findViewById(R.id.IB_Add_to_Cart);
        cartButton.setOnClickListener(new View.OnClickListener() 
        {
			@Override
			public void onClick(View v) 
			{
				Intent intent = new Intent();
				intent.setClass(context, CartListActivity.class);
				startActivity(intent);
			}
		});
        
        ab.setDisplayOptions(ab.DISPLAY_SHOW_CUSTOM | ab.DISPLAY_SHOW_CUSTOM);
        return true;
    }

	@Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
    	// Choice logics within the soft menu key dropdown event when one of the item ID's is touched.
    	switch(item.getItemId())
    	{
    	case R.id.OPT_1_Home:
    		home();
    		return true;
    		
    	case R.id.OPT_2_Login:
    		login();
    		return true;
    		
    	case R.id.OPT_3_Share:
    		share();
    		return true;
    		
    	case R.id.OPT_4_Categories:
    		categories();
    		return true;
    		
    	case R.id.OPT_5_Search:
    		search();
    		return true;
    		
    	case R.id.OPT_6_Cart_List:
    		cart();
    		return true;
    		
    	case R.id.OPT_7_Wish_List:
    		wish();
    		return true;
    		
    	case R.id.OPT_8_Buy:
    		buy();
    		return true;
    		
    	case R.id.OPT_9_Exit:
    		exit();
    		return true;
    		
    	default:
    		return true;
    	}
    }
    
    
    
    
    
    //TODO _______________[ On Choice Listener Event for Menu Soft Key ]_______________
    private void home()
    {
    	Intent intent = new Intent(this, ProjectGalleonMainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP); 
        startActivity(intent);
    }
    
    private void login()
    {
    	Flag.dialogResult = "login";
    	System.out.println("FLAG " + Flag.dialogResult);
    	startActivity(new Intent(this, Message_Dialog.class));
    }
    
    private void share()
    {
    	startActivity(new Intent(this, Share_Dialog.class));
    }
    
    private void categories()
    {
//    	startActivity(new Intent(this, Category_Select_Dialog_Pane.class));
    	Flag.dialogResult = "login";
    	System.out.println("FLAG " + Flag.dialogResult);
    	startActivity(new Intent(this, Message_Dialog.class));
    }
    
    private void search()
    {
    	startActivity(new Intent(this, Category_Custom_Search.class));
    }
    
    private void cart()
    {
    	startActivity(new Intent(this, CartListActivity.class));
    }
    
    private void wish()
    {
    	startActivity(new Intent(this, WishListActivity.class));
    }
    
    private void buy()
    {
    	startActivity(new Intent(this, PurchaseActivity.class));
    }
    
    private void exit()
    {
    	Flag.dialogResult = "exit";
    	System.out.println("FLAG " + Flag.dialogResult);
    	startActivity(new Intent(this, Message_Dialog.class));
    }
    
    
    
    
    
    
    //TODO _______________[ On Method Callbacks for Category Dialogs ]_______________
    public void toysKidsandBaby()
    {
    	startActivity(new Intent(this, Category_1_ToysKidsandBaby.class));
    }
    
    public void sportsAndOutdoors()
    {
    	startActivity(new Intent(this, Category_2_SportsAndOutdoors.class));
    }
    
    public void clothingShoesAndJewelry()
    {
    	startActivity(new Intent(this, Category_3_ClothingShoesAndJewelry.class));
    }
    
    public void electronicsAndComputers()
    {
    	startActivity(new Intent(this, Category_4_ElectronicsAndComputer.class));
    }
    
    public void homeGardenAndTools()
    {
    	startActivity(new Intent(this, Category_5_HomeGardenAndTools.class));
    }
    
    
    
    
    
    //TODO _______________[ Show Message Dialog Method) ]_______________
    public void showMessageDialog() 
    {
    	startActivity(new Intent(this, Message_Dialog.class));
	}
    
    
    
    
    
    //TODO _______________[ Layout View for Category (Home Page) ]_______________
    private void setRandomCategoryLayout()
    {
    	// Set size of the Two-way Gridview's height according to the density of the screen.
    	int size = 0;
    	switch (getResources().getDisplayMetrics().densityDpi) 
    	{
    	case DisplayMetrics.DENSITY_LOW:
    	    // ...
    		Log.v("DENSITY STATUS", "LDPI " + size);
    	    break;
    	    
    	case DisplayMetrics.DENSITY_MEDIUM:
    	    // ...
    		Log.v("DENSITY STATUS", "HDPI " + size);
    	    break;
    	    
    	case DisplayMetrics.DENSITY_HIGH:
    	    size = 350;
    	    Log.v("DENSITY STATUS", "HDPI " + size);
    	    break;
    	    
    	case DisplayMetrics.DENSITY_TV:
    	    size = 450;
    	    Log.v("DENSITY STATUS", "TVDPI 	" + size);
    	    break;
    	    
    	case DisplayMetrics.DENSITY_XHIGH:
    	    // ...
    		Log.v("DENSITY STATUS", "XHDPI " + size);
    	    break;
    	    
    	case DisplayMetrics.DENSITY_XXHIGH:
    	    // ...
    		Log.v("DENSITY STATUS", "XXHDPI " + size);
    	    break;
    	}
    	
    	// Set the URL of the JSON files.
    	url = new JSONSourceURLFinder();
    	FeatureModel model = new FeatureModel("0");
    	
    	// Inflate layout ID from TwoWayGridView class.
//    	View gridLayoutView0 = LayoutInflater.from(getBaseContext()).inflate(com.jess.ui.R.layout.feature_main, null);
    	View gridLayoutView1 = LayoutInflater.from(getBaseContext()).inflate(com.jess.ui.R.layout.main, null);
    	View gridLayoutView2 = LayoutInflater.from(getBaseContext()).inflate(com.jess.ui.R.layout.main2, null);
    	View gridLayoutView3 = LayoutInflater.from(getBaseContext()).inflate(com.jess.ui.R.layout.main3, null);
    	View gridLayoutView4 = LayoutInflater.from(getBaseContext()).inflate(com.jess.ui.R.layout.main4, null);
    	
    	// Feature Item (Set the image source from the URL first)
//    	dgrid0 = (TwoWayGridView) findViewById(R.id.TGV_Button);
//    	dgrid0.setScrollDirectionPortrait(com.jess.ui.R.id.horizontal);
//    	dgrid0.setScrollDirectionLandscape(com.jess.ui.R.id.horizontal);
//    	new DownloadContentAsyncTask3(JSONSourceURLFinder.getPopularSearchURL(), dgrid0, getApplicationContext(), true).execute();
////    	new DownloadContentAsyncTask3("https://pbs.twimg.com/media/A8VFtIICIAE7BUO.jpg", dgrid0, getApplicationContext(), true).execute();
//    	ImageView feature = (ImageView) findViewById(R.id.productPic2);
//        ImageDownloadAsynctask idat = new ImageDownloadAsynctask(feature, context, featureURL);
//        Bitmap bitmap = idat.fetchBitmapFromCache(featureURL);
//        if(bitmap == null)
//        {
//            idat.execute();
//            
//        } else {
//        	
//        	((ImageView) findViewById(R.id.productPic2)).setImageBitmap(bitmap);
//        	Toast.makeText(context, "URL at Home page:\n\n" + DownloadContentAsyncTask3.getImageUrl(), Toast.LENGTH_SHORT).show();
//        	
//        }
    	try
    	{
    		/*
    		 * 
    		 * 			Back-up URL (for testing):
    		 * 			"https://pbs.twimg.com/media/A8VFtIICIAE7BUO.jpg"
    		 * 
    		 */
    		
    		dgrid0 = (TwoWayGridView) findViewById(R.id.TGV_Button);
        	dgrid0.setScrollDirectionPortrait(com.jess.ui.R.id.horizontal);
        	dgrid0.setScrollDirectionLandscape(com.jess.ui.R.id.horizontal);
        	new DownloadContentAsyncTask3(JSONSourceURLFinder.getPopularSearchURL(), dgrid0, getApplicationContext(), true).execute();
//        	new DownloadContentAsyncTask3("https://pbs.twimg.com/media/A8VFtIICIAE7BUO.jpg", dgrid0, getApplicationContext(), true).execute();
    		
    		Toast.makeText(context, "Checking for FEATURED ITEM URL...\n\n" + featureURL, Toast.LENGTH_SHORT).show();
    		
    	} catch(NullPointerException e) {
    		
//    		Toast.makeText(context, "Cannot view on FEATURED ITEM.", Toast.LENGTH_SHORT).show();
    		
    	}  catch(RuntimeException e) {
    		
//    		Toast.makeText(context, "Process halted on FEATURED ITEM.", Toast.LENGTH_SHORT).show();
    		
    	} finally {
    		
    		ImageView feature = (ImageView) findViewById(R.id.productPic2);
            ImageDownloadAsynctask idat = new ImageDownloadAsynctask(feature, context, Flag.imageURL);
            Bitmap bitmap = idat.fetchBitmapFromCache(Flag.imageURL);
            
//            Log.v("FEATURE IMAGE URL @ MAIN", DownloadContentAsyncTask3.getImageUrl());
            
            try
            {
            	Toast.makeText(context, "Searching for FEATURED ITEM URL at...\n\n" + Flag.imageURL, Toast.LENGTH_SHORT).show();
            	
            	if(bitmap == null)
                {
                    idat.execute();
                    
                } else {
                	
                	((ImageView) findViewById(R.id.productPic2)).setImageBitmap(bitmap);
//                	Toast.makeText(context, "Feature item is set.", Toast.LENGTH_SHORT).show();
                	Toast.makeText(context, "URL at Home page:\n\n" + Flag.imageURL, Toast.LENGTH_SHORT).show();
                	
                }
            	
            } catch(NullPointerException e) {
            	
//            	Toast.makeText(context, "Couldn't find the URL of the FEATURED ITEM.\nSearch canceled", Toast.LENGTH_SHORT).show();
            	
            }
    		
    	}
    	
    	// Random Category 1 (Popular Search Results)
    	LinearLayout category1 = (LinearLayout) findViewById(R.id.Random_Category_1);
    	category1.addView(gridLayoutView1);
    	dgrid1 = (TwoWayGridView) findViewById(com.jess.ui.R.id.gridview);
    	dgrid1.setScrollDirectionPortrait(com.jess.ui.R.id.horizontal);
    	dgrid1.setScrollDirectionLandscape(com.jess.ui.R.id.horizontal);
    	dgrid1.setNumRows(1);
    	dgrid1.setHorizontalSpacing(20);
    	dgrid1.setVerticalSpacing(5);
    	dgrid1.setBackgroundColor(Color.WHITE);
        dgrid1.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, size));
//        new DownloadContentAsyncTask3(JSONSourceURLFinder.getPopularSearchURL(), dgrid1, getApplicationContext(), true).execute();
        
    	// Random Category 2
    	LinearLayout category2 = (LinearLayout) findViewById(R.id.Random_Category_2);
    	category2.addView(gridLayoutView2);
    	dgrid2 = (TwoWayGridView) findViewById(com.jess.ui.R.id.gridview2);
    	dgrid2.setScrollDirectionPortrait(com.jess.ui.R.id.horizontal);
    	dgrid2.setScrollDirectionLandscape(com.jess.ui.R.id.horizontal);
    	dgrid2.setNumRows(1);
    	dgrid2.setHorizontalSpacing(20);
    	dgrid2.setVerticalSpacing(5);
    	dgrid2.setBackgroundColor(Color.WHITE);
        dgrid2.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, size));
//        new DownloadContentAsyncTask2(url.getURL2(0, 0), dgrid2, getApplicationContext()).execute();
        
    	// Random Category 3
    	LinearLayout category3 = (LinearLayout) findViewById(R.id.Random_Category_3);
    	category3.addView(gridLayoutView3);
    	dgrid3 = (TwoWayGridView) findViewById(com.jess.ui.R.id.gridview3);
    	dgrid3.setScrollDirectionPortrait(com.jess.ui.R.id.horizontal);
    	dgrid3.setScrollDirectionLandscape(com.jess.ui.R.id.horizontal);
    	dgrid3.setNumRows(1);
    	dgrid3.setHorizontalSpacing(20);
    	dgrid3.setVerticalSpacing(0);
    	dgrid3.setBackgroundColor(Color.WHITE);
        dgrid3.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, size));
//        new DownloadContentAsyncTask2(url.getURL5(0, 0), dgrid3, getApplicationContext()).execute();
        
    	// Random Category 4
    	LinearLayout category4 = (LinearLayout) findViewById(R.id.Newest_Products);
    	category4.addView(gridLayoutView4);
    	dgrid4 = (TwoWayGridView) findViewById(com.jess.ui.R.id.gridview4);
    	dgrid4.setScrollDirectionPortrait(com.jess.ui.R.id.horizontal);
    	dgrid4.setScrollDirectionLandscape(com.jess.ui.R.id.horizontal);
    	dgrid4.setNumRows(3);
    	dgrid4.setHorizontalSpacing(20);
    	dgrid4.setVerticalSpacing(0);
    	dgrid4.setBackgroundColor(Color.WHITE);
        dgrid4.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, size * 3));
//        new DownloadContentAsyncTask3(JSONSourceURLFinder.getBrowseURL(), dgrid4, getApplicationContext(), true).execute();
        
        // Download the contents properly so that it can manage the thread process.
//        ProgressDialog dialog = new ProgressDialog(this);
        try
        {
//        	dialog.setMessage("Connecting to WI-FI...");
//        	dialog.show();
        	new DownloadContentAsyncTask3(JSONSourceURLFinder.getPopularSearchURL(), dgrid1, getApplicationContext(), true).execute();
        	
        } catch(RuntimeException e) {
        	
//        	Toast.makeText(context, "Process halted. Unable to verify content asyncronization \nat POPULAR SEARCH.", Toast.LENGTH_SHORT).show();
        	
        } finally {
        	
        	try
            {
//        		dialog.setMessage("Loading RANDOM CATEGORY 1 RESULTS...");
//        		new DownloadContentAsyncTask3(url.getURL2(0, 0), dgrid2, getApplicationContext(), true).execute();
            	
            } catch(RuntimeException e) {
            	
//            	Toast.makeText(context, "Process halted. Unable to verify content asyncronization \nat RANDOM CATGEORY 1.", Toast.LENGTH_SHORT).show();
            	
            } finally {
            	
            	try
                {
//            		dialog.setMessage("Loading RANDOM CATEGORY 2 RESULTS...");
//            		new DownloadContentAsyncTask3(url.getURL5(0, 0), dgrid3, getApplicationContext(), true).execute();
                	
                } catch(RuntimeException e) {
                	
//                	Toast.makeText(context, "Process halted. Unable to verify content asyncronization \nat RANDOM CATEGORY 2.", Toast.LENGTH_SHORT).show();
                	
                } finally {
                	
                	try
                    {
//                		dialog.setMessage("Loading BROWSE ITEM RESULTS...");
                		new DownloadContentAsyncTask3(JSONSourceURLFinder.getBrowseURL(), dgrid4, getApplicationContext(), true).execute();
                    	
                    } catch(RuntimeException e) {
                    	
//                    	Toast.makeText(context, "Process halted. Unable to verify content asyncronization \nat BROWSE RESULT.", Toast.LENGTH_SHORT).show();
                    	
                    } finally {
                    	
//                    	dialog.dismiss();
                    	Toast.makeText(context, "Content loaded.", Toast.LENGTH_SHORT).show();
                    	
                    }
                	
                }
            	
            }
        	
        }
    }
    
    
    
    
    
    //
    private void unbindDrawables(View view) 
    {
        if (view.getBackground() != null) 
        {
        	view.getBackground().setCallback(null);
        }
        
        context.getCacheDir().deleteOnExit();
        Log.v("MAIN ACTIVITY RECYCLE STATUS", "All views removed.");
        Log.v("TEMP FILE IF EXISTED", String.valueOf(context.getCacheDir().delete()));
        
        // Check if the cache memory is cleared.
        if(context.getCacheDir().delete())
        {
        	Toast.makeText(context, "Cache memory cleared and refreshed.", Toast.LENGTH_SHORT).show();
        }
        
//        if ((view instanceof ViewGroup)  && !(view instanceof AdapterView)) 
//        {
//            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) 
//            {
//            	unbindDrawables(((ViewGroup) view).getChildAt(i));
//            }
//            
//            ((ViewGroup) view).removeAllViews();
//            
//            Log.v("MAIN ACTIVITY RECYCLE STATUS", "All views removed.");
//        }
    }
    
    
    
    
    
//    @Override
//    public boolean dispatchTouchEvent(MotionEvent ev)
//    {
//       if(ev.getAction() == MotionEvent.ACTION_MOVE)
//       {
////    	   return true;
//    	   return false;
//    	   
//       } else {
//    	   
//    	   return super.dispatchTouchEvent(ev);
//    	   
//       }
//    }
}
