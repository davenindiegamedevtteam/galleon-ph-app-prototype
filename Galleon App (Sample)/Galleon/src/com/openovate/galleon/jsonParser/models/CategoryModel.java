package com.openovate.galleon.jsonParser.models;

import java.io.Serializable;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.openovate.galleon.R;
import com.openovate.galleon.jsonParser.utils.ImageDownloadAsynctask;

/**
 * <i>
 * Created by andybooboo on 8/2/13. <br>
 * Revised by David Dimalanta since 8/7/2013. <br><br><br>
 * </i>
 * 
 * <b>INFO:</b> Displays the following content from the JSON file.
 */

public class CategoryModel implements Serializable
{
	//TODO _______________[ Field Objects ]_______________
    private String categoryId;
    private String categoryName;

    
    
    
    
    //TODO _______________[ Product Model Getters and Setters ]_______________
    public CategoryModel(String categoryId)
    {
        this.categoryId = categoryId;
    }

    public String getCategoryId() 
    {
        return categoryId;
    }

    public void setCategoryId(String categoryId) 
    {
        this.categoryId = categoryId;
    }

    public String getCategoryName() 
    {
        return categoryName;
    }

    public void setCategoryName(String categoryName) 
    {
        this.categoryName = categoryName;
    }

    public View getView(int position, View convertView, ViewGroup parent, Context context)
    {
    	// Inflate the view and insert it on each grid view.
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.subcategory_spinner_layout, parent, false); // --> Get this layout and implemented onto each item within the list.

        // Get the ID of the TextView from this view and display the name of product. 
        TextView categoryName = (TextView) rowView.findViewById(R.id.TV_Category_List);
        categoryName.setText(this.getCategoryName());

        return rowView;
    }

    
    
    
    
    //TODO _______________[ Serializable Overriden Method(s) ]_______________
    @Override
    public String toString()
    {
        return this.categoryName;
    }
}
