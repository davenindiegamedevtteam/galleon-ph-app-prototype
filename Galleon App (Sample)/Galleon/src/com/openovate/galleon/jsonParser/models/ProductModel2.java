package com.openovate.galleon.jsonParser.models;

import java.io.Serializable;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.openovate.galleon.R;
import com.openovate.galleon.WordLengthTrimmer.WordCrop;
import com.openovate.galleon.jsonParser.utils.ImageDownloadAsynctask;

/**
 * <i>
 * Created by andybooboo on 8/2/13. <br>
 * Revised by David Dimalanta since 8/7/2013. <br><br><br>
 * </i>
 * 
 * <b>INFO:</b> Displays the following content from the JSON file.
 */

public class ProductModel2 implements Serializable
{
	//TODO _______________[ Field Objects ]_______________
    private String productId;
    private String subproductId;
    private String productAmazon;
    private String productUrl;
    private String productName;
    private String productDescription;
    private String productPrice;
    private String productSrp;
    private String productActive;
    private String productUpdated;
    private String productCreated;
    private String productCategory;
    private String productImageUrl;
    
    public static boolean isSelected = true;

    
    
    
    
    //TODO _______________[ Product Model Getters and Setters ]_______________
    public ProductModel2(String productId)
    {
        this.productId = productId;
    }

    public String getProductId() 
    {
        return productId;
    }
    
    public String getSubProductId() 
    {
        return subproductId;
    }

    public void setProductId(String productId) 
    {
        this.subproductId = productId;
    }

    public String getProductAmazon() 
    {
        return productAmazon;
    }

    public void setProductAmazon(String productAmazon) 
    {
        this.productAmazon = productAmazon;
    }

    public String getProductUrl() 
    {
        return productUrl;
    }

    public void setProductUrl(String productUrl) 
    {
        this.productUrl = productUrl;
    }

    public String getProductName() 
    {
        return productName;
    }

    public void setProductName(String productName) 
    {
        this.productName = productName;
    }

    public String getProductDescription() 
    {
        return productDescription;
    }

    public void setProductDescription(String productDescription) 
    {
        this.productDescription = productDescription;
    }

    public String getProductPrice() 
    {
        return productPrice;
    }

    public void setProductPrice(String productPrice) 
    {
        this.productPrice = productPrice;
    }

    public String getProductSrp() 
    {
        return productSrp;
    }

    public void setProductSrp(String productSrp) 
    {
        this.productSrp = productSrp;
    }

    public String getProductActive() 
    {
        return productActive;
    }

    public void setProductActive(String productActive) 
    {
        this.productActive = productActive;
    }

    public String getProductUpdated() 
    {
        return productUpdated;
    }

    public void setProductUpdated(String productUpdated) 
    {
        this.productUpdated = productUpdated;
    }

    public String getProductCreated() 
    {
        return productCreated;
    }

    public void setProductCreated(String productCreated) 
    {
        this.productCreated = productCreated;
    }

    public String getProductCategory() 
    {
        return productCategory;
    }

    public void setProductCategory(String productCategory) 
    {
        this.productCategory = productCategory;
    }

    public String getProductImageUrl() 
    {
        return productImageUrl;
    }

    public void setProductImageUrl(String productImageUrl) 
    {
        this.productImageUrl = productImageUrl;
    }

    public View getView(int position, View convertView, ViewGroup parent, Context context)
    {
    	// Inflate the view and insert it on each grid view.
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.list_view_item_base_layout_for_home_page, parent, false); // --> Get this layout and implemented onto each item within the list.

        // Set Progress Bar.
//        ProgressBar bar = (ProgressBar) rowView.findViewById(R.id.PB_Loading_Image);
//        bar.setVisibility(View.GONE);
        
        // Get the ID of the TextView from this view and display the name of product. 
        TextView productName = (TextView) rowView.findViewById(R.id.productName);
        productName.setText(WordCrop.truncate(this.getProductName(), 40));

        // Set ImagedownloadAsynctask object.
        ImageDownloadAsynctask idat = new ImageDownloadAsynctask((ImageView)rowView.findViewById(R.id.productPic), context, this.getProductImageUrl());

        // Set the bitmsp.
        Bitmap bitmap = idat.fetchBitmapFromCache(this.getProductImageUrl());
        
        // Check String value for URL.
//        Log.v("PRODUCT URL @ ProductModel2", this.getProductUrl());
//        Log.v("IMAGE URL @ ProductModel2", this.getProductImageUrl());
        
//        if (!((bitmap == null) || (bitmap.toString().trim().equals("")))) // --> Check if the image content is not available.
        if(bitmap == null)
        {
//        	bar.setVisibility(View.VISIBLE);
            idat.execute();
            
        } else {
        	
//        	bar.setVisibility(View.GONE);
        	((ImageView) rowView.findViewById(R.id.productPic)).setImageBitmap(bitmap);
        	
        }

        return rowView;
    }

    
    
    
    
    //TODO _______________[ Serializable Overriden Method(s) ]_______________
    @Override
    public String toString()
    {
        return this.productName;
    }
}
