package com.openovate.galleon.jsonParser.models;

import java.io.Serializable;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.HttpVersion;
import org.apache.http.message.BasicHttpResponse;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.openovate.galleon.R;
import com.openovate.galleon.WordLengthTrimmer.WordCrop;
import com.openovate.galleon.jsonParser.utils.ImageDownloadAsynctask;

/**
 * <i>
 * Created by andybooboo on 8/2/13. <br>
 * Revised by David Dimalanta since 8/7/2013. <br><br><br>
 * </i>
 * 
 * <b>INFO:</b> Displays the following content from the JSON file.
 */

public class ProductModel implements Serializable
{
	//TODO _______________[ Field Objects ]_______________
	private String productId;
	private String subproductId;
    private String productAmazon;
    private String productUrl;
    private String productName;
    private String productDescription;
    private String productPrice;
    private String productSrp;
    private String productActive;
    private String productUpdated;
    private String productCreated;
    private String productCategory;
    private String productImageUrl;
    
    public static boolean isSelected = true;

    
    
    
    
    //TODO _______________[ Product Model Getters and Setters ]_______________
    public ProductModel(String productId)
    {
        this.productId = productId;
    }

    public String getProductId() 
    {
        return productId;
    }

    public String getSubProductId() 
    {
        return subproductId;
    }
    
    public void setProductId(String productId) 
    {
        this.subproductId = productId;
    }

    public String getProductAmazon() 
    {
        return productAmazon;
    }

    public void setProductAmazon(String productAmazon) 
    {
        this.productAmazon = productAmazon;
    }

    public String getProductUrl() 
    {
        return productUrl;
    }

    public void setProductUrl(String productUrl) 
    {
        this.productUrl = productUrl;
    }

    public String getProductName() 
    {
        return productName;
    }

    public void setProductName(String productName) 
    {
        this.productName = productName;
    }

    public String getProductDescription() 
    {
        return productDescription;
    }

    public void setProductDescription(String productDescription) 
    {
        this.productDescription = productDescription;
    }

    public String getProductPrice() 
    {
        return productPrice;
    }

    public void setProductPrice(String productPrice) 
    {
        this.productPrice = productPrice;
    }

    public String getProductSrp() 
    {
        return productSrp;
    }

    public void setProductSrp(String productSrp) 
    {
        this.productSrp = productSrp;
    }

    public String getProductActive() 
    {
        return productActive;
    }

    public void setProductActive(String productActive) 
    {
        this.productActive = productActive;
    }

    public String getProductUpdated() 
    {
        return productUpdated;
    }

    public void setProductUpdated(String productUpdated) 
    {
        this.productUpdated = productUpdated;
    }

    public String getProductCreated() 
    {
        return productCreated;
    }

    public void setProductCreated(String productCreated) 
    {
        this.productCreated = productCreated;
    }

    public String getProductCategory() 
    {
        return productCategory;
    }

    public void setProductCategory(String productCategory) 
    {
        this.productCategory = productCategory;
    }

    public String getProductImageUrl() 
    {
        return productImageUrl;
    }

    public void setProductImageUrl(String productImageUrl) 
    {
        this.productImageUrl = productImageUrl;
    }

    public View getView(int position, View convertView, ViewGroup parent, Context context)
    {
    	// Change the flag tom prevent getting from IllegalStateException.
    	ProductModel2.isSelected = false;
    	Log.v("SWITCH STATUS", "Product Model 1 is selected.");
//    	System.out.println(this.getProductActive());
    	
    	// Inflate the view and insert it on each grid view.
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.row_product, parent, false); // --> Get this layout and implemented onto each item within the list.
//        View buyView = inflater.inflate(R.layout.activity_product_detail, parent, false);
        
        // Get the ID of the TextView from this view and display the name of product. 
        TextView productName = (TextView) rowView.findViewById(R.id.productName);
        productName.setText(WordCrop.truncate(this.getProductName(), 40));
        
        // Get the ID of the Button(s) from this view and display the name of product. 
//        View gridLayoutView1 = LayoutInflater.from(getBaseContext()).inflate(R.layout.activity_product_detail, null);
//        ImageButton buy = (ImageButton) buyView.findViewById(R.id.BTN_Buy);
//        
//        // Check for product active status if item is available.
//        String check = this.getProductActive();
//        
//        if((check != null) && check.equalsIgnoreCase("2"))
//        {
//        	buy.setVisibility(View.VISIBLE);
//        	
//        } else {
//        	
//        	buy.setVisibility(View.GONE);
//        	
//        }

        // Set ImagedownloadAsynctask object.
        ImageDownloadAsynctask idat = new ImageDownloadAsynctask((ImageView)rowView.findViewById(R.id.productPic), context, this.getProductImageUrl());

        // Set the bitmsp.
        Bitmap bitmap = idat.fetchBitmapFromCache(this.getProductImageUrl());
        
//        if (!((bitmap == null) || (bitmap.toString().trim().equals("")))) // --> Check if the image content is not available.
        if(bitmap == null)
        {
            idat.execute();
            
        } else {
        	
        	((ImageView) rowView.findViewById(R.id.productPic)).setImageBitmap(bitmap);
        	
        }
        
        // Check the server connection status.
        HttpResponse response = new BasicHttpResponse(HttpVersion.HTTP_1_1, 
        HttpStatus.SC_OK, "OK");
        System.out.println(response.getProtocolVersion());
        System.out.println(response.getStatusLine().getStatusCode());
        System.out.println(response.getStatusLine().getReasonPhrase());
        System.out.println(response.getStatusLine().toString());

        return rowView;
    }

    
    
    
    
    //TODO _______________[ Serializable Overriden Method(s) ]_______________
    @Override
    public String toString()
    {
        return this.productName;
    }
}
