package com.openovate.galleon.jsonParser.utils;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;

public class ImageDownloadHelper 
{
    public static final String TAG = "ImageDownloadHelper";

    public static Bitmap download(String url) 
    {
        Bitmap bm = null;
        
        try 
        {
            URL u = new URL(url);
            URLConnection connection = u.openConnection();
            connection.connect();
            InputStream is = connection.getInputStream();
            BufferedInputStream bis = new BufferedInputStream(is);
            bm = BitmapFactory.decodeStream(bis);
            bis.close();
            is.close();
            
        } catch (IOException e) {
        	
        	e.printStackTrace();
        	Log.e(TAG, "URL Error! Search failed.");
        	
        }
        
        return bm;
    }
}
