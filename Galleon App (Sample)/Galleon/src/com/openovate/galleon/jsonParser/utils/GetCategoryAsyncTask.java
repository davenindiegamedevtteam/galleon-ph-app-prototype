package com.openovate.galleon.jsonParser.utils;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Iterator;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.HttpVersion;
import org.apache.http.message.BasicHttpResponse;
import org.json.JSONObject;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.GridView;

import com.openovate.galleon.jsonParser.adapters.CategoryListAdapter;
import com.openovate.galleon.jsonParser.models.CategoryModel;

/**
 * <i>
 * Created by andybooboo on 8/2/13. <br>
 * Revised by andybooboo since 9/12/2013. <br><br><br>
 * </I>
 * 
 * <b>INFO:</b> JSON Content Downloader used for downloading content from a JSON file through text search only.
 */

public class GetCategoryAsyncTask extends AsyncTask<String, String, String> 
{
	//TODO _______________[ Field Objects ]_______________
    private String url;
    private GridView categoryList;
    private Context applicationContext;
    private JSONObject jObject;
    private static int noOfArrays;

    
    
    
    
    //TODO _______________[ Constructor ]_______________
    public GetCategoryAsyncTask(String url, GridView categoryList,  Context applicationContext)
    {
        this.url = url;
        this.categoryList = categoryList;
        this.applicationContext = applicationContext;
    }

    
    
    
    
    //TODO _______________[ Async Task Overriden Method(s) ]_______________
    @Override
    protected String doInBackground(String... parameters)
    {
        try 
        {
        	// Fetch the URL first and buffer it.
            URL dataSource = new URL(url);
            URLConnection dSCon = dataSource.openConnection();
            BufferedReader in = new BufferedReader(new InputStreamReader(dSCon.getInputStream()));
            String line;
            String str = "";

            // Check if line is empty.
            while ((line = in.readLine()) != null) 
            {
                str += line;
            }
            
            // Check the server connection status.
            HttpResponse response = new BasicHttpResponse(HttpVersion.HTTP_1_1, 
            HttpStatus.SC_OK, "OK");
            System.out.println(response.getProtocolVersion());
            System.out.println(response.getStatusLine().getStatusCode());
            System.out.println(response.getStatusLine().getReasonPhrase());
            System.out.println(response.getStatusLine().toString());

            return str;
            
        } catch (Exception e) {
        	
            e.printStackTrace();
            return e.getMessage();
            
        }
    }

	@SuppressWarnings("unchecked")
	@Override
    protected void onPostExecute(String result) // --> String str
    {
        try
        {
            ArrayList<CategoryModel> list = new ArrayList<CategoryModel>();
            CategoryModel temp;
            jObject = new JSONObject(result); // --> Get the result as constructor for JSON file.
            
            //for(int i=0; i<(jObject.length()); i++) // --> Do the loop until all the contents are filled in the list.
            for(Iterator<String> iter = jObject.keys(); iter.hasNext();)
            {
            	String key = iter.next();
            	{
            		temp = new CategoryModel(jObject.getJSONObject(key).getString("category_id"));
                    temp.setCategoryName(jObject.getJSONObject(key).getString("category_name"));
                    list.add(temp);
                    
                    Log.v("CATEGORY ASYNC TASK STATUS", jObject.getJSONObject(key).getString("category_id") + ": " + jObject.getJSONObject(key).getString("category_name"));
            	}
            }
            
            categoryList.setAdapter(new CategoryListAdapter(applicationContext, list)); // --> Set the adapter.
             
        } catch (Exception e) {
        	
            e.printStackTrace();
            
        }
    }
}
