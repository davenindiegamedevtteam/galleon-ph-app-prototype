package com.openovate.galleon.jsonParser.utils;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.ImageView;
import android.widget.Toast;

import com.openovate.galleon.R;
import com.openovate.galleon.Controls.CacheManagement.Cache;
import com.openovate.galleon.StaticClassPackage.StaticClass;

/**
 * <i>
 * Created by andybooboo on 8/2/13. <br><br><br>
 * </I>
 * 
 * <b>INFO:</b> Downloads image file from the URL.
 */

@SuppressLint("NewApi")
public class ImageDownloadAsynctask extends AsyncTask<String, Void, Bitmap> 
{
    public static String TAG = "ImageDownloadAsyncTask";
    private String url;
    private Context context;
    private boolean resize = true;
    public int width;
    public int height;
    private ImageView imageView;

    public ImageDownloadAsynctask(ImageView imageView, Context context, String url)
    {
        this.imageView = imageView;
        this.context = context;
        this.url = url;
        width = 100;
        height = 100;
    }

    @Override
    protected Bitmap doInBackground(String... params) 
    {
        Bitmap bitmap = fetchBitmapFromCache(url);
        
        if (bitmap == null) 
        {
            bitmap = ImageDownloadHelper.download(url);
        }
        
        return bitmap;
    }

    @Override
    protected void onPreExecute() 
    {
        super.onPreExecute();
    }


    @Override
    protected void onPostExecute(Bitmap bitmap) 
    {
        super.onPostExecute(bitmap);

        if (isCancelled()) 
        {
            bitmap = null;
            Log.w("DOWNLOAD STATUS", "Download failed. Process aborted.");
        }

        cacheBitmap(url, bitmap);

        if(bitmap!=null)
        {
            if(resize)
            {
//                bitmap = Bitmap.createScaledBitmap(bitmap, width, height, false);
            	bitmap = Bitmap.createBitmap(bitmap);
            }
            
            Drawable drawable = new BitmapDrawable(context.getResources(), bitmap);

            if((imageView != null) && (drawable != null))
            {
            	imageView.setImageDrawable(drawable);
            	
            } else {
            	
//            	imageView.setBackgroundColor(Color.RED);
            	
            }
            
            Log.i("DOWNLOAD STATUS", "Complete");
        }
    }

    private void cacheBitmap(String url, Bitmap bitmap) 
    {
        if (bitmap != null) 
        {
            synchronized (StaticClass.bitmapCache) 
            {
                StaticClass.bitmapCache.put(url, bitmap);
            }
        }
    }

    public Bitmap fetchBitmapFromCache(String url) 
    {
        synchronized (StaticClass.bitmapCache)  // --> Checking for nullPointerException and cache memory clearing status...
        {
            Bitmap bitmap = StaticClass.bitmapCache.get(url);
            
            if (bitmap != null) 
            {
                StaticClass.bitmapCache.remove(url);
//                context.getCacheDir().delete();
//                Cache.trimCache(context);
                StaticClass.bitmapCache.put(url, bitmap);
                
                Log.v("BITMAP CACHE STATUS", "Overwrite");
                Log.v("BITMAP CACHE IF DELETED", String.valueOf(context.getCacheDir().delete()));
                
                // Check if the cache memory is cleared.
//                if(context.getCacheDir().delete())
//                {
//                	Toast.makeText(context, "Cache memory cleared and refreshed.", Toast.LENGTH_SHORT).show();
//                }
            }
            
            return bitmap;
        }
    }
}
