//package com.openovate.galleon.jsonParser.utils;
//
//import java.io.BufferedReader;
//import java.io.InputStreamReader;
//import java.net.URL;
//import java.net.URLConnection;
//import java.util.ArrayList;
//import java.util.Iterator;
//
//import org.apache.http.HttpResponse;
//import org.apache.http.HttpStatus;
//import org.apache.http.HttpVersion;
//import org.apache.http.message.BasicHttpResponse;
//import org.json.JSONObject;
//
//import android.content.Context;
//import android.os.AsyncTask;
//import android.util.Log;
//import android.widget.Spinner;
//
//import com.openovate.galleon.ProjectGalleonMainActivity;
//import com.openovate.galleon.jsonParser.adapters.ProductListAdapter2;
//import com.openovate.galleon.jsonParser.models.FeatureModel;
//import com.openovate.galleon.jsonParser.models.ProductModel2;
//
///**
// * <i>
// * Created by andybooboo on 8/2/13. <br>
// * Revised by David Coronado Dimalanta since 9/25/2013. <br><br><br>
// * </I>
// * 
// * <b>INFO:</b> JSON Content Downloader used for downloading content from a JSON file that does not contain an array only.
// */
//
//public class CategoryLoader
//{
//	
//	public class Category extends AsyncTask<String, String, String> 
//	{
//		//TODO _______________[ Field Objects ]_______________
//	    private String url;
//	    private Spinner category;
//	    private Context applicationContext;
//	    private JSONObject jObject;
//	    private boolean index;
//
//	    
//	    
//	    
//	    
//	    //TODO _______________[ Constructor ]_______________
//	    /**
//	     * <b>INFO:</b> The constructor of the async task. (CATEGORY URL only.)
//	     * 
//	     * @param url = String value for the JSON file. (URL)
//	     */
//	    public Category(String url, Spinner category, Context applicationContext)
//	    {
//	        this.url = url;
//	        this.category = category;
//	        this.applicationContext = applicationContext;
//	        this.index = index;
//	    }
//
//	    
//	    
//	    
//	    
//	    //TODO _______________[ Async Task Overriden Method(s) ]_______________
//	    @Override
//	    protected String doInBackground(String... parameters)
//	    {
//	        try 
//	        {
//	        	// Fetch the URL first and buffer it.
//	            URL dataSource = new URL(url);
//	            URLConnection dSCon = dataSource.openConnection();
//	            BufferedReader in = new BufferedReader(new InputStreamReader(dSCon.getInputStream()));
//	            String line;
//	            String str = "";
//
//	            // Check if line is empty.
//	            while ((line = in.readLine()) != null) 
//	            {
//	                str += line;
//	            }
//	            
//	            // Check the server connection status.
//	            HttpResponse response = new BasicHttpResponse(HttpVersion.HTTP_1_1, 
//	            HttpStatus.SC_OK, "OK");
//	            System.out.println(response.getProtocolVersion());
//	            System.out.println(response.getStatusLine().getStatusCode());
//	            System.out.println(response.getStatusLine().getReasonPhrase());
//	            System.out.println(response.getStatusLine().toString());
//
//	            return str;
//	            
//	        } catch (Exception e) {
//	        	
//	            e.printStackTrace();
//	            return e.getMessage();
//	            
//	        }
//	    }
//
//		@SuppressWarnings("unchecked")
//		@Override
//	    protected void onPostExecute(String result) // --> String str
//	    {
//	        try
//	        {
//	        	ArrayList<ProductModel2> list = new ArrayList<ProductModel2>();
//	        	ArrayList<FeatureModel> f = new ArrayList<FeatureModel>();
//	            ProductModel2 temp;
//	            FeatureModel feature;
//	            jObject = new JSONObject(result); // --> Get the result as constructor for JSON file.
//	            
//	            //for(int i=0; i<(jObject.length()); i++) // --> Do the loop until all the contents are filled in the list.
//	            if(index)
//	            {
//	            	boolean stop = false;
//	            	int flag = 0;
//	            	
//	            	for(Iterator<String> iter = jObject.keys(); iter.hasNext();)
//	                {
//	                	String key = iter.next();
//	                	{
//	                		temp = new ProductModel2(jObject.getJSONObject(key).getString("product_id"));
//	                		temp.setProductId(jObject.getJSONObject(key).getString("product_id"));
//	                        temp.setProductImageUrl(jObject.getJSONObject(key).getString("product_image"));
//	                        temp.setProductName(jObject.getJSONObject(key).getString("product_name"));
//	                        temp.setProductDescription(jObject.getJSONObject(key).getString("product_description"));
//	                        temp.setProductPrice(jObject.getJSONObject(key).getString("product_price"));
//	                        temp.setProductSrp(jObject.getJSONObject(key).getString("product_srp"));
//	                        temp.setProductActive(jObject.getJSONObject(key).getString("product_active"));
//	                        list.add(temp);
//	                        
//	                        feature = new FeatureModel(jObject.getJSONObject(key).getString("product_id"));
//	                        feature.setProductId(jObject.getJSONObject(key).getString("product_id"));
//	                        feature.setProductImageUrl(jObject.getJSONObject(key).getString("product_image"));
//	                        feature.setProductName(jObject.getJSONObject(key).getString("product_name"));
//	                        feature.setProductDescription(jObject.getJSONObject(key).getString("product_description"));
//	                        feature.setProductPrice(jObject.getJSONObject(key).getString("product_price"));
//	                        feature.setProductSrp(jObject.getJSONObject(key).getString("product_srp"));
//	                        feature.setProductActive(jObject.getJSONObject(key).getString("product_active"));
//	                        f.add(feature);
//	                        
//	                        if(flag == 0)
//	                        {
//	                        	imageUrl = jObject.getJSONObject(key).getString("product_image"); 
//	                        	ProjectGalleonMainActivity.featureURL = imageUrl;
//	                        	flag = 1;
//	                        }
//
//	                        Log.v("DOWNLOAD ASYNC TASK 3 ID", jObject.getJSONObject(key).getString("product_id"));
//	                	}
//	                	
//	                	Log.v("DOWNLOAD ASYNC TASK 3 KEY", key);
//	                	Log.v("DOWNLOAD ASYNC TASK 3 URL", jObject.getJSONObject(key).optString("product_image"));
//	                }
//	            	
//	            	Log.v("FEATURE IMAGE URL", imageUrl); // --> Check if FEATURE IMAGE URL returned to null.
//	            	
//	            	category.setAdapter(new ProductListAdapter2(applicationContext, list)); // --> Set the adapter.
//	            	
//	            } else {
//	            	
//	            	String key = "246507";
//	            	feature = new FeatureModel(jObject.getJSONObject(key).getString("product_id"));
//	                feature.setProductImageUrl(jObject.getJSONObject(key).optString("product_image"));
//	                feature.setProductName(jObject.getJSONObject(key).getString("product_name"));
//	                feature.setProductDescription(jObject.getJSONObject(key).getString("product_description"));
//	                feature.setProductPrice(jObject.getJSONObject(key).getString("product_price"));
//	                feature.setProductSrp(jObject.getJSONObject(key).getString("product_srp"));
//	                feature.setProductActive(jObject.getJSONObject(key).getString("product_active"));
//	                f.add(feature);
//	            	
//	                Log.v("DOWNLOAD ASYNC TASK 3 STATUS", "Complete");
//	            }
//	             
//	        } catch (Exception e) {
//	        	
//	            e.printStackTrace();
//	            
//	        }
//	    }
//		
//		
//		
//		
//		
//		public static String getImageUrl()
//		{
//			return imageUrl;
//		}
//	}
//}
