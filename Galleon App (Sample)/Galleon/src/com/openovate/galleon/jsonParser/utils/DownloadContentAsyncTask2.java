package com.openovate.galleon.jsonParser.utils;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.HttpVersion;
import org.apache.http.message.BasicHttpResponse;
import org.json.JSONArray;
import org.json.JSONException;

import android.content.Context;
import android.os.AsyncTask;
import android.widget.GridView;

import com.jess.ui.TwoWayGridView;
import com.openovate.galleon.jsonParser.adapters.ProductListAdapter2;
import com.openovate.galleon.jsonParser.models.ProductModel2;

/**
 * <i>
 * Created by andybooboo on 8/2/13. <br>
 * Revised by David Dimalanta since 9/5/2013. <br><br><br>
 * </i>
 * 
 * <b>INFO:</b> JSON Content Downloader used for downloading content from a JSON file. (<b>TwoWayGridView<b> only.)
 */

public class DownloadContentAsyncTask2 extends AsyncTask<String, String, String> 
{
	//TODO _______________[ Field Objects ]_______________
    private String url;
    private TwoWayGridView productUrlListView;
    private Context applicationContext;
    private JSONArray jArray;
    private static int noOfArrays;

    
    
    
    
    //TODO _______________[ Constructor ]_______________
    public DownloadContentAsyncTask2(String url, TwoWayGridView gridView, Context applicationContext)
    {
        this.url = url;
        this.productUrlListView = gridView;
        this.applicationContext = applicationContext;
    }

    
    
    
    
    //TODO _______________[ Async Task Overriden Method(s) ]_______________
    @Override
    protected String doInBackground(String... parameters)
    {
        try 
        {
        	// Fetch the URL first and buffer it.
            URL dataSource = new URL(url);
            URLConnection dSCon = dataSource.openConnection();
            BufferedReader in = new BufferedReader(new InputStreamReader(dSCon.getInputStream()));
            String line;
            String str = "";

            // Check if line is empty.
            while ((line = in.readLine()) != null) 
            {
                str += line;
            }
            
            // Check the server connection status.
            HttpResponse response = new BasicHttpResponse(HttpVersion.HTTP_1_1, 
            HttpStatus.SC_OK, "OK");
            System.out.println(response.getProtocolVersion());
            System.out.println(response.getStatusLine().getStatusCode());
            System.out.println(response.getStatusLine().getReasonPhrase());
            System.out.println(response.getStatusLine().toString());

            return str;
            
        } catch (Exception e) {
        	
            e.printStackTrace();
            return e.getMessage();
            
        }
    }

    @Override
    protected void onPostExecute(String result) // --> String str
    {
    	int x = 0;
    	
        try
        {
            ArrayList<ProductModel2> list = new ArrayList<ProductModel2>();
            ProductModel2 temp;
            jArray = new JSONArray(result); // --> Get the result as constructor for JSON file.
            noOfArrays = jArray.length();
            
            for(int i=0; i<(jArray.length()); i++) // --> Do the loop until all the contents are filled in the list.
            {
            	x = i;
            	
            	temp = new ProductModel2(jArray.getJSONObject(i).getString("product_id"));
            	temp.setProductId(jArray.getJSONObject(i).getString("product_id"));
                temp.setProductImageUrl(jArray.getJSONObject(i).getString("product_image"));
                temp.setProductName(jArray.getJSONObject(i).getString("product_name"));
                temp.setProductDescription(jArray.getJSONObject(i).getString("product_description"));
                temp.setProductPrice(jArray.getJSONObject(i).getString("product_price"));
                temp.setProductSrp(jArray.getJSONObject(i).getString("product_srp"));
                temp.setProductActive(jArray.getJSONObject(i).getString("product_active"));
                list.add(temp);
                
                System.out.println("URL --> " + jArray.getJSONObject(i).getString("product_image"));
            }

             productUrlListView.setAdapter(new ProductListAdapter2(applicationContext, list)); // --> Set the adapter.
             
        } catch (Exception e) {
        	
            e.printStackTrace();
        	System.out.println("WALA!");
        	System.out.println("WALA!");
        	System.out.println("WALA!");
        	System.out.println("WALA!");
        	System.out.println("WALA!");
        	System.out.println("WALA!");
        	System.out.println("WALA!");
        	System.out.println("WALA!");
        	System.out.println("WALA!");
            
        }
    }
    
    
    
    
    
    //TODO _______________[ Others ]_______________
    /**
     * 
     * Check if the total number of arrays within a JSON file.
     */
    public static int getTotalNumberOfArrays()
    {
    	return noOfArrays;
    }
}
