package com.openovate.galleon.jsonParser.adapters;

import java.io.Serializable;
import java.util.ArrayList;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;

import com.openovate.galleon.ProductDetailActivity;
import com.openovate.galleon.ProjectGalleonMainActivity;
import com.openovate.galleon.R;
import com.openovate.galleon.jsonParser.models.ProductModel;
import com.openovate.galleon.jsonParser.utils.DownloadContentAsyncTask;

/**
 * <i>
 * Created by andybooboo on 8/2/13. <br><br><br>
 * </i>
 * 
 * <b>INFO:</b> List Adapter to be used for the product's content.
 */
public class ProductListAdapter extends BaseAdapter 
{
	//TODO _______________[ Field Objects ]_______________
    private Context context;
    private ArrayList<ProductModel> products;
    private LayoutInflater inflater;

    
    
    
    
    //TODO _______________[ Constructor ]_______________
    public ProductListAdapter(Context context, ArrayList<ProductModel> products)
    {
        this.context = context;
        this.products = products;
        this.inflater = LayoutInflater.from(context);
    }

    
    
    
    
    //TODO _______________[ Base Adapter's Overriden Method(s) ]_______________
    @Override
    public int getCount() 
    {
        return products.size();
    }

    @Override
    public ProductModel getItem(int i) 
    {
        return products.get(i);
    }

    @Override
    public long getItemId(int i) 
    {
        if (i < getCount() && i >= 0) 
        {
            return i;
        }
        
        return 0;
    }

    @Override
    public View getView(final int i, View view, ViewGroup viewGroup) 
    {
        View rowView = this.products.get(i).getView(i, view, viewGroup, context);

        rowView.setOnClickListener(new View.OnClickListener() // --> If this view is clicked, it will directly preview via product viewer activity. (ProductDetailActivity.java)
        {
            @Override
            public void onClick(View view) 
            {
            	// Change the flag tom prevent getting from IllegalStateException.
            	ProjectGalleonMainActivity.flag = 0;
            	Log.v("SWITCH STATUS " + Integer.toString(ProjectGalleonMainActivity.flag), "From CATEGORY DIALOG PANE.");
            	
            	// Open and display item.
                Context context = view.getRootView().getContext();
                Intent intent = new Intent(context, ProductDetailActivity.class);
                Log.d("PUTEXTRA", "" + getItem(i));
                intent.putExtra("product", (Serializable) getItem(i));
                context.startActivity(intent);
            }
        });
    	
        return rowView;
    }
}
